﻿using Android.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using View = Android.Views.View;

namespace HomieProveedorApp.Droid
{
    public abstract class BaseEffect : PlatformEffect
    {
        private bool _unloaded;
        private bool _attached;

        protected bool Attached
        {
            get
            {
                return this._attached;
            }
        }

        protected BaseEffect() : base()
        {
        }

        protected override void OnAttached()
        {
            if (!this.CanBeApplied())
                return;
            this._attached = true;

            this.Container.ViewDetachedFromWindow -= ContainerViewDetachedFromWindow;
            this.Container.ViewDetachedFromWindow += ContainerViewDetachedFromWindow;

            this.Container.ViewAttachedToWindow -= ContainerViewAttachedToWindow;
            this.Container.ViewAttachedToWindow += ContainerViewAttachedToWindow;

            this.OnAttachedInternal();
        }

        protected override void OnDetached()
        {
            if (!this._attached || this._unloaded)
                return;
            this._attached = false;


            this.Container.ViewDetachedFromWindow -= this.ContainerViewDetachedFromWindow;
            this.Container.ViewAttachedToWindow -= this.ContainerViewAttachedToWindow;
            this.OnDetachedInternal();
        }

        protected virtual bool CanBeApplied()
        {
            return true;
        }

        protected virtual void OnAttachedInternal()
        {
        }

        protected virtual void OnDetachedInternal()
        {
        }

        private void ContainerViewDetachedFromWindow(object sender, View.ViewDetachedFromWindowEventArgs e)
        {
            this._unloaded = true;
        }

        private void ContainerViewAttachedToWindow(object sender, View.ViewAttachedToWindowEventArgs e)
        {
            this._unloaded = false;
        }
    }
}