﻿using Android.Graphics;
using Android.Widget;
using HomieProveedorApp.Droid;
using Xamarin.Forms;

[assembly: ExportEffect(typeof(UnderlineEffect), "UnderlineEffect")]
namespace HomieProveedorApp.Droid
{
    public class UnderlineEffect : BaseEffect
    {
        private TextView _text;

        protected override bool CanBeApplied()
        {
            _text = Control as TextView;
            return _text != null;
        }

        protected override void OnAttachedInternal()
        {
            TextView text = _text;
            //int num = text.PaintFlags | 8;

            text.PaintFlags = PaintFlags.UnderlineText;
        }

        protected override void OnDetachedInternal()
        {
            _text.PaintFlags = PaintFlags.LinearText;
        }
    }
}
