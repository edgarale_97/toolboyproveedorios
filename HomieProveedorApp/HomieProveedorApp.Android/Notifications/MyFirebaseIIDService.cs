﻿using System;
using Android.App;
using Firebase.Iid;
using Android.Util;
using System.Collections.Generic;
using WindowsAzure.Messaging;
using HomieProveedorApp.Helpers;
using Plugin.DeviceInfo;

namespace FCMClient
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";
        NotificationHub hub;
        NotificationHub hubMaster;

        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "FCM token: " + refreshedToken);
            SendRegistrationToServer(refreshedToken);
        }

        void SendRegistrationToServer(string token)
        {
            // Register with Notification Hubs
            hub = new NotificationHub(HomieProveedorApp.Helpers.ApiKeys.NotificationHubName,
                                        HomieProveedorApp.Helpers.ApiKeys.ListenConnectionString, this);

            var regID = "";
            var tags = new List<string>() { CrossDeviceInfo.Current.Id };
            regID = hub.Register(token, tags.ToArray()).RegistrationId;
            //var tag = hub.Register(token, "id" + regID).RegistrationId;

            Log.Debug(TAG, $"Successful registration of ID {regID}");
            HomieProveedorApp.Helpers.Settings.DeviceId = CrossDeviceInfo.Current.Id;
            HomieProveedorApp.Helpers.Settings.RegistrationId = regID;
        }

    }
}