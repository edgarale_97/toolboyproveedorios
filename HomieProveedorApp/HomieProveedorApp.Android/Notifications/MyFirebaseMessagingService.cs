﻿using Android.App;
using Android.Util;
using Firebase.Messaging;

using System.Linq;
using Android.Content;
using HomieProveedorApp.Droid;
using Android.Support.V4.App;
using System.Collections.Generic;

[Service]
[IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
public class MyFirebaseMessagingService : FirebaseMessagingService
{
    const string TAG = "MyFirebaseMsgService";
    public override void OnMessageReceived(RemoteMessage message)
    {
        Log.Debug(TAG, "From: " + message.From);
        if (message.GetNotification() != null)
        {
            // These is how most messages will be received
            Log.Debug(TAG, "Notification Message Body: " + message.GetNotification().Body);
            SendNotification(message.GetNotification().Body, message.GetNotification().Title, message.Data);
        }
        else
        {
            //Only used for debugging payloads sent from the Azure portal
            SendNotification(message.Data["title"], message.Data["message"], message.Data);
        }
    }

    void SendNotification(string title, string message, IDictionary<string, string> data)
    {
        var intent = new Intent(this, typeof(MainActivity));
        intent.AddFlags(ActivityFlags.ClearTop);
        foreach (var key in data.Keys)
        {
            intent.PutExtra(key, data[key]);
            //Log.Debug(TAG, "PutExtra: " + key + " ---> " + data[key]);
        }

        var pendingIntent = PendingIntent.GetActivity(this,
                                                      MainActivity.NOTIFICATION_ID,
                                                      intent,
                                                      PendingIntentFlags.OneShot);

        var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
                                  .SetSmallIcon(Resource.Drawable.ic_home)
                                  .SetContentTitle(title)
                                  .SetContentText(message)
                                  .SetAutoCancel(true)
                                  .SetContentIntent(pendingIntent);

        var notificationManager = NotificationManagerCompat.From(this);
        notificationManager.Notify(MainActivity.NOTIFICATION_ID, notificationBuilder.Build());
    }

}


