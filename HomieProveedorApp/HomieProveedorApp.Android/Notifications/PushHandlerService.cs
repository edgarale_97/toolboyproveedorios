﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsAzure.Messaging;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
//using Gcm.Client;
using HomieProveedorApp.Helpers;
using Plugin.DeviceInfo;

namespace HomieProveedorApp.Droid.Notifications
{
    /*[Service]
    public class PushHandlerService : GcmServiceBase
    {
        #region Fields and Properties

        private NotificationHub Hub { get; set; }
        public static Context Context;

        #endregion

        #region Constructor

        public PushHandlerService() : base(ApiKeys.GoogleSenderId)
        {
            Log.Info(AzurePushBroadcastReceiver.TAG, "PushHandlerService() constructor");
        }

        #endregion

        #region Override

        protected override void OnRegistered(Context context, string registrationId)
        {
            Log.Verbose(AzurePushBroadcastReceiver.TAG, "GCM Registered: " + registrationId);

            Settings.RegistrationId = registrationId;

            Hub = new NotificationHub(ApiKeys.NotificationHubName, ApiKeys.ListenConnectionString, context);
            try
            {
                Log.Error(AzurePushBroadcastReceiver.TAG, registrationId);
                Hub.UnregisterAll(registrationId);
            }
            catch (Exception ex)
            {
                Log.Error(AzurePushBroadcastReceiver.TAG, ex.Message);
            }

            //var tags = new List<string>() { "falcons" }; // create tags if you want
            var tags = new List<string>() { Settings.DeviceId };

            try
            {
                Hub.Register(registrationId, tags.ToArray());
            }
            catch (Exception ex)
            {
                Log.Error(AzurePushBroadcastReceiver.TAG, ex.Message);
            }
        }

        protected override void OnMessage(Context context, Intent intent)
        {
            // Solamente puede recibir notificaciones si está confirmado (login).
            if (Settings.Confirmado)
            {
                var msg = new StringBuilder();

                if (intent != null && intent.Extras != null)
                {
                    foreach (var key in intent.Extras.KeySet())
                        msg.AppendLine(key + "=" + intent.Extras.Get(key));

                    var messageText = intent.Extras.GetString("message");
                    var titleText = intent.Extras.GetString("title");
                    var notificacion = intent.Extras.GetString("notificacion");
                    var appName = intent.Extras.GetString("appName");
                    var pageToOpen = intent.Extras.GetString("pageToOpen");
                    var parameters = intent.Extras.GetString("parameters");

                    if (appName == "homie_proveedor")
                    {
                        if (!string.IsNullOrEmpty(messageText) && !string.IsNullOrWhiteSpace(titleText))
                        {
                            CreateNotification(titleText, messageText, notificacion, pageToOpen, parameters);
                        }

                        Intent notificationMessageIntent = new Intent(this, typeof(NotificationMessageService));
                        notificationMessageIntent.PutExtra("pageToOpen", pageToOpen);
                        notificationMessageIntent.PutExtra("parameters", parameters);
                        notificationMessageIntent.PutExtra("title", titleText);
                        notificationMessageIntent.PutExtra("message", messageText);
                        StartService(notificationMessageIntent);
                        //StartForegroundService(notificationMessageIntent); //https://developer.android.com/about/versions/oreo/android-8.0-changes

                        Intent badgeIntent = new Intent(this, typeof(BadgeService));
                        badgeIntent.PutExtra("pageToOpen", pageToOpen);
                        StartService(badgeIntent);
                        //StartForegroundService(badgeIntent);
                        
                        StopService(new Intent(this, typeof(NotificationMessageService)));
                    }
                }
            }
        }

        protected override void OnUnRegistered(Context context, string registrationId)
        {
            Log.Verbose(AzurePushBroadcastReceiver.TAG, "GCM Unregistered: " + registrationId);
        }

        protected override bool OnRecoverableError(Context context, string errorId)
        {
            Log.Warn(AzurePushBroadcastReceiver.TAG, "Recoverable Error: " + errorId);

            return base.OnRecoverableError(context, errorId);
        }

        protected override void OnError(Context context, string errorId)
        {
            Log.Error(AzurePushBroadcastReceiver.TAG, "GCM Error: " + errorId);
        }

        #endregion

        #region Methods

        void CreateNotification(string title, string desc, string notificacion, string pageToOpen, string parameters)
        {
            //Create notification
            //var notificationManager = GetSystemService(Context.NotificationService) as NotificationManager;

            //Create an intent to show UI
            //var uiIntent = new Intent(this, typeof(MainActivity));
            //uiIntent.PutExtra("notificacion", notificacion);
            //uiIntent.PutExtra("pageToOpen", pageToOpen);
            //uiIntent.PutExtra("parameters", parameters);

            ////Create the notification
            ////var notification = new Notification(Android.Resource.Drawable.SymActionEmail, title);
            //var notification = new Notification(Resource.Drawable.icon, title);

            ////Auto-cancel will remove the notification once the user touches it
            //notification.Defaults = NotificationDefaults.All; 
            //notification.Flags = NotificationFlags.AutoCancel;

            //Set the notification info
            //we use the pending intent, passing our ui intent over, which will get called
            //when the notification is tapped.
            //notification.SetLatestEventInfo(this, title, desc, PendingIntent.GetActivity(this, 0, uiIntent, PendingIntentFlags.UpdateCurrent));

            //notification.SetLatestEventInfo(this, title, desc, PendingIntent.GetActivity(this, 0, uiIntent, PendingIntentFlags.Immutable));


            ////Show the notification
            //notificationManager.Notify(0, notification);








            var intent = new Intent(this, typeof(MainActivity));
            intent.PutExtra("notificacion", notificacion);
            intent.PutExtra("pageToOpen", pageToOpen);
            intent.PutExtra("parameters", parameters);
            intent.AddFlags(ActivityFlags.ClearTop);

            Random random = new Random();
            int pushCount = 0;

            if (pageToOpen != HomieProveedorApp.Entities.NotificacionPage.Chat)
            {
                pushCount = random.Next(9999 - 1000) + 1000; //for multiplepushnotifications
            }

            var pendingIntent = PendingIntent.GetActivity(this, pushCount, intent, PendingIntentFlags.UpdateCurrent);

            var notificationBuilder = new Notification.Builder(this)
                .SetSmallIcon(Resource.Drawable.icon)
                .SetContentTitle(title)
                .SetContentText(desc)
                .SetAutoCancel(true)
                //.SetDefaults(NotificationDefaults.Sound)
                .SetContentIntent(pendingIntent);

            var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(pushCount, notificationBuilder.Build());
        }

        #endregion
    }*/
}