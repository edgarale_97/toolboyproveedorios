﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HomieProveedorApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using HomieProveedorApp.Views.Common;

[assembly: ExportRenderer(typeof(LetterSpacingLabel), typeof(LetterSpacingLabelRenderer))]
namespace HomieProveedorApp.Droid.Renderers
{
    public class LetterSpacingLabelRenderer : LabelRenderer
    {
        protected LetterSpacingLabel LetterSpacingLabel { get; private set; }

        #region -- Overrides --

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                this.LetterSpacingLabel = (LetterSpacingLabel)this.Element;
            }

            var letterSpacing = this.LetterSpacingLabel.LetterSpacing;
            this.Control.LetterSpacing = letterSpacing;

            this.UpdateLayout();
        }

        #endregion
    }
}