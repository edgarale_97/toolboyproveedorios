﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using FFImageLoading.Forms.Touch;
using Foundation;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.iOS.Helpers;
using HomieProveedorApp.ViewModel;
using ImageCircle.Forms.Plugin.iOS;
using KeyboardOverlap.Forms.Plugin.iOSUnified;
using Lottie.Forms.iOS.Renderers;
using Plugin.DeviceInfo;
using Plugin.VersionTracking;
using System;
using System.Collections.Generic;
using UIKit;
using Xamarin.Forms;

[assembly: ResolutionGroupName("HomieProveedorApp")]
namespace HomieProveedorApp.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //private SBNotificationHub Hub { get; set; }
        public static LocationManager Manager = null;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            // Initializing FFImageLoading
            CachedImageRenderer.Init();

            // Color negro para el Switch
            UISwitch.Appearance.OnTintColor = UIColor.FromRGB(0, 169, 157);

            global::Xamarin.Forms.Forms.Init();

            //Xamarin.FormsGoogleMaps.Init(HomieProveedorApp.Helpers.ApiKeys.iOSGoogleMapsKey);
            //ImageCircleRenderer.Init();
            //CrossVersionTracking.Current.Track();
            //CachedImageRenderer.Init(); //Initializing FFImageLoading
            ////FFImageLoading.Forms.Platform.CachedImageRenderer.Init();

            OneSignal.Current.StartInit("103fa994-5f04-46d6-8748-c7d1f2f49428").Settings(new Dictionary<string, bool>() {
                { IOSSettings.kOSSettingsKeyAutoPrompt, false },
                { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } }).HandleNotificationOpened(onHandleNotificationOpened).HandleNotificationReceived(onHandleNotificationReceived).
                InFocusDisplaying(OSInFocusDisplayOption.Notification).EndInit();

            OneSignal.Current.RegisterForPushNotifications();

            OneSignal.Current.IdsAvailable(IdsAvailable);

            OneSignal.Current.SendTag("deviceId", Settings.DeviceId);

            Xamarin.FormsGoogleMaps.Init(HomieProveedorApp.Helpers.ApiKeys.iOSGoogleMapsKey);
            ImageCircleRenderer.Init();
            CrossVersionTracking.Current.Track();
            CachedImageRenderer.Init(); //Initializing FFImageLoading
            AnimationViewRenderer.Init(); //Initializing Lottie

            HomieProveedorApp.Helpers.Settings.Platform = (int)Entities.Platform.iOS;


            // Para hacerlo funcionar tuve que habilitar Google Places API Web Service.
            HomieProveedorApp.Helpers.Settings.GooglePlacesApiKey = HomieProveedorApp.Helpers.ApiKeys.iOSGooglePlacesApiKey;

            AnimationViewRenderer.Init(); //Initializing Lottie

            Settings.Platform = (int) Entities.Platform.iOS;

            if (string.IsNullOrWhiteSpace(Settings.DeviceId))
            {
                Settings.Confirmado = false;
            }

            LoadApplication(new App());

            KeyboardOverlapRenderer.Init();

            //if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            //{
            //    var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, new NSSet());
            //    UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
            //    UIApplication.SharedApplication.RegisterForRemoteNotifications();
            //}
            //else
            //{
            //    UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
            //    UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            //}

            //as soon as the app is done launching, begin generating location updates in the location manager
            if (Settings.ObtenerUbicacion)
            {
                Manager = new LocationManager();
                Manager.StartLocationUpdates();
            }

            MessagingCenter.Subscribe<DetalleServicioViewModel, bool>(this, "ObtenerUbicacionProveedor",
                (sender, param) =>
                {
                    if (Manager == null)
                    {
                        Manager = new LocationManager();
                    }
                    if (param)
                    {
                        Manager.StartLocationUpdates();
                    }
                    else
                    {
                        Manager.StopLocationUpdates();
                    }
                });

            ////check for remote notifications
            //if (options != null)
            //    {
            //        if (options.ContainsKey(UIApplication.LaunchOptionsRemoteNotificationKey))
            //        {
            //            NSDictionary userInfo = (NSDictionary)options[UIApplication.LaunchOptionsRemoteNotificationKey];
            //            ProcessNotification(userInfo, true);
            //        }
            //    }

            return base.FinishedLaunching(app, options);
        }

        private void IdsAvailable(string userID, string pushToken)
        {
            Settings.DeviceId = userID;
        }

        private static void onHandleNotificationReceived(OSNotification notification)
        {
            OSNotificationPayload payload = notification.payload;
            Dictionary<string, object> additionalData = payload.additionalData;
            string message = payload.body;
            if (additionalData != null)
            {
                if (additionalData.ContainsKey("pageToOpen"))
                {

                    var notificacion = new HomieProveedorApp.Entities.Notificaciones();
                    notificacion.PageToOpen = additionalData["pageToOpen"].ToString();
                    notificacion.Parameters = additionalData["parameters"].ToString();
                    notificacion.Titulo = payload.title;
                    notificacion.Descripcion = message;


                    //if (additionalData["pageToOpen"] != HomieApp.Entities.NotificacionPage.Chat)
                    //{
                    //    HomieApp.Helpers.Settings.Notificaciones++;
                    //    CrossBadge.Current.SetBadge(HomieApp.Helpers.Settings.Notificaciones);

                    //    //UIAlertView avAlert = new UIAlertView(titleText, alert, null, "OK", null);
                    //    //avAlert.Show();
                    //}

                    // Enviamos un message para abrir las pantallas.
                    MessagingCenter.Send<App, HomieProveedorApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "HubNotification", notificacion);
                    // Take user to your store.
                }
            }
            //if (actionID != null)
            //{
            //    // actionSelected equals the id on the button the user pressed.
            //    // actionSelected will equal "__DEFAULT__" when the notification itself was tapped when buttons were present.

            //}
        }

        private static void onHandleNotificationOpened(OSNotificationOpenedResult result)
        {
            OSNotificationPayload payload = result.notification.payload;
            Dictionary<string, object> additionalData = payload.additionalData;
            string message = payload.body;
            if (additionalData != null)
            {
                if (additionalData.ContainsKey("pageToOpen"))
                {

                    var notificacion = new HomieProveedorApp.Entities.Notificaciones();
                    notificacion.PageToOpen = additionalData["pageToOpen"].ToString();
                    notificacion.Parameters = additionalData["parameters"].ToString();
                    notificacion.Titulo = payload.title;
                    notificacion.Descripcion = message;


                    //if (additionalData["pageToOpen"] != HomieApp.Entities.NotificacionPage.Chat)
                    //{
                    //    HomieApp.Helpers.Settings.Notificaciones++;
                    //    CrossBadge.Current.SetBadge(HomieApp.Helpers.Settings.Notificaciones);

                    //    //UIAlertView avAlert = new UIAlertView(titleText, alert, null, "OK", null);
                    //    //avAlert.Show();
                    //}

                    // Enviamos un message para abrir las pantallas.
                    MessagingCenter.Send<App, HomieProveedorApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "OpenFromNotificationBar", notificacion);
                    // Take user to your store.
                }
            }
            //if (actionID != null)
            //{
            //    // actionSelected equals the id on the button the user pressed.
            //    // actionSelected will equal "__DEFAULT__" when the notification itself was tapped when buttons were present.

            //}
        }

        //public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        //{
        //    byte[] result = new byte[deviceToken.Length];
        //    Marshal.Copy(deviceToken.Bytes, result, 0, (int)deviceToken.Length);
        //    var token = BitConverter.ToString(result).Replace("-", "");

        //    Settings.RegistrationId = deviceToken.GetBase64EncodedString(NSDataBase64EncodingOptions.None);

        //    Hub = new SBNotificationHub(ApiKeys.ListenConnectionString, ApiKeys.NotificationHubName);
        //    Hub.UnregisterAllAsync(token);

        //    string[] list = { Settings.DeviceId };
        //    NSSet tags = new NSSet(list);
        //    Hub.RegisterNativeAsync(token, tags);
        //}


        /*public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Settings.RegistrationId = deviceToken.GetBase64EncodedString(NSDataBase64EncodingOptions.None);

            Hub = new SBNotificationHub(ApiKeys.ListenConnectionString, ApiKeys.NotificationHubName);
            Hub.UnregisterAllAsync(deviceToken, (error) =>
            {
                if (error != null)
                {
                    Console.WriteLine("Error calling Unregister: {0}", error.ToString());
                    return;
                }

                string[] list = { Settings.DeviceId };
                NSSet tags = new NSSet(list);

                Hub.RegisterNativeAsync(deviceToken, tags, (errorCallback) =>
                {
                    if (errorCallback != null)
                        Console.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                });
            });
        }*/

        //public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        //{
        //    if (application.ApplicationState == UIApplicationState.Active)
        //    {
        //        ProcessNotification(userInfo, false);
        //    }
        //    else if (application.ApplicationState == UIApplicationState.Background)
        //    {
        //        ProcessNotification(userInfo, true);
        //    }
        //    else if (application.ApplicationState == UIApplicationState.Inactive)
        //    {
        //        ProcessNotification(userInfo, true);
        //    }
        //}

        //void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        //{
        //    // Solamente puede recibir notificaciones si está confirmado (login).
        //    if (Settings.Confirmado)
        //    {
        //        // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
        //        if (null != options && options.ContainsKey(new NSString("aps")))
        //        {
        //            //Get the aps dictionary
        //            NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

        //            string alert = string.Empty;
        //            var titleText = string.Empty;
        //            var appName = string.Empty;
        //            var pageToOpen = string.Empty;
        //            var parameters = string.Empty;

        //            if (aps.ContainsKey(new NSString("alert")))
        //            {
        //                alert = (aps[new NSString("alert")] as NSString).ToString();
        //            }
        //            if (aps.ContainsKey(new NSString("title")))
        //            {
        //                titleText = (aps[new NSString("title")] as NSString).ToString();
        //            }
        //            if (aps.ContainsKey(new NSString("appName")))
        //            {
        //                appName = (aps[new NSString("appName")] as NSString).ToString();
        //            }

        //            if (appName != "homie_proveedor")
        //            {
        //                return;
        //            }

        //            if (aps.ContainsKey(new NSString("pageToOpen")))
        //            {
        //                pageToOpen = (aps[new NSString("pageToOpen")] as NSString).ToString();
        //                parameters = (aps[new NSString("parameters")] as NSString).ToString();
        //            }

        //            var notificacion = new HomieProveedorApp.Entities.Notificaciones();
        //            notificacion.PageToOpen = pageToOpen;
        //            notificacion.Parameters = parameters;
        //            notificacion.Titulo = titleText;
        //            notificacion.Descripcion = alert;

        //            //If this came from the ReceivedRemoteNotification while the app was running,
        //            // we of course need to manually process things like the sound, badge, and alert.
        //            if (!fromFinishedLaunching)
        //            {
        //                //Manually show an alert
        //                //if (!string.IsNullOrEmpty(alert))
        //                //{
        //                    if (pageToOpen != HomieProveedorApp.Entities.NotificacionPage.Chat)
        //                    {
        //                        Settings.Notificaciones++;
        //                        CrossBadge.Current.SetBadge(Settings.Notificaciones);

        //                        UIAlertView avAlert = new UIAlertView(titleText, alert, null, "OK", null);
        //                        avAlert.Show();
        //                    }

        //                    // Enviamos un message para abrir las pantallas.
        //                    MessagingCenter.Send<App, HomieProveedorApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "HubNotification", notificacion);
        //                //}
        //            }
        //            else
        //            {
        //                Settings.Notificaciones = 0;
        //                CrossBadge.Current.SetBadge(Settings.Notificaciones);
        //                MessagingCenter.Send<App, HomieProveedorApp.Entities.Notificaciones>((App)Xamarin.Forms.Application.Current, "OpenFromNotificationBar", notificacion);
        //            }
        //        }
        //    }
        //}

        public override void OnActivated(UIApplication application)
        {
            MessagingCenter.Send<App>((App)Xamarin.Forms.Application.Current, "OnActivated");
        }

        public override void DidEnterBackground(UIApplication application)
        {
            MessagingCenter.Send<App>((App)Xamarin.Forms.Application.Current, "DidEnterBackground");
        }

        //public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        //{
        //    new UIAlertView("Registration failed", error.LocalizedDescription, null, "OK", null).Show();
        //}
    }
}
