﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreLocation;
using Foundation;
using HomieProveedorApp.Helpers;
using UIKit;
using HomieProveedorApp.iOS.Helpers;
using HomieProveedorApp.Models;

namespace HomieProveedorApp.iOS.Helpers
{
    public class LocationManager
    {
        protected CLLocationManager locMgr;
        //event for the location changing
        public event EventHandler<LocationUpdateEventArgs> LocationUpdated = delegate { };

        public LocationManager()
        {
            try
            {
                this.locMgr = new CLLocationManager();

                this.locMgr.PausesLocationUpdatesAutomatically = false;

                //iOS 8 has additional permissions requeriments
                if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                {
                    locMgr.RequestAlwaysAuthorization(); // works in background
                }
                if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
                {
                    locMgr.AllowsBackgroundLocationUpdates = true;
                }
                LocationUpdated += PrintLocation;
            }
            catch (Exception ex)
            {
                
            }
        }

        //create a location manager to get system location updates to the application
        public CLLocationManager LocMgr
        {
            get { return locMgr; }
        }

        public void StartLocationUpdates()
        {
            try
            {
                //We need the user´s permission for our app to use the GPS in iOs. This is done eigther by the user accepting
                //the popover when the app is first launched, or by changing the permissions for the app in Settings

                if (CLLocationManager.LocationServicesEnabled)
                {
                    LocMgr.DesiredAccuracy = 10; //TODO: PROBAR. sets the accuracy that we want in meters
                    LocMgr.DistanceFilter = Settings.DistanceAccuracy;
                    if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
                    {
                        LocMgr.LocationsUpdated += (sender, e) =>
                        {
                            //fire our custom Location Updated event
                            this.LocationUpdated(this, new LocationUpdateEventArgs(e.Locations[e.Locations.Length - 1]));
                        };
                    }
                    else
                    {
                        //this won´t be called on iOS 6 (deprecated). We will get a warning here when we build.
                        LocMgr.UpdatedLocation += (sender, e) =>
                        {
                            this.LocationUpdated(this, new LocationUpdateEventArgs(e.NewLocation));
                        };
                    }

                    //Start our location updates
                    LocMgr.StartUpdatingLocation();

                    //Get some output from our manager in case of failure
                    LocMgr.Failed += (sender, e) =>
                    {
                        Console.WriteLine(e.Error);
                    };
                }
                else
                {
                    //Let the user know that they need to enable LocationServices
                    Console.WriteLine("Location services not enabled, please enable this in your Settings.");
                }
            }
            catch (Exception ex)
            {
                Settings.ObtenerUbicacion = false;
            }
        }

        public void StopLocationUpdates()
        {
            try
            {
                if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
                {
                    LocMgr.LocationsUpdated -= (sender, e) =>
                    {
                        //fire our custom Location Updated event
                        this.LocationUpdated(this, new LocationUpdateEventArgs(e.Locations[e.Locations.Length - 1]));
                    };
                }
                else
                {
                    // this won't be called on iOS 6 (deprecated). We will get a warning here when we build.
                    LocMgr.UpdatedLocation -= (object sender, CLLocationUpdatedEventArgs e) =>
                    {
                        this.LocationUpdated(this, new LocationUpdateEventArgs(e.NewLocation));
                    };
                }
                // Start our location updates
                LocMgr.StopUpdatingLocation();
            }
            catch (Exception ex)
            {
                Settings.ObtenerUbicacion = false;
            }
        }

        //This will keep going in the background and the foreground
        public async void PrintLocation(object sender, LocationUpdateEventArgs e)
        {
            try
            {
                if (Settings.ObtenerUbicacion)
                {
                    CLLocation location = e.Location;
                    var model = new UbicacionProveedorModel();
                    var result = await model.PostAsync(location.Coordinate.Latitude, location.Coordinate.Longitude, (int) location.Speed);
                    var test = result.Result;

                    Console.WriteLine("Altitude: " + location.Altitude + " meters");
                    Console.WriteLine("Longitude: " + location.Coordinate.Longitude);
                    Console.WriteLine("Latitude: " + location.Coordinate.Latitude);
                    Console.WriteLine("Course: " + location.Course);
                    Console.WriteLine("Speed: " + location.Speed);
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}