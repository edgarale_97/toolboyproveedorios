﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreLocation;
using Foundation;
using UIKit;

namespace HomieProveedorApp.iOS.Helpers
{
    public class LocationUpdateEventArgs : EventArgs
    {
        private CLLocation location;

        public LocationUpdateEventArgs(CLLocation location)
        {
            this.location = location;
        }

        public CLLocation Location
        {
            get { return location; }
        }
    }
}