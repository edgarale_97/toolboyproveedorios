﻿using System;
using System.ComponentModel;
using CoreGraphics;
using HomieProveedorApp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportEffect(typeof(ShadowEffect), "ShadowEffect")]
namespace HomieProveedorApp.iOS
{
    public class ShadowEffect : PlatformEffect
    {
        private CGSize _originalOffset;
        private CGColor _originalColor;
        private float _originalOpacity;
        private nfloat _originalRadius;

        protected override void OnAttached()
        {
            if (Container == null)
                return;
            _originalOffset = Container.Layer.ShadowOffset;
            _originalColor = Container.Layer.ShadowColor;
            _originalOpacity = Container.Layer.ShadowOpacity;
            _originalRadius = Container.Layer.ShadowRadius;
            UpdateShadow();
        }

        protected override void OnDetached()
        {
            if (Container == null)
                return;
            Container.Layer.ShadowColor = (_originalColor);
            Container.Layer.ShadowOffset = (_originalOffset);
            Container.Layer.ShadowOpacity = (_originalOpacity);
            Container.Layer.ShadowRadius = (_originalRadius);
        }

        protected virtual void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            OnElementPropertyChanged(args);
            if (!(args.PropertyName == HomieProveedorApp.Helpers.Effects.ShadowProperty.PropertyName) && !(args.PropertyName == HomieProveedorApp.Helpers.Effects.ShadowIOSColorProperty.PropertyName) && !(args.PropertyName == HomieProveedorApp.Helpers.Effects.ShadowSizeProperty.PropertyName))
                return;
            UpdateShadow();
        }

        private void UpdateShadow()
        {
            Container.Layer.ShadowOpacity = (1f);
            Container.Layer.ShadowColor = (HomieProveedorApp.Helpers.Effects.GetShadowIOSColor(Element).ToCGColor());
            nfloat nfloat = (nfloat)(HomieProveedorApp.Helpers.Effects.GetShadowSize(Element));
            Container.Layer.ShadowOffset = (new CGSize(0.0f, !(nfloat > 0) ? 0.0f : 1f));
            Container.Layer.ShadowRadius = (!(nfloat > 0) ? 0 : nfloat);
        }
    }
}