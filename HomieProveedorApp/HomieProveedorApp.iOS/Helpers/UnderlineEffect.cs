﻿using System.ComponentModel;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace HomieProveedorApp.iOS
{
    public class UnderlineEffect : PlatformEffect
    {
        public UnderlineEffect() : base()
        {
        }

        protected override void OnAttached()
        {
            this.Underline();
        }

        protected override void OnDetached()
        {
            Button element = this.Element as Button;
            UIButton control = this.Control as UIButton;
            if (element == null || control == null)
                return;
            NSMutableAttributedString attributedString = new NSMutableAttributedString(element.Text);
            attributedString.RemoveAttribute(UIStringAttributeKey.UnderlineStyle, new NSRange(0, attributedString.Length));
            control.SetAttributedTitle(attributedString, 0L);
        }

        protected virtual void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            OnElementPropertyChanged(args);
            if (!(args.PropertyName == Button.TextProperty.PropertyName))
                return;
            this.Underline();
        }

        private void Underline()
        {
            Button element = this.Element as Button;
            UIButton control = this.Control as UIButton;
            if (element == null || control == null)
                return;
            NSMutableAttributedString attributedString = new NSMutableAttributedString(element.Text);
            attributedString.AddAttribute(UIStringAttributeKey.UnderlineStyle, NSNumber.FromInt32(1), new NSRange(0, attributedString.Length));
            control.SetAttributedTitle(attributedString, 0L);
        }
    }
}