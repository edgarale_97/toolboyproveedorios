﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Views;
using HomieProveedorApp.Views.Common;
using HomieProveedorApp.Views.Navigation;
using Xamarin.Forms;
using Com.OneSignal;

namespace HomieProveedorApp
{
	public partial class App : Application
	{
	    public static MasterDetailPage MasterDetailPage;
	    public static Messages Messages;

        public App ()
        {
			InitializeComponent();
            Messages = new Messages();

		    Settings.MetodoPago = true;

		    SetMainPage();

            OneSignal.Current.StartInit("3a5b8c7c-c9a7-4e0a-8981-23e83f867d03").EndInit();

            MessagingCenter.Subscribe<HomePage>(this,"Login", (sender) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var login = new LoginPage();
                    login.LoginConfirmado += Login_LoginConfirmado;

                    MainPage = login;
                });
            });

		    MessagingCenter.Subscribe<HomePage>(this, "Registro", (sender) =>
		    {
		        Device.BeginInvokeOnMainThread(() =>
		        {
		            var registro = new SignUpPage();

		            MainPage = registro;
		        });
		    });

            MessagingCenter.Subscribe<HomeLabelNavigation>(this,"Home", (sender) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var home = new HomePage();
                    MainPage = home;
                });
            });

            MessagingCenter.Subscribe<App>(this,"RegistroCompletado", app =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var home = new HomePage();
                    MainPage = home;
                });
            });
		}

	    private void SetMainPage()
	    {
            if (Settings.Confirmado)
            {
                var rootPage = new RootPage();
                rootPage.CerrarSesion += Rootpage_CerrarSesion;
                MainPage = rootPage;
            }
            else
            {
                var home = new HomePage();
                MainPage = home;
            }
	        
            MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
	    }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

	    private void Rootpage_CerrarSesion(object sender, EventArgs e)
	    {
	        ((RootPage)sender).CerrarSesion -= Rootpage_CerrarSesion;
	        SetMainPage();
	    }

	    private void Login_LoginConfirmado(object sender, EventArgs e)
	    {
	        ((LoginPage)sender).LoginConfirmado -= Login_LoginConfirmado;
	        SetMainPage();
	    }
    }
}
