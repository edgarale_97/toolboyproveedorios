﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomieProveedorApp.Entities
{
    public class Alertas
    {
        public long IdAlertas { get; set; }
        public int IdTipo { get; set; }
        public long IdServicio { get; set; }
        public long IdUser { get; set; }
        public string Mensaje { get; set; }
        public string Comentarios { get; set; }
        public bool Atendido { get; set; }
        public long IdUserAtendio { get; set; }
    }
}
