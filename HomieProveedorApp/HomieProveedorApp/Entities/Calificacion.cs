﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class Calificacion
    {
        public long IdCalificacion { get; set; }
        public Nullable<long> IdCliente { get; set; }
        public Nullable<long> IdProveedor { get; set; }
        public long IdServicio { get; set; }
        public int Tipo { get; set; }
        public short CalificacionGeneral { get; set; }
        public string Observaciones { get; set; }
    }
}
