﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomieProveedorApp.Entities
{
    public class Categorias
    {
        public int IdCategoria { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Icono { get; set; }

        public int? IdCategoriaPadre { get; set; }

        [JsonIgnore]
        public bool IsVisibleFondo => IdCategoriaPadre == null;
    }
}
