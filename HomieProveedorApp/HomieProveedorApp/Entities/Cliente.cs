﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class Cliente
    {
        public long IdCliente { set; get; }
        public string Nombre { set; get; }
        public string Foto { set; get; }
    }

    public class DetalleCliente
    {
        public string Correo { get; set; }
        public int Calificacion { get; set; }
        public string NombreCompleto { get; set; }
        public string TelMovil { get; set; }
        public string Foto { get; set; }
    }
}
