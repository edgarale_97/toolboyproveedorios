﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class CreditCard
    {
        public virtual CreditCardType CardType { get; set; }

        public int ExpiryYear { get; set; }

        public string Cvv { get; set; }

        public string CardholderName { get; set; }

        public string CardNumber { get; set; }

        public int ExpiryMonth { get; set; }

        public string RedactedCardNumber { get; set; }
    }
    public enum CreditCardType
    {
        Unrecognized = 0,
        Amex = 52,
        Visa = 50,
        Mastercard = 51
    }
}
