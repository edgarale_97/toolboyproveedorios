﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class Direcciones
    {
        public long IdDireccion { get; set; }
        public string Direccion { get; set; }
        public string Nombre { get; set; }
        public bool Principal { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }

        public string TextoPrincipal => Principal ? "Principal" : string.Empty;
    }
}
