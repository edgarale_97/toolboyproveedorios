﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace HomieProveedorApp.Entities
{
    public class Garantias
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Factura { get; set; }
        public string NumeroSerie { get; set; }
        public string Tienda { get; set; }
        public string NombreVendedor { get; set; }
        public byte[] FotoEquipo { get; set; }
        public byte[] FotoFactura { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCompra { get; set; }
        public DateTime FechaVigencia { get; set; }
        public DateTime FechaInstalacion { get; set; }
        public string NombreCategoria { get; set; }
    }
}

