﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public interface IImageOverlay
    {
        Func<object> GetOverlay();
    }
}
