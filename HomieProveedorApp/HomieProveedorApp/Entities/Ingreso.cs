﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class Ingreso
    {
        public long IdServicio { get; set; }
        public DateTime Fecha { get; set; }
        public Decimal Tarifa { get; set; }
        public Decimal Materiales { get; set; }



        public Decimal Total => Tarifa + Materiales;

        public string TarifaText => Tarifa.ToString("C");
        public string MaterialesText => Materiales.ToString("C");
        public string TotalText => Total.ToString("C");

        public string FechaText
        {
            get
            {
                CultureInfo culture = CultureInfo.GetCultureInfo("es-MX");
                return IdServicio == 0 ? " " : Fecha.ToString("dd-MMMM-yyyy", culture);
            }
        }

        public string NoServicio => IdServicio == 0 ? "Gran Total" : "Servicio " + IdServicio;
    }
}
