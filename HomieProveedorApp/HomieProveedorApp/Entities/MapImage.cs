﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class MapImage
    {
        public double Latitud { get; }
        public double Longitud { get; }
        public int Zoom { get; }
        public int SizeHeight { get; }
        public int SizeWidth { get; }

        public MapImage(double latitud, double longitud)
        {
            Latitud = latitud;
            Longitud = longitud;
            Zoom = 18;
            SizeHeight = 200;
            SizeWidth = 600;
        }

        public string GetUri()
        {
            var uri = $"https://maps.googleapis.com/maps/api/staticmap?center={Latitud},{Longitud}&zoom={Zoom}&size={SizeWidth}x{SizeHeight}&maptype=roadmap&markers=icon:http://goo.gl/nYr8bk%7C{Latitud},{Longitud}&language=es&region=MX";
            return uri;
        }
    }
}
