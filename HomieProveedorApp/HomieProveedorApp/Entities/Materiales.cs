﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HomieProveedorApp.Entities
{
    public class Materiales : INotifyPropertyChanged
    {
        public long IdServicioMaterial { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal Importe { get; set; }
        public bool Aplicado { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaAplicacion { get; set; }
        public bool MaterialExtra { get; set; }
        public long IdServicio { get; set; }
        public bool Activo { get; set; }
        public bool Editable { get; set; }
        public Nullable<bool> AceptadoCliente { get; set; }

        [JsonIgnore]
        public string TextCantidad => Cantidad.ToString();
        [JsonIgnore]
        public string TextImporte => Importe.ToString("C");

        [JsonIgnore]
        public Color EstatusColor
        {
            get
            {
                if (AceptadoCliente == null)
                {
                    return Color.Black;
                }
                else if (AceptadoCliente.Value)
                {
                    return Color.DarkGreen;
                }
                else
                {
                    return Color.DarkRed;
                }
            }
        }

        #region PropertyChanged
        
        public event PropertyChangedEventHandler PropertyChanged;
        
        public void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
