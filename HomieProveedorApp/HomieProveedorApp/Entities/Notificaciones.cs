﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class Notificaciones
    {
        public long IdNotificacion { get; set; }
        public long UserId { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public bool Correo { get; set; }
        public bool App { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Imagen { get; set; }
        public string PageToOpen { get; set; }
        public string Parameters { get; set; }
        public Nullable<bool> Activa { get; set; }

        public string FechaString => Fecha.ToString("dd/MM/yyyy hh:mm tt");
    }
}
