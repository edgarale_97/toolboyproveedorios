﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HomieProveedorApp.Entities
{
    class ProfileProveedor
    {
        public Int64 Id { get; set; }
        public string Correo { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroTelefonico { get; set; }
        public string Descripcion { get; set; }
        public bool LunesDisponible { get; set; }
        public bool MartesDisponible { get; set; }
        public bool MiercolesDisponible { get; set; }
        public bool JuevesDisponible { get; set; }
        public bool ViernesDisponible { get; set; }
        public bool SabadoDisponible { get; set; }
        public bool DomingoDisponible { get; set; }
        public TimeSpan HoraInicioTrabajo { get; set; }
        public TimeSpan HoraFinTrabajo { get; set; }
        public string FotoPerfil { get; set; }
        public byte[] Foto { get; set; }
        public string Banco { get; set; }
        public string Clabe { get; set; }

        [JsonIgnore]
        public string BancoSeleccion
        {
            get
            {
                if(Banco != null)
                {
                    switch (Banco)
                    {
                        case "75":
                            return "BANAMEX ";
                            break;
                        case "76":
                            return "BANCOMEXT";
                            break;
                        case "77":
                            return "BANOBRAS";
                            break;
                        case "78":
                            return "BBVA BANCOMER";
                            break;
                        case "79":
                            return "SANTANDER";
                            break;
                        case "80":
                            return "BANJERCITO";
                            break;
                        case "81":
                            return "HSBC";
                            break;
                        case "82":
                            return "BAJIO";
                            break;
                        case "83":
                            return "IXE";
                            break;
                        case "84":
                            return "INBURSA";
                            break;
                        case "85":
                            return "INTERACCIONES";
                            break;
                        case "86":
                            return "MIFEL";
                            break;
                        case "87":
                            return "SCOTIABANK";
                            break;
                        case "88":
                            return "BANREGIO";
                            break;
                        case "89":
                            return "INVEX";
                            break;
                        case "90":
                            return "BANSI";
                            break;
                        case "91":
                            return "AFIRME";
                            break;
                        case "92":
                            return "BANORTE";
                            break;
                        case "93":
                            return "THE ROYAL BANK";
                            break;
                        case "94":
                            return "AMERICAN EXPRESS";
                            break;
                        case "95":
                            return "BAMSA";
                            break;
                        case "96":
                            return "TOKYO";
                            break;
                        case "97":
                            return "JP MORGAN";
                            break;
                        case "98":
                            return "BMONEX";
                            break;
                        case "99":
                            return "VE POR MAS";
                            break;
                        case "100":
                            return "ING";
                            break;
                        case "101":
                            return "DEUTSCHE";
                            break;
                        case "102":
                            return "AZTECA";
                            break;
                        case "103":
                            return "AUTOFIN";
                            break;
                        case "104":
                            return "BARCLAYS";
                            break;
                        case "105":
                            return "COMPARTAMOS";
                            break;
                        case "106":
                            return "BANCO FAMSA";
                            break;
                        case "107":
                            return "BMULTIVA";
                            break;
                        case "108":
                            return "ACTINVER";
                            break;
                        case "109":
                            return "WAL-MART";
                            break;
                        case "110":
                            return "NAFIN";
                            break;
                        case "111":
                            return "INTERBANCO";
                            break;
                        case "112":
                            return "BANCOPPEL";
                            break;
                        case "113":
                            return "ABC CAPITAL";
                            break;
                        case "114":
                            return "UBS BANK";
                            break;
                        case "115":
                            return "CONSUBANCO";
                            break;
                        case "116":
                            return "VOLKSWAGEN";
                            break;
                        case "117":
                            return "CIBANCO";
                            break;
                    }
                }
                return "-- Selecciona tu Banco --";
            }
        }
    }
}

