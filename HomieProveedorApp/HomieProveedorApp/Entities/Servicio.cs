﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace HomieProveedorApp.Entities
{
    public class Servicio
    {
        public long IdServicio { get; set; }
        public long IdCliente { get; set; }
        public int IdTipo { get; set; }
        public System.DateTime FechaProgramado { get; set; }
        public System.TimeSpan InicioProgramado { get; set; }
        public System.TimeSpan FinProgramado { get; set; }
        public string Direccion { get; set; }
        public string DetalleDireccion { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public Nullable<long> IdDireccion { get; set; }
        public string Foto { get; set; }
        public string Video { get; set; }
        public int Estatus { get; set; }

        public decimal MontoTarifa { get; set; }
        public int IdCategoria { get; set; }

        public long IdProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public int EstatusProveedor { get; set; }

        public string IconoCategoria { get; set; }
        public string NombreCategoria { get; set; }

        public bool ChatHabilitado { get; set; }
        public List<Categorias> Categorias { get; set; }

        public List<Materiales> Materiales { get; set; }

        public string Detalle { get; set; }

        public string NombreCliente { get; set; }

        public string TipoString
        {
            get { return IdTipo == TipoServicio.Agendado ? "Agendado" : "Urgente"; }
        }

        public string EstatusString
        {
            get
            {
                switch (Estatus)
                {
                    case ServicioEstatus.SolicitudAceptada:
                        return "Se hizo match con el proveedor";
                    case ServicioEstatus.AceptadoCliente:
                        return "Aceptado por el Cliente";
                    case ServicioEstatus.Confirmado:
                        return "Servicio Confirmado";
                    case ServicioEstatus.Iniciado:
                        return "Servicio Iniciado";
                    case ServicioEstatus.Terminado:
                        return "Servicio Terminado";
                    case ServicioEstatus.Cancelado:
                        return "Servicio Cancelado";
                    default:
                        return "";
                }
            }
        }


        public string EstatusProveedorString
        {
            get
            {
                switch (EstatusProveedor)
                {
                    case ServicioEstatus.AceptadoCliente:
                        return "Aceptado por el Cliente";
                    case ServicioEstatus.Confirmado:
                        return "Servicio Confirmado";
                    case ServicioEstatus.Iniciado:
                        return "Servicio Iniciado";
                    case ServicioEstatus.Terminado:
                        return "Servicio Terminado";
                    case ServicioEstatus.Cancelado:
                        return "Servicio Cancelado";
                    default:
                        return "";
                }
            }
        }

        [JsonIgnore]
        public string FechaString
        {
            get
            {
                var format = new CultureInfo("es-MX");
                return FechaProgramado.ToString(format.DateTimeFormat.ShortDatePattern);
            }
        }

        [JsonIgnore]
        public string MapImage
        {
            get
            {
                var map = new MapImage((double)Latitud, (double)Longitud);
                return map.GetUri();
            }
        }

        [JsonIgnore]
        public List<Cargo> Cargos
        {
            get
            {
                var cargo = new List<Cargo>();

                cargo.Add(new Cargo
                {
                    Descripcion = "Tarifa",
                    Importe = MontoTarifa
                });

                if (Materiales != null && Materiales.Count > 0)
                {
                    //TODO: Quitar el where si se quiere mostrar el descuento
                    Materiales.Where(x => x.IdServicioMaterial != 0).ToList().ForEach(x =>
                    {
                        cargo.Add(new Cargo
                        {
                            Descripcion = x.Descripcion,
                            Importe = x.Importe
                        });
                    });
                }

                decimal total = 0;
                cargo.ForEach(x =>
                {
                    total += x.Importe;
                });

                cargo.Add(new Cargo
                {
                    Descripcion = "Total",
                    Importe = total
                });

                return cargo;
            }
        }
    }
}
