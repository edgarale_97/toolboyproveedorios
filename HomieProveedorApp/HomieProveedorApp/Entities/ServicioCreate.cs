﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class ServicioCreate
    {
        public int IdTipo { get; set; }
        public int IdCategoria { get; set; }

        public string Descripcion { get; set; }
        public byte[] FotoBytes { get; set; }

        public DateTime Fecha { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }

        public byte[] FotoDocumentacionInicialBytes { get; set; }
        public string ComentariosDocumentacionInicial { get; set; }

        public byte[] FotoDocumentacionFinalBytes { get; set; }
        public string ComentariosDocumentacionFinal { get; set; }

        public DateTime FechaInicialReal { get; set; }
        public DateTime FechaFinalReal { get; set; }

        public List<Materiales> Materiales { get; set; }
    }
}
