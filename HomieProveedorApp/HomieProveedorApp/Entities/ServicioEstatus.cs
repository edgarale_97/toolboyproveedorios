﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class ServicioEstatus
    {
        public const int Solicitado = 20;
        public const int SolicitudAceptada = 21;
        public const int SolicitudDenegada = 22;
        public const int AceptadoCliente = 23;
        public const int Confirmado = 25;
        public const int Iniciado = 26;
        public const int Terminado = 27;
        public const int Cancelado = 28;
    }
    public class TipoServicio
    {
        public const int Urgente = 67;
        public const int Agendado = 68;
    }

    public class EstatusServicioCliente
    {
        public const int Creado = 13;       // Cliente crea servicio
        public const int PorIniciar = 15;   // El proveedor acepta la solicitud
        public const int Confirmado = 14;   // Cliente Acepta la cotización
        public const int Iniciado = 16;     // El proveedor inica el servicio
        public const int Terminado = 17;
        public const int Cancelado = 18;
    }

    public class EstatusMateriales
    {
        public const int Ninguno = 0;
        public const int Primera = 1;
        public const int Segunda = 2;
        public const int Tercera = 3;
    }

}
