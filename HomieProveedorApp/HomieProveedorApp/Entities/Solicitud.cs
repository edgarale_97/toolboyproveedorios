﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomieProveedorApp.Entities
{
    public class Solicitud
    {
        public long IdSolicitud { set; get; }
        public long IdServicio { set; get; }
        public string TipoServicio { get; set; }
        public List<Categorias> Categorias { get; set; }
        public string Direccion { get; set; }
        public string FechaYHora { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public string Detalle { get; set; }
        public string DetalleDireccion { get; set; }
        public string Foto { get; set; }

        [JsonIgnore]
        public string MapImage
        {
            get
            {
                var map = new MapImage(Latitud, Longitud);
                return map.GetUri();
            }
        }
    }
}
