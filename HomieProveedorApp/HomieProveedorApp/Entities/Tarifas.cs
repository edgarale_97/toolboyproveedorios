﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    public class Tarifas
    {
        public int IdTarifa { get; set; }
        public int IdCategoria { get; set; }
        public int IdArea { get; set; }
        public decimal Monto { get; set; }
        public bool Activa { get; set; }

        public string MontoString => Monto.ToString("C");
    }
}
