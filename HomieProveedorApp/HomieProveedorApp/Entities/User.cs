﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Entities
{
    class User
    {
        public string Correo { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroTelefonico { get; set; }
        public bool LunesDisponible { get; set; }
        public bool MartesDisponible { get; set; }
        public bool MiercolesDisponible { get; set; }
        public bool JuevesDisponible { get; set; }
        public bool ViernesDisponible { get; set; }
        public bool SabadoDisponible { get; set; }
        public bool DomingoDisponible { get; set; }
        public TimeSpan HoraInicioTrabajo { get; set; }
        public TimeSpan HoraFinTrabajo { get; set; }
        public string Descripcion { get; set; }
        public byte[] FotoPerfil { get; set; }
        public byte[] FotoTrabajo1 { get; set; }
        public byte[] FotoTrabajo2 { get; set; }
        public byte[] FotoTrabajo3 { get; set; }
        public string Banco { get; set; }
        public string Clabe { get; set; }
    }
}
