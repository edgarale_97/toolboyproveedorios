﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomieProveedorApp.Helpers
{
    public enum BorderStyle
    {
        Default,
        None,
        BottomLine,
        Rect,
        RoundRect,
    }
}
