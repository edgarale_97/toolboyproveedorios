﻿using System;
using System.Collections.Generic;
using System.Text;
using HomieProveedorApp.Entities;
using Xamarin.Forms;

namespace HomieProveedorApp.Helpers
{
    class ChatDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate LeftChatTemplate { get; set; }
        public DataTemplate RightChatTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return((ServicioChat) item).TipoEnvio == 1 ? LeftChatTemplate : RightChatTemplate;
        }
    }
}
