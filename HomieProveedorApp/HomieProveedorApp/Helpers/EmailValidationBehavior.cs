﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace HomieProveedorApp.Helpers
{
    public class EmailValidationBehavior : Behavior<Entry>
    {
        const string emailRegex = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                   + "@"
                                   + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";

        
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.Unfocused += UnfocusedEvent;
            base.OnAttachedTo(bindable);
        }

        void UnfocusedEvent(object sender, FocusEventArgs e)
        {
            bool IsValid = false;
            if(((Entry)sender).Text != null)
            {
                IsValid = (Regex.IsMatch(((Entry)sender).Text, emailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            }
            else
            {
                ((Entry)sender).Text = "";
            }
            if (!IsValid)
            {
                if (((Entry)sender).Text.Length > 0)
                {
                    ((Entry)sender).Text = "";
                }
                else 
                {
                    ((Entry)sender).Text = "";
                }

            }
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.Unfocused -= UnfocusedEvent;
            base.OnDetachingFrom(bindable);
        }
    }
}
