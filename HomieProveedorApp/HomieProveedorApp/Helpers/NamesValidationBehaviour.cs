﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace HomieProveedorApp.Helpers
{
    public class NamesValidationBehavior : Behavior<Entry>
    {
        const string namesRegex = @"^[a-zA-ZÀ-ÿ]+$";
        
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;
            base.OnAttachedTo(bindable);
        }

        void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            bool IsValid = false;
            IsValid = Regex.IsMatch(e.NewTextValue, namesRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            if (!IsValid)
            {
                if (((Entry)sender).Text.Length > 0)
                {
                    ((Entry)sender).Text = ((Entry)sender).Text.Remove(((Entry)sender).Text.Length - 1);
                }
                else 
                {
                    ((Entry)sender).Text = "";
                }

            }
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;
            base.OnDetachingFrom(bindable);
        }
    }
}
