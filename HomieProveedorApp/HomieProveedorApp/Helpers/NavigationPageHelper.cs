﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HomieProveedorApp.Helpers
{
    class NavigationPageHelper
    {
        public static NavigationPage Create(Page page)
        {
            //return new NavigationPage(page) { BarTextColor = Color.White, BarBackgroundColor = Color.Black};
            return new NavigationPage(page) { BarTextColor = Color.White, BarBackgroundColor = Color.FromHex("#73CFB6") };
        }
    }
}
