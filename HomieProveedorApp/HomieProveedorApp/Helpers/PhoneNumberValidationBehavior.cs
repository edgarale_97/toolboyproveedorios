﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace HomieProveedorApp.Helpers
{
    public class PhoneNumberValidationBehavior : Behavior<Entry>
    {

        const string phoneNumberRegex = @"^[0-9#+*]*$";

        protected override void OnAttachedTo(Entry entry)
        {
            entry.TextChanged += OnEntryTextChanged;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.TextChanged -= OnEntryTextChanged;
            base.OnDetachingFrom(entry);
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            int result;

            bool isValid = (Regex.IsMatch(args.NewTextValue, phoneNumberRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

            if (!isValid)
            {
                if (((Entry)sender).Text.Length > 0)
                {
                    ((Entry)sender).Text = ((Entry)sender).Text.Remove(((Entry)sender).Text.Length - 1);
                }
                else
                {
                    ((Entry)sender).Text = "";
                }
                

            }
            if(((Entry)sender).Placeholder == "Teléfono Móvil")
            {
                if (((Entry)sender).Text.Length > 15)
                {
                    ((Entry)sender).Text = ((Entry)sender).Text.Remove(((Entry)sender).Text.Length - 1);
                }
            }
            else
            {
                if (((Entry)sender).Text.Length > 20)
                {
                    ((Entry)sender).Text = ((Entry)sender).Text.Remove(((Entry)sender).Text.Length - 1);
                }
            }
            
        }
    }
}
