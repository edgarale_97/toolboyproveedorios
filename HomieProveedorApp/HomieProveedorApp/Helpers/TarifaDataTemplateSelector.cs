﻿using System;
using System.Collections.Generic;
using System.Text;
using HomieProveedorApp.Entities;
using Xamarin.Forms;

namespace HomieProveedorApp.Helpers
{
    public class TarifaDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate MontoTemplate { get; set; }
        public DataTemplate TotalTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((Cargo)item).Descripcion.Contains("Total") ? TotalTemplate : MontoTemplate;
        }
    }
}
