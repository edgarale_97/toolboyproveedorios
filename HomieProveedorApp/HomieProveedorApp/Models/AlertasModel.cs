﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    public class AlertasModel
    {
        private const string ApiControllerName = "AlertasApi";

        public async Task<RootObject<int>> PostAsync(long idServicio)
        {
            var modelBase = new ModelBase<int>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio }
            };
            return await modelBase.PostAsync(null, parameters);
        }
    }
}
