﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    public class ArchivosModel
    {
        private const string ApiControllerName = "ChatApi";

        public async Task<RootObject<List<Archivo>>> GetByIdAsync(long idServicio)
        {
            var modelBase = new ModelBase<List<Archivo>>(ApiControllerName);

            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio }
            };

            return await modelBase.GetAsync(parameters);
        }
    }
}
