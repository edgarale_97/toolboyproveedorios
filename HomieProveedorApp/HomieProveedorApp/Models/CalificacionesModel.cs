﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    public class CalificacionesModel
    {
        private const string ApiControllerName = "CalificarClienteApi";

        public async Task<RootObject<CalificacionData>> GetAsync()
        {
            var modelBase = new ModelBase<CalificacionData>(ApiControllerName);
            return await modelBase.GetAsync(new Dictionary<string, object>());
        }

        public async Task<RootObject<bool>> PostAsync(Calificacion calificacion)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);
            return await modelBase.PostAsync(calificacion);
        }
    }
}
