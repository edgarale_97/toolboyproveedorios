﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class ChangePasswordModel
    {
        private const string ApiControllerName = "ChangePasswordApi";

        public async Task<RootObject<bool>> PostAsync(string password)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);
            return await modelBase.PostAsync(new { password });
        }
    }
}
