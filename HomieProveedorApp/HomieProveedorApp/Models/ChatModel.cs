﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class ChatModel
    {
        private const string ApiControllerName = "ServicioChat";

        public async Task<RootObject<ServicioChat>> PostAsync(long idServicio, string mensaje, long idCliente,
            byte[] imagenBytes, byte[] videoBytes, bool notificar = true)
        {
            var modelBase = new ModelBase<ServicioChat>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(notificar), notificar }
            };
            return await modelBase.PostAsync(new
            {
                Mensaje = mensaje,
                IdServicio = idServicio,
                IdCliente = idCliente,
                ImagenBytes = imagenBytes,
                VideoBytes = videoBytes,
                TipoEnvio = 2
            }, parameters);
        }

        public async Task<RootObject<List<ServicioChat>>> GetAsycn(long idServicio, long lastIdServicioChat)
        {
            var modelBase = new ModelBase<List<ServicioChat>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {"appName","homie_proveedor" },
                {nameof(idServicio),idServicio },
                {nameof(lastIdServicioChat),lastIdServicioChat }
            };
            return await modelBase.GetAsync(parameters);
        }
    }
}

