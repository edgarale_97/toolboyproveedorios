﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using Xamarin.Forms;

namespace HomieProveedorApp.Models
{
    class DetalleClienteModel
    {
        private const string ApiControllerName = "ClienteApi";

        public async Task<RootObject<DetalleCliente>> GetByIdAsync(long idCliente)
        {
            var modelBase = new ModelBase<DetalleCliente>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idCliente), idCliente }
            };
            return await modelBase.GetAsync(parameters);
        }
    }
}

