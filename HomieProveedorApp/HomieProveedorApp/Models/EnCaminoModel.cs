﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class EnCaminoModel
    {
        private const string ApiControllerName = "EnCaminoApi";

        public async Task<RootObject<int>> PostAsync(long idServicio)
        {
            var modelBase = new ModelBase<int>(ApiControllerName);
            var param = new Dictionary<string, object>
            {
                {nameof(idServicio),idServicio }    
            };
            return await modelBase.PostAsync(null, param);
        }
    }
}
