﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class IngresosModel
    {
        private const string ApiControllerName = "ProveedorApi";

        public async Task<RootObject<List<Ingreso>>> GetAsync(DateTime fechaInicio, DateTime fechaFinal)
        {
            var baseModel = new ModelBase<List<Ingreso>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(fechaInicio), fechaInicio.ToString("yyyy-MM-dd") },
                {nameof(fechaFinal), fechaFinal.ToString("yyyy-MM-dd") }
            };
            return await baseModel.GetAsync(parameters);
        }
    }
}
