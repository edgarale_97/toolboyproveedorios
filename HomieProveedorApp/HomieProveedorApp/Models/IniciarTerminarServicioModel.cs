﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class IniciarTerminarServicioModel
    {
        private const string ApiNameController = "IniciarTerminarServicioApi";

        public async Task<RootObject<int>> PostAsync(long idServicio, int idEstatusProveedorServicio,
            ServicioCreate servicio)
        {
            var model = new ModelBase<int>(ApiNameController);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio },
                {nameof(idEstatusProveedorServicio), idEstatusProveedorServicio }
            };
            return await model.PostAsync(servicio, parameters);
        }

        public async Task<RootObject<int>> PostDemora(long idServicio, int idEstatusProveedorServicio, ServicioCreate servicio)
        {
            var model = new ModelBase<int>("DemoraApi");
            var parameters = new Dictionary<string, object>
            {
                { nameof(idServicio), idServicio },
                {nameof(idEstatusProveedorServicio), idEstatusProveedorServicio }
            };
            return await model.PostAsync(servicio, parameters);
        }
    }
}
