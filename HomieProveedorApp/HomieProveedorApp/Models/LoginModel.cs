﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using Newtonsoft.Json;
using Plugin.DeviceInfo;

namespace HomieProveedorApp.Models
{
    class LoginModel
    {
        public async Task<RootObject<int>> AutenticarUsuarioAsync(string usuario, string contrasena)
        {
            Settings.User = usuario;
            Settings.Password = contrasena;

            var res = new RootObject<int>();
            var client = new HttpClient();
            var param = string.Format("username={0}&password={1}&grant_type={2}&user_type={3}&platform={4}&registrationId={5}&deviceId={6}&model={7}&version={8}",
                usuario,
                contrasena,
                "password",
                "proveedor",
                Settings.Platform,
                Settings.RegistrationId,
                Settings.DeviceId,
                CrossDeviceInfo.Current.Model,
                CrossDeviceInfo.Current.Version
                );

            try
            {
                var content = new StringContent(param, Encoding.UTF8, "application/x-www-form-urlencoded");

                var uri = new Uri(Settings.TokenUri);
                var response = await client.PostAsync(uri, content);
                var x = response;

                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    var token = JsonConvert.DeserializeObject<TokenResult>(body);

                    if (token != null)
                    {
                        Settings.Token = token.AccessToken;
                        Settings.User = token.UserName;
                        Settings.Password = contrasena;
                        Settings.FechaExpiracionToken = token.Expires;
                        Settings.MetodoPago = token.MetodoPago;
                    }
                }
                else
                {
                    res.Error = true;
                    res.ErrorDescription = "El correo o la contraseña son inválidos";
                }
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.ErrorDescription = ex.ToString().Contains("ConnectFailure") || ex.ToString().Contains("NameResolutionFailure")
                    ? Messages.ConnectFailure
                    : ex.Message;
            }

            return res;
        }


        public async Task<RootObject<bool>> LogOutAsync()
        {
            var res = new RootObject<bool>();
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Settings.Token);

            try
            {
                var uri = string.Format("{0}{1}?deviceId={2}", Settings.ApiUri, "LogOutApi", Settings.DeviceId);
                var response = await client.PostAsync(uri, null);

                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    res = JsonConvert.DeserializeObject<RootObject<bool>>(body);
                }
                else
                {
                    res.Error = true;
                    res.ErrorDescription = Messages.SignUpError;
                }

                Settings.User = string.Empty;
                Settings.Password = string.Empty;
                Settings.Token = string.Empty;
                Settings.Confirmado = false;
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.ErrorDescription = ex.ToString().Contains("ConnectFailure") || ex.ToString().Contains("NameResolutionFailure")
                    ? Messages.ConnectFailure
                    : ex.Message;
            }
            return res;
        }
    }
}
