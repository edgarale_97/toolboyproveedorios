﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class MaterialesModel
    {
        private const string ApiControllerName = "MaterialesProveedorApi";

        //Obtiene la lista de materiales de un servicio.
        public async Task<RootObject<List<Materiales>>> GetAsync(long idServicio)
        {
            var modelBase = new ModelBase<List<Materiales>>(ApiControllerName);
            var param = new Dictionary<string,object>
            {
                {nameof(idServicio),idServicio }
            };
            return await modelBase.GetAsync(param);
        }

        //Inserta o actualiza un material (para actualizar mandar el idServicioMaterial != 0)
        public async Task<RootObject<long>> PostAsync(Materiales material)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            var m = new
            {
                material.IdServicioMaterial,
                material.Descripcion,
                material.Cantidad,
                material.Importe,
                material.Aplicado,
                material.FechaCreacion,
                material.FechaAplicacion,
                material.MaterialExtra,
                material.IdServicio
            };
            return await modelBase.PostAsync(m);
        }

        //Elimina un material
        public async Task<RootObject<long>> DeleteAsync(long idMaterial)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            var param = new Dictionary<string, object>
            {
                {nameof(idMaterial),idMaterial }
            };
            return await modelBase.DeleteAsync(param);
        }
    }
}
