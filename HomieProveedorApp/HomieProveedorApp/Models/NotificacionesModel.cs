﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class NotificacionesModel
    {
        private const string ApiControllerName = "NotificacionesApi";

        public async Task<RootObject<List<Notificaciones>>> GetListAsync(int page = 0, int pageSize = 10)
        {
            var modelBase = new ModelBase<List<Notificaciones>>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(page), page},
                {nameof(pageSize), pageSize}
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<bool>> DeleteAsync(long idNotificacion)
        {
            var modelBase = new ModelBase<bool>(ApiControllerName);
            var parameters = new Dictionary<string, object>();
            parameters.Add(nameof(idNotificacion), idNotificacion);
            return await modelBase.DeleteAsync(parameters);
        }
    }
}
