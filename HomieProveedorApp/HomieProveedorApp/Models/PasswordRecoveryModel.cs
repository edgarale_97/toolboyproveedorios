﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using Xamarin.Forms;

namespace HomieProveedorApp.Models
{
    class PasswordRecoveryModel
    {
        private const string ApiControllerName = "ResetPasswordApi";

    public async Task<RootObject<bool>> PostAsync(string email, string phone)
    {
        var modelBase = new ModelBase<bool>(ApiControllerName);
        var parameters = new Dictionary<string, object>();
        parameters.Add("email", email);
        parameters.Add("phone", phone);

        return await modelBase.PostAsync(parameters, null);
    }

}
}

