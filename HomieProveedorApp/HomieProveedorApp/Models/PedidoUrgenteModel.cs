﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    public class PedidoUrgenteModel
    {
        private const string ApiControllerName = "PedidoUrgenteApi";

        public async Task<RootObject<long>> PostAsync(bool habilitar)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);

            return await modelBase.PostAsync(null,
                new Dictionary<string, object>
                {
                    { nameof(habilitar), habilitar }
                });
        }
    }
}
