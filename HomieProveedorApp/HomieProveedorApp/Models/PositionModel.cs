﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace HomieProveedorApp.Models
{
    public class PositionModel
    {
        public async Task<Position> GetPositionAsync()
        {
            var locator = CrossGeolocator.Current;
            Position position;
            try
            {
                position = await locator.GetPositionAsync(10000);
                if (position == null)
                {
                    return new Position
                    {
                        Latitude = 0,
                        Longitude = 0
                    };
                }
                return position;
            }
            catch (Exception e)
            {
                return new Position
                {
                    Latitude = 0,
                    Longitude = 0
                };
            }
        }
    }
}
