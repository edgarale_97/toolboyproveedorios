﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class ProfileModel
    {
        private const string ApiControllerName = "ProveedorProfileApi";

        public async Task<RootObject<long>> PostAsync(ProfileProveedor profile)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            return await modelBase.PostAsync(profile);
        }

        public async Task<RootObject<ProfileProveedor>> GetAsync()
        {
            var modelBase = new ModelBase<ProfileProveedor>(ApiControllerName);
            var parameters = new Dictionary<string, object>();

            return await modelBase.GetAsync(parameters);
        }

    }
}

