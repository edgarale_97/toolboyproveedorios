﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace HomieProveedorApp.Models
{
    public class SenderModel<T>
    {
        public string ApiControllerName { get; }
        public long MaxResponseContentBufferSize { get; }

        public SenderModel(string apiControllerName, long maxResponseContentBufferSize = 256000)
        {
            ApiControllerName = apiControllerName;
            MaxResponseContentBufferSize = maxResponseContentBufferSize;
        }

        public async Task<RootObject<T>> PostAsync(object data, Dictionary<string, object> parameters = null)
        {
            var res = new RootObject<T>();
            var client = new HttpClient
            {
                MaxResponseContentBufferSize = MaxResponseContentBufferSize
            };

            var json = JsonConvert.SerializeObject(data, Formatting.Indented, new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.Local
            });

            try
            {
                var queryString = parameters == null ? string.Empty : string.Join("&", parameters.Select(kvp => $"{kvp.Key}={kvp.Value}"));
                var uri = string.IsNullOrWhiteSpace(queryString)
                    ? new Uri($"{Settings.SenderSMS}{ApiControllerName}")
                    : new Uri($"{Settings.SenderSMS}{ApiControllerName}?{queryString}");

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    res = JsonConvert.DeserializeObject<RootObject<T>>(body);
                }
                else
                {
                    res.Error = false;
                    res.ErrorDescription = Messages.ProfileError;
                }
            }
            catch (Exception ex)
            {
                res.Error = true;
                res.ErrorDescription = ex.ToString().Contains("ConnectFailure") || ex.ToString().Contains("NameResolutionFailure")
                    ? Messages.ConnectFailure
                    : ex.Message;
            }

            return res;

        }

    }
}

