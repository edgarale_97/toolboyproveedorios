﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class ServiciosModel
    {
        private const string ApiControllerName = "ServiciosProveedorApi";

        public async Task<RootObject<List<Servicio>>> GetListAsync(int page = 0, int pageSize = 10, int estatus = 0)
        {
            var modelBase = new ModelBase<List<Servicio>>(ApiControllerName);
            var parameters = new Dictionary<string,object>
            {
                {nameof(page),page },
                {nameof(pageSize), pageSize},
                {nameof(estatus), estatus }
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<Servicio>> GetByIdAsync(long idServicio)
        {
            var modelBase = new ModelBase<Servicio>("ServiciosApi");
            var parameters = new Dictionary<string,object>
            {
                {nameof(idServicio),idServicio }
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<Servicio>> GetDetalleAsync(long idServicio)
        {
            var modelBase = new ModelBase<Servicio>("ServiciosApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio }
            };
            return await modelBase.GetAsync(parameters);
        }

        public async Task<RootObject<int>> PostAsync(long idServicio, int idEstatusProveedorServicio)
        {
            var modelBase = new ModelBase<int>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                { nameof(idServicio),idServicio },
                { nameof(idEstatusProveedorServicio),idEstatusProveedorServicio }
                
            };
            
            return await modelBase.PostAsync(null, parameters);
        }

        public async Task<RootObject<int>> PostAsync(long idServicio, List<Materiales> materiales)
        {
            var modelBase = new ModelBase<int>("TerminarServicioApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio),idServicio }
            };
            var materials = materiales.Select(x => new
            {
                x.IdServicioMaterial,
                x.Aplicado,
                x.Cantidad,
                x.IdServicio,
                x.Importe
            });
            return await modelBase.PostAsync(materials, parameters);
        }


        //Cancelar un servicio.
        public async Task<RootObject<int>> PostAsync(long idServicio)
        {
            var modelBase = new ModelBase<int>("CancelarServicioApi");
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio },
                {"isProveedor", true }
            };
            return await modelBase.PostAsync(null,parameters);
        }
    }
}
