﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class SignUpModel
    {
        private const string ApiControllerName = "SignUpProveedorApi";

        public async Task<RootObject<long>> PostAsync(User user)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);
            return await modelBase.PostAsync(user);
        }
    }
}
