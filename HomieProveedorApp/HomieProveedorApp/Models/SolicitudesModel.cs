﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using Microsoft.AspNet.SignalR.Client.Http;

namespace HomieProveedorApp.Models
{
    class SolicitudesModel
    {
        private const string ApiControllerName = "SolicitudesApi";

        public async Task<RootObject<Solicitud>> GetAsync()
        {
            var modelBase = new ModelBase<Solicitud>(ApiControllerName);
            return await modelBase.GetAsync(new Dictionary<string, object>());
        }

        public async Task<RootObject<int>> PostAsync(long idServicio, int estatus)
        {
            var modelBase = new ModelBase<int>(ApiControllerName);
            var parameters = new Dictionary<string, object>
            {
                {nameof(idServicio), idServicio},
                {nameof(estatus), estatus }
            };
            return await modelBase.PostAsync(null, parameters);
        }
    }
}
