﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    class TarifasModel
    {
        private const string ApiControllerName = "TarifasApi";

        public async Task<RootObject<Tarifas>> GetAsync(long idServicio)
        {
            var modelBase = new ModelBase<Tarifas>(ApiControllerName);
            var param = new Dictionary<string, object>
            {
                {nameof(idServicio),idServicio }
            };
            return await modelBase.GetAsync(param);

        }
    }
}
