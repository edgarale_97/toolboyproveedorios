﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;

namespace HomieProveedorApp.Models
{
    public class UbicacionProveedorModel
    {
        private const string ApiControllerName = "UbicacionProveedorApi";

        public async Task<RootObject<long>> PostAsync(double latitud, double longitud, int velocidad)
        {
            var modelBase = new ModelBase<long>(ApiControllerName);

            return await modelBase.PostAsync(null, new Dictionary<string, object>
            {
                {nameof(latitud), latitud},
                {nameof(longitud), longitud},
                {nameof(velocidad), velocidad}
            });
        }
    }
}
