﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class AddMaterialViewModel : HomieViewModelBase
    {
        #region Fields and properties

        private Materiales _material;
        private bool _close;

        public Materiales Material
        {
            get { return _material; }
            set
            {
                _material = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        #endregion

        #region Constructor

        public AddMaterialViewModel(long idServicio)
        {
            IdServicio = idServicio;
            Close = false;
            Material = new Materiales();
            Material.Editable = true;
        }

        public AddMaterialViewModel(Materiales material)
        {
            IdServicio = material.IdServicio;
            Close = false;
            Material = material;
        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand( async () =>
        {
            IsLoading = true;
            if (string.IsNullOrEmpty(Material.Descripcion))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar la descripción del material",
                    "OK");
                IsLoading = false;
                return;
            }
            if (Material.Cantidad == 0)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar la cantidad del material",
                    "OK");
                IsLoading = false;
                return;
            }
            if (Material.Importe == 0)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar el importe del material",
                    "OK");
                IsLoading = false;
                return;
            }
            if (Material.Editable)
            {
                var model = new MaterialesModel();
                Material.Aplicado = Material.MaterialExtra;
                Material.FechaCreacion = DateTime.Now;
                Material.FechaAplicacion = null;
                Material.IdServicio = IdServicio;
                Material.Activo = true;
                var result = await model.PostAsync(Material);

                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    MessagingCenter.Send<AddMaterialViewModel, bool>(this, "MaterialAdded", true);
                    Close = true;

                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "No se puede editar un material de una cotización aceptada", "OK");
            }
            
            IsLoading = false;
        });

        #endregion
    }
}
