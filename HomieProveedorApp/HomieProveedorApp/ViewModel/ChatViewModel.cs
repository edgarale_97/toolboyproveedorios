﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using Microsoft.AspNet.SignalR.Client;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    public class ChatViewModel : HomieViewModelBase
    {
        #region Properties

        private ObservableCollection<ServicioChat> _mensajes;
        private string _mensaje;
        private HubConnection _hubConnection;
        private long _idServicio;
        private Cliente _cliente;
        private string _chatPageId;
        private bool _cargandoArchivo;

        public ObservableCollection<ServicioChat> Mensajes
        {
            get { return _mensajes; }
            set
            {
                _mensajes = value;
                RaisePropertyChanged();
            }
        }

        public string Mensaje
        {
            get { return _mensaje; }
            set
            {
                _mensaje = value;
                RaisePropertyChanged();
            }
        }

        public Cliente Cliente
        {
            get { return _cliente; }
            set
            {
                _cliente = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio
        {
            get { return _idServicio; }
            set
            {
                _idServicio = value; 
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public ChatViewModel(long idServicio)
        {
            _chatPageId = Guid.NewGuid().ToString();
            Settings.ChatPageId = _chatPageId;

            IdServicio = idServicio;
            Mensajes = new ObservableCollection<ServicioChat>();
            BuscarServicio();
            Hub();
        }

        #endregion

        #region Methods

        private async Task EnviarMensaje(string mensaje, byte[] imagen, byte[] video)
        {
            IsLoading = true;

            if (imagen != null || video != null)
            {
                _cargandoArchivo = true;
            }

            var model = new ChatModel();
            var result = await model.PostAsync(IdServicio, mensaje, Cliente.IdCliente, imagen, video);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    if (imagen != null)
                    {
                        result.Result.Mensaje = "Enviaste una imágen";
                    }
                    else if (video != null)
                    {
                        result.Result.Mensaje = "Enviaste un video";
                    }
                    else
                    {
                        result.Result.Mensaje = mensaje;
                    }

                    Mensajes.Add(result.Result);

                    Mensaje = string.Empty;
                    RaisePropertyChanged("Envia");
                }

                IsLoading = false;
                _cargandoArchivo = false;
            });
        }

        private async Task BuscarServicio()
        {            
            if (!IsLoading)
            {
                IsLoading = true;
                var modelServicio = new ServiciosModel();
                var resultSevicio = await modelServicio.GetByIdAsync(IdServicio);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (resultSevicio.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", resultSevicio.ErrorDescription, "OK");
                    }
                    else
                    {
                        if (!resultSevicio.Result.ChatHabilitado)
                        {
                            //await Application.Current.MainPage.DisplayAlert("Información", "El chat no se encuentra habilitado para el servicio.", "OK");
                            MessagingCenter.Send((App)Application.Current, "DetalleServicio", IdServicio);
                            return;
                        }

                        Cliente = new Cliente
                        {
                            IdCliente = resultSevicio.Result.IdCliente,
                            Nombre = resultSevicio.Result.NombreCliente
                        };
                        if (Cliente != null && Cliente.IdCliente > 0)
                        {
                            IsLoading = true;
                            Mensajes = new ObservableCollection<ServicioChat>();
                            // Buscamos los mensajes en memoria y solamente consultamos a partir del último que exista.
                            var chatSettings = new SettingsChat();
                            //SettingsChat.RemoveElement(IdServicio);
                            var mensajes = chatSettings.GetByIdServicioChat(IdServicio);
                            var lastIdServicioChat = mensajes.Count > 0 ? mensajes.Max(x => x.IdServicioChat) : 0;
                            foreach (var mensaje in mensajes.OrderBy(x => x.IdServicioChat))
                            {
                                Mensajes.Add(mensaje);
                            }

                            var model = new ChatModel();
                            var result = await model.GetAsycn(IdServicio, lastIdServicioChat);
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                if (result.Error)
                                {
                                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                                }
                                else
                                {
                                    foreach (var x in result.Result)
                                    {
                                        AddMessage(x);

                                        chatSettings.AddElement(x);
                                    }
                                    RaisePropertyChanged("Envia");
                                }
                                IsLoading = false;
                            });
                        }
                    }
                    IsLoading = false;
                });
            }
        }

        private void AddMessage(ServicioChat x)
        {
            if (!string.IsNullOrWhiteSpace(x.Imagen))
            {
                if (x.TipoEnvio == 2)
                {
                    x.Mensaje = "Enviaste una imágen";
                }
                else
                {
                    x.Mensaje = "Te enviaron una imágen";
                }
            }
            if (!string.IsNullOrWhiteSpace(x.Video))
            {
                if (x.TipoEnvio == 2)
                {
                    x.Mensaje = "Enviaste un video";
                }
                else
                {
                    x.Mensaje = "Te enviaron un video";
                }
            }

            Mensajes.Add(x);
        }

        private async void Hub()
        {
            //Connect to the server
            _hubConnection = new HubConnection(Settings.SignalrUri);
            _hubConnection.Headers.Add("Authorization", $"Bearer {Settings.Token}");

            //Create a proxy to the SignalR Hub
            var hubProxy = _hubConnection.CreateHubProxy("ChatHub");

            //Wire up a handler for the 'NotificarMensaje' for the server
            //to be called on our client
            hubProxy.On<ServicioChat>("NotificarMensaje", (mensaje) =>
            {
                if (mensaje.IdServicio == IdServicio && mensaje.IdCliente == Cliente.IdCliente)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        AddMessage(mensaje);
                        RaisePropertyChanged("Envia");
                    });
                }
            });
            //Start the connection
            await _hubConnection.Start();
        }

        public async void StartChatHub()
        {
            if (_hubConnection == null && Settings.ChatPageId == _chatPageId)
            {
                if (!_cargandoArchivo)
                    BuscarServicio();

                Hub();
            }
        }

        public async void StopChatHub()
        {
            if (_hubConnection != null && Settings.ChatPageId == _chatPageId)
            {
                _hubConnection.Stop();
                _hubConnection = null;
            }
        }

        public async Task ActualizarMensaje()
        {
            var modelServicio = new ServiciosModel();
            var resultServicio = await modelServicio.GetByIdAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                Mensajes = new ObservableCollection<ServicioChat>();

                // Buscamos los mensajes en memoria y solamente consultamos a partir del último que exista.
                var chatSettings = new SettingsChat();
                var mensajes = chatSettings.GetByIdServicioChat(IdServicio);
                var lastIdServicioChat = mensajes.Count > 0 ? mensajes.Max(x => x.IdServicioChat) : 0;
                foreach (var mensaje in mensajes.OrderBy(x => x.IdServicioChat))
                {
                    Mensajes.Add(mensaje);
                }
                var model = new ChatModel();
                var result = await model.GetAsycn(IdServicio, lastIdServicioChat);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        // Eliminamos los que no tengan id
                        var sinId = Mensajes.Where(x => x.IdServicioChat == 0).ToList();

                        foreach (var servicioChat in sinId)
                        {
                            Mensajes.Remove(servicioChat);
                        }


                        foreach (var x in result.Result)
                        {
                            AddMessage(x);

                            chatSettings.AddElement(x);
                        }

                        RaisePropertyChanged("Envia");
                    }

                    IsLoading = false;
                });

            });
        }


        #endregion

        #region Commands

        public RelayCommand EnviarMensajeCommand => new RelayCommand(async () =>
        {
            if (string.IsNullOrWhiteSpace(Mensaje))
            {
                return;
            }
            EnviarMensaje(Mensaje, null, null);
        });

        public RelayCommand EnviarImagenCommand => new RelayCommand(async () =>
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Medium
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);         

                    }
                    count++;
                }
            }
            if (array != null)
            {
                EnviarMensaje(string.Empty, array, null);
            }

        });

        public RelayCommand EnviarVideoCommand => new RelayCommand(async () =>
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }
            var file = await CrossMedia.Current.TakeVideoAsync(new StoreVideoOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                Quality = VideoQuality.Low,
                DesiredLength = new TimeSpan(0, 0, 0, 5)
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            if (array != null)
            {
                EnviarMensaje(string.Empty, null, array);
            }
        });

        #endregion
    }
}
