﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class CotizacionViewModel : HomieViewModelBase
    {
        #region Fields and Properties


        private Tarifas _tarifa;
        private ObservableCollection<Materiales> _materiales;
        private string _totalMaterialesString;
        private string _totalCotizacionString;
        private bool _close;

        public Tarifas Tarifa
        {
            get { return _tarifa; }
            set
            {
                _tarifa = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Materiales> Materiales
        {
            get { return _materiales; }
            set
            {
                _materiales = value;
                RaisePropertyChanged();
            }
        }

        public string TotalMaterialesString
        {
            get { return _totalMaterialesString; }
            set
            {
                _totalMaterialesString = value;
                RaisePropertyChanged();
            }
        }

        public string TotalCotizacionString
        {
            get { return _totalCotizacionString; }
            set
            {
                _totalCotizacionString = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        public long IdCliente { get; set; }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public CotizacionViewModel(long idServicio)
        {
            Tarifa = new Tarifas();
            Materiales = new ObservableCollection<Materiales>();
            IdServicio = idServicio;
            BuscarTarifa();
            BuscarMateriales();
        }

        #endregion

        #region Methods

        public async Task BuscarTarifa()
        {
            IsLoading = true;
            var model = new TarifasModel();
            var result = await model.GetAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Tarifa = result.Result;
                }
                IsLoading = false;
            });

            //IsLoading = true;
            //var tarifa = new Tarifas
            //{
            //    IdArea = 1,
            //    IdCategoria = 1,
            //    IdTarifa = 1,
            //    Activa = true,
            //    Monto = 500
            //};
            //Tarifa = tarifa;
            //TotalMaterialesString = 0M.ToString("C");
            //TotalCotizacionString = Tarifa.Monto.ToString("C");
            //IsLoading = false;
        }


        public async Task BuscarMateriales()
        {
            IsLoading = true;
            Materiales = new ObservableCollection<Materiales>();
            var model = new MaterialesModel();
            var result = await model.GetAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    decimal totalMateriales = 0;
                    foreach (var material in result.Result)
                    {
                        Materiales.Add(material);
                        totalMateriales += material.Importe;
                    }
                    var total = totalMateriales + Tarifa.Monto;
                    TotalMaterialesString = totalMateriales.ToString("C");
                    TotalCotizacionString = total.ToString("C");
                }
                IsLoading = false;
            });
        }


        public async Task EnviarCotizacion()
        {
            var model = new ChatModel();
            var mensaje = "El proveedor te envió una cotización";
            foreach (var material in Materiales)
            {
                if (material.MaterialExtra)
                {
                    mensaje = "El proveedor agregó materiales extra a la cotización";
                    break;
                }
            }

            if (Materiales == null || Materiales.Count == 0)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar al menos 1 material a la lista", "OK");
                return;
            }

            IsLoading = true;
            var result = await model.PostAsync(IdServicio, mensaje, IdCliente, null, null);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(null, "Se envió la cotización al cliente", "OK");
                    MessagingCenter.Send<CotizacionViewModel, bool>(this, "CotizacionEnviada", true);
                    Close = true;
                }
                IsLoading = true;
            });
        }

        public async Task ElimianrMaterial(long idMaterial)
        {
            IsLoading = true;
            var model = new MaterialesModel();
            var result = await model.DeleteAsync(idMaterial);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Correcto", "Se elimino el material", "OK");
                    BuscarMateriales();
                }
                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand(() =>
        {
            EnviarCotizacion();
        });

        public RelayCommand<long> EliminarCommand => new RelayCommand<long>(async (IdServicioMateriall) =>
        {
            foreach (var material in Materiales)
            {
                if (material.IdServicioMaterial == IdServicioMateriall)
                {
                    if (material.Editable)
                    {
                        await ElimianrMaterial(IdServicioMateriall);

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Error",
                            "No se puede eliminar un material de una cotización aceptada", "OK");
                    }
                    break;
                }
            }

        });

        #endregion
    }
}