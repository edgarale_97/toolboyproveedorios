﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    public class DetalleActivoViewModel : HomieViewModelBase
    {
        private Activos _activos;

        private Direcciones _direccion;
        private Categorias _categorias;
        private Categorias _categoriaSeleccionada;

        private bool _isComplete;

        public Activos Activos
        {
            get { return _activos; }
            set
            {
                _activos = value;
                RaisePropertyChanged();
            }
        }

        public Direcciones Direccion
        {
            get { return _direccion; }
            set
            {
                _direccion = value;                
                RaisePropertyChanged();
            }
        }

        public Categorias Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                RaisePropertyChanged();
            }
        }

        public Categorias CategoriaSeleccionada
        {
            get { return _categoriaSeleccionada; }
            set
            {
                _categoriaSeleccionada = value;                
                RaisePropertyChanged();
            }
        }

        public bool IsComplete
        {
            get { return _isComplete; }
            set
            {
                _isComplete = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        #region Constructor

        public DetalleActivoViewModel(long idServicio)
        {
            IsLoading = true;
            IsComplete = false;
            Categorias = new Categorias();
            Activos = new Activos();
            IdServicio = idServicio;
            Buscar(idServicio);
            
        }

        #endregion

        #region Methods

        public async Task Buscar(long idServicio)
        {
            var model = new ActivosModel();
            var result = await model.GetByIdAsync(idServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Activos = result.Result;
                    //BuscarDireccion();
                }
            });
        }

        //private async void BuscarDireccion()
        //{
        //    Direccion = new Direcciones();
        //    var model = new DireccionesModel();
        //    var result = await model.GetListAsync();
        //    Device.BeginInvokeOnMainThread(async () =>
        //    {
        //        if (result.Error)
        //        {
        //            await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
        //        }
        //        else
        //        {
        //            Direccion = result.Result.FirstOrDefault(x => x.Principal);
        //        }

        //        if(Activos != null)
        //        {
        //            var modelCat = new CategoriasModel();
        //            var resultCat = await modelCat.GetCategoriaAsync(Activos.IdCategoria);
        //            Device.BeginInvokeOnMainThread(async () =>
        //            {
        //                if (resultCat.Error)
        //                {
        //                    await Application.Current.MainPage.DisplayAlert("Error", resultCat.ErrorDescription, "OK");
        //                }
        //                else
        //                {
        //                    if(resultCat != null)
        //                    {
        //                        Categorias = resultCat.Result;
        //                    }
        //                    if (Categorias != null && Activos != null)
        //                    {
        //                        if (Categorias.IdCategorias == Activos.IdCategoria)
        //                        {
        //                            CategoriaSeleccionada = Categorias;
        //                        }
        //                    }
        //                    IsLoading = false;
        //                    IsComplete = true;
        //                }
        //            });
        //        }
                
        //    });
        //}

        #endregion

        #region Commands

        public RelayCommand ShowImageCommnad => new RelayCommand(async () =>
        {
            if (!string.IsNullOrWhiteSpace(Activos.FotoFactura))
            {
                Device.OnPlatform(
                    iOS: () =>
                    {
                        Device.OpenUri(new Uri(Activos.FotoFactura));
                    },
                    Android: () =>
                    {
                        Device.OpenUri(new Uri(Activos.FotoFactura));
                    });
            }
        });

        #endregion

    }
}

