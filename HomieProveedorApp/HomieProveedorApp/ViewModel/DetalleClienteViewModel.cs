﻿using System;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    public class DetalleClienteViewModel : HomieViewModelBase
    {
        private Entities.DetalleCliente _detalleCliente;

        #region Properties

        public Entities.DetalleCliente DetalleCliente
        {
            get { return _detalleCliente; }
            set
            {
                _detalleCliente = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public DetalleClienteViewModel(long idCliente)
        {
            Buscar(idCliente);
        }

        #endregion

        #region Metodos

        private async Task Buscar(long idCliente)
        {
            IsLoading = true;
            var model = new DetalleClienteModel();
            var result = await model.GetByIdAsync(idCliente);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    DetalleCliente = result.Result;
                }
            });
            IsLoading = false;
        }

        #endregion
    }
}

