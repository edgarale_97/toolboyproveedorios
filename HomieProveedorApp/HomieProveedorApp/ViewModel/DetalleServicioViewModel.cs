﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    public class DetalleServicioViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private Servicio _servicio;
        private bool _servicioIniciado;
        private bool _close;

        private bool _notaDireccion;

        private int _IdTipo;

        public Servicio Servicio
        {
            get { return _servicio; }
            set
            {
                _servicio = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        public bool ServicioIniciado
        {
            get { return _servicioIniciado; }
            set
            {
                _servicioIniciado = value;
                RaisePropertyChanged();
            }
        }

        public int IdTipo
        {
            get { return _IdTipo; }
            set
            {
                _IdTipo = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        public bool NotaDireccion
        {
            get { return _notaDireccion; }
            set
            {
                _notaDireccion = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor
        public DetalleServicioViewModel() { }

        public DetalleServicioViewModel(long idServicio, int IsGarantia)
        {
            IdServicio = idServicio;
            Buscar();
            IdTipo = IsGarantia;
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            IsLoading = true;

            var model = new ServiciosModel();
            var result = await model.GetDetalleAsync(IdServicio);

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    if(result.Result.DetalleDireccion != null)
                    {
                        NotaDireccion = true;
                    }
                    else
                    {
                        NotaDireccion = false;
                    }
                    var materiales = new List<Materiales>();
                    foreach (var material in result.Result.Materiales)
                    {
                        if ((material.AceptadoCliente ?? false))
                        {
                            materiales.Add(material);
                        }
                    }
                    Servicio = result.Result;
                    Servicio.Materiales = materiales;
                    if (Servicio.Estatus == EstatusServicioCliente.Iniciado)
                    {
                        ServicioIniciado = true;
                    }
                }
                IsLoading = false;
            });
        }

        public async Task EnCamino()
        {
            IsLoading = true;
            var model = new EnCaminoModel();
            var result = await model.PostAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Settings.ObtenerUbicacion = true;
                    MessagingCenter.Send<DetalleServicioViewModel,bool>(this,"ObtenerUbicacionProveedor",true);
                    await Application.Current.MainPage.DisplayAlert(null, "Se encuentra en camino", "OK");
                }
                IsLoading = false;
            });
        }

        public async Task IniciarServicio()
        {
            IsLoading = true;
            var model = new ServiciosModel();
            var result = await model.PostAsync(IdServicio, ServicioEstatus.Iniciado);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    ServicioIniciado = true;
                    await Application.Current.MainPage.DisplayAlert("Correcto", "El servicio se inició exitosamente",
                        "OK");
                }
                IsLoading = false;
            });
        }

        public async Task CancelarServicio()
        {
            IsLoading = true;
            var model = new ServiciosModel();
            var result = await model.PostAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Correcto", "Se cancelo el servicio", "OK");
                    Close = true;
                }
                IsLoading = false;
            });
        }

        public async Task Emergencia()
        {
            IsLoading = true;
            var model = new AlertasModel();

            var result = await model.PostAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Correcto", "Se cancelo el servicio", "OK");
                    Close = true;
                }
                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand EnCaminoCommand => new RelayCommand(async () =>
        {
            var answer =
                await Application.Current.MainPage.DisplayAlert(null, "¿Se encuentra en camino hacia el servicio?",
                    "Si", "No");
            if (answer)
            {
                await EnCamino();
            }
            
        });

        public RelayCommand IniciarServicioCommand => new RelayCommand(async () =>
        {
            
            var answer =
                await Application.Current.MainPage.DisplayAlert(null, "¿Esta seguro de iniciar el servicio?", "Si",
                    "No");
            if (answer)
            {
                await IniciarServicio();
            }
        });

        public RelayCommand CancelarCommand => new RelayCommand(async () =>
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Confirmacion",
                "¿Esta seguro que desea cancelar el servicio?", "Si", "No");
            if (answer)
            {
                CancelarServicio();
            }
        });

        public RelayCommand EmergenciaCommand => new RelayCommand(async () =>
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Llamada de Emergencia",
                "¿Esta seguro que desea solicitar una llamada de Emergencia por parte de Toolboy?", "Si", "No");
            if (answer)
            {
                Emergencia();
            }
        });

        #endregion
    }
}
