﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class DocumentacionInicialViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private ServicioCreate _servicio;
        private ImageSource _image;
        private bool _close;

		private bool _refaccionesToggle;

        public ServicioCreate Servicio
        {
            get { return _servicio; }
            set
            {
                _servicio = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource Image
        {
            get { return _image; }
            set
            {
                _image = value; 
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        public bool RefaccionesToggle
		{
			get { return _refaccionesToggle; }
			set
			{
				_refaccionesToggle = value;
				RaisePropertyChanged();
			}
		}

        public long IdServicio { get; set; }

        #endregion

        #region Constructor 

        public DocumentacionInicialViewModel(long idServicio)
        {
            Servicio = new ServicioCreate();
            IdServicio = idServicio;
        }

        #endregion

        #region Methods

        public async Task TakePicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Medium
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Servicio.FotoDocumentacionInicialBytes = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
        }

        public async Task PickPicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "No se otorgaron permisos para accesar a las fotos", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 30
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Servicio.FotoDocumentacionInicialBytes = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;

        }

        public async Task SendMessageDemora()
        {
            IsLoading = true;
            var model = new DemoraModel();
            var result = await model.GetAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Console.WriteLine($"Se mando correctamente y supuestamente la notificacion {result.ErrorDescription}");
                }
            });
        }

        public async Task Guardar()
        {
            IsLoading = true;
            var model = new IniciarTerminarServicioModel();
            var result = await model.PostAsync(IdServicio, ServicioEstatus.Iniciado, Servicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    //Detenemos el envio de la ubicacion
                    if (Settings.ObtenerUbicacion)
                    {
                        Settings.ObtenerUbicacion = false;
                        MessagingCenter.Send<DetalleServicioViewModel, bool>(new DetalleServicioViewModel(), "ObtenerUbicacionProveedor", false);
                    }
                    await Application.Current.MainPage.DisplayAlert("Correcto", "Se inició exitosamente el servicio", "OK");
                    Close = true;
                    /*Device.StartTimer(TimeSpan.FromSeconds(60), () =>
                    {
                        //Application.Current.MainPage.DisplayAlert("Alert", "This fired after 2 seconds", "OK");
                        SendMessageDemora();
                        return false;
                    });*/
                }
                IsLoading = false;
            });
        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand(async () =>
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Iniciar Servicio",
                "¿Esta seguro que desea iniciar el servicio?", "Si", "No");
            if (answer)
            {
                Guardar();
            }
        });

        #endregion
    }
}
