﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class GarantiasViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private int _paginaActivos;
        private int _paginaFinalizados;
        private ObservableCollection<Entities.Activos> _activos;
        private ObservableCollection<Entities.Servicio> _finalizados;
        private bool _pendientesCalificar;
        private CalificacionData _calificacionData;

        public int TotalPagesServicios { set; get; }

        public int PaginaActivos
        {
            get { return _paginaActivos; }
            set
            {
                _paginaActivos = value;
                RaisePropertyChanged();
            }
        }

        public int TotalPagesFinalizados { get; set; }

        public int PaginaFinalizados
        {
            get { return _paginaFinalizados; }
            set
            {
                _paginaFinalizados = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Entities.Activos> Activos
        {
            get { return _activos; }
            set
            {
                _activos = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Entities.Servicio> Finalizados
        {
            get { return _finalizados; }
            set
            {
                _finalizados = value;
                RaisePropertyChanged();
            }
        }

        public bool PendientesCalificar
        {
            get { return _pendientesCalificar; }
            set
            {
                _pendientesCalificar = value;
                RaisePropertyChanged();
            }
        }

        public CalificacionData CalificacionData
        {
            get { return _calificacionData; }
            set
            {
                _calificacionData = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public GarantiasViewModel()
        {
            PaginaActivos = 0;
            TotalPagesServicios = 0;
            Activos = new ObservableCollection<Entities.Activos>();
            //Buscar();

            PaginaFinalizados = 0;
            TotalPagesFinalizados = 0;
            Finalizados = new ObservableCollection<Entities.Servicio>();

            this.CalificacionData = new CalificacionData();
        }

        #endregion

        #region Methods

        public async Task BuscarServicios()
        {
            if (IsLoading == false)
            {
                Activos = new ObservableCollection<Entities.Activos>();
                IsLoading = true;
                var model = new ActivosModel();
                var result = await model.GetListAsync(0, 10);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        foreach (var activos in result.Result)
                        {
                            //Console.WriteLine($"En Servicio: {activos.IsService.ToString()}");
                            Activos.Add(activos);
                        }
                        TotalPagesServicios = result.TotalPages;
                    }
                    IsLoading = false;
                });
                
            }
        }

        public async Task BuscarFinalizados()
        {
            Finalizados = new ObservableCollection<Entities.Servicio>();
            IsLoading = true;
            var model = new ServiciosModel();
            var result = await model.GetListAsync(0, 100, ServicioEstatus.Terminado);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    foreach (var servicio in result.Result)
                    {
                        Finalizados.Add(servicio);
                    }

                }
                IsLoading = false;
            });
        }

        /*public async Task Buscar()
        {
                if (IsLoading == false)
                {
                    Servicios = new ObservableCollection<Entities.Servicio>();

                    IsLoading = true;
                    var model = new ServiciosModel();
                    var result = await model.GetListAsync();
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (result.Error)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");

                        }
                        else
                        {
                            foreach (var item in result.Result)
                            {
                                Servicios.Add(item);
                            }
                            //TotalPages = result.TotalPages;
                            
                            await BuscarPendientesDeCalificar();
                        }
                        IsLoading = false;
                    });
                }
            
        }*/

        public async Task BuscarPendientesDeCalificar()
        {
            var model = new CalificacionesModel();
            var result = await model.GetAsync();
            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");

            }
            else
            {

                if (result.Result != null)
                {
                    this.CalificacionData = result.Result;
                    Helpers.SettingsChat.RemoveElement(CalificacionData.IdServicio);
                    PendientesCalificar = true;
                }
            }

        }

        //public async Task Eliminar(long idServicio)
        //{
        //    IsLoading = true;
        //    var model = new ServiciosModel();
        //    var result = await model.DeleteAsync(idServicio);
        //    Device.BeginInvokeOnMainThread(async () =>
        //    {
        //        if (result.Error)
        //        {
        //            await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
        //        }
        //        else
        //        {
        //            await Application.Current.MainPage.DisplayAlert(null, "El servicio se elimino correctamente.", "OK");
        //            BuscarServicios();
        //        }
        //        IsLoading = false;
        //    });
        //}

        #endregion

        #region Commands

        public RelayCommand BuscarServiciosCommand => new RelayCommand(async () =>
        {
            Activos = new ObservableCollection<Entities.Activos>();
            PaginaActivos = 0;
            TotalPagesServicios = 0;

            await BuscarServicios();
        });

        public RelayCommand BuscarFinalizadosCommand => new RelayCommand(async () =>
        {
            Finalizados = new ObservableCollection<Entities.Servicio>();
            PaginaFinalizados = 0;
            TotalPagesFinalizados = 0;

            await BuscarFinalizados();
        });

        //public RelayCommand<long> EliminarCommand => new RelayCommand<long>(async (id) =>
        //{
        //    await Eliminar(id);
        //});

        #endregion

    }
}

