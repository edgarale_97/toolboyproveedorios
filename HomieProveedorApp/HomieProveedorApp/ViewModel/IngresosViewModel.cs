﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace HomieProveedorApp.ViewModel
{
    class IngresosViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private DateTime _fechaInicio;
        private DateTime _fechaFin;
        private ObservableCollection<Ingreso> _ingresos;
        private Chart _chart;
        private string _textTotal;

        public DateTime FechaInicio
        {
            get { return _fechaInicio; }
            set
            {
                _fechaInicio = value; 
                RaisePropertyChanged();
            }
        }

        public DateTime FechaFin
        {
            get { return _fechaFin; }
            set
            {
                _fechaFin = value; 
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Ingreso> Ingresos
        {
            get { return _ingresos; }
            set
            {
                _ingresos = value;
                RaisePropertyChanged();
            }
        }

        public Chart Chart
        {
            get { return _chart; }
            set
            {
                _chart = value;
                RaisePropertyChanged();
            }
        }

        public string TextTotal
        {
            get { return _textTotal; }
            set
            {
                _textTotal = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Costructor

        public IngresosViewModel()
        {
            FechaFin = DateTime.Today;
            FechaInicio = DateTime.Today.AddMonths(-1);
            Chart = new LineChart
            {
                Entries = new List<Entry>(),
                LineMode = LineMode.Straight,
                LineSize = 8,
                PointMode = PointMode.Circle,
                PointSize = 18
            };
            Ingresos = new ObservableCollection<Ingreso>();
            TextTotal = $"Total = {0m.ToString("C")}";
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            IsLoading = true;

            var model = new IngresosModel();
            var result = await model.GetAsync(FechaInicio, FechaFin);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    Ingresos = new ObservableCollection<Ingreso>();
                    Chart = new LineChart
                    {
                        LineMode = LineMode.Straight,
                        LineSize = 8,
                        PointMode = PointMode.Circle,
                        PointSize = 18
                    };
                    decimal granTotalTarifa = 0;
                    decimal granTotalMateriales = 0;
                    var entries = new List<Entry>();
                    foreach (var ingreso in result.Result)
                    {
                        Ingresos.Add(ingreso);
                        granTotalTarifa += ingreso.Tarifa;
                        granTotalMateriales += ingreso.Materiales;

                        //entries.Add(new Entry((float)ingreso.Total)
                        //{
                        //    Label = $"S{ingreso.IdServicio}",
                        //    ValueLabel = ingreso.TotalText,
                        //    Color = SKColor.Parse("#ffffff")
                        //    //Color = SKColor.Parse("#00a99d")
                        //});
                    }
                    var ingresosByFecha = result.Result.GroupBy(x => x.Fecha);
                    foreach (var ingresos in ingresosByFecha)
                    {
                        var total = ingresos.Sum(x => x.Total);
                        entries.Add(new Entry((float)total)
                        {
                            Label = ingresos.First().FechaText,
                            ValueLabel = total.ToString("C"),
                            //Color = SKColor.Parse("#ffffff")
                            Color = SKColor.Parse("#00a99d")
                        });
                    }
                    Chart.Entries = entries;
                    //Chart.BackgroundColor = SKColor.Parse("#8edbc5");
                    TextTotal = $"Total {(granTotalTarifa + granTotalMateriales).ToString("C")}";
                }
                IsLoading = false;
            });

        }

        #endregion
    }
}
