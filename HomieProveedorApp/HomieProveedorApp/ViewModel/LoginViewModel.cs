﻿using System;
using System.Collections.Generic;
using System.Text;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class LoginViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private bool _close;
        private string _login;
        private string _password;

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        public string MainText { get; set; }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged();
            }
        }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public LoginViewModel()
        {
            if (!IsInDesignMode)
            {
                if (Settings.Confirmado)
                {
                    if (!string.IsNullOrWhiteSpace(Settings.Token))
                    {
                        Login = Settings.User;
                        Password = Settings.Password;

                        IngresarCommand.Execute(null);
                    }
                    else if (!string.IsNullOrWhiteSpace(Settings.User))
                    {
                        Login = Settings.User;
                        Password = Settings.Password;

                        IngresarCommand.Execute(null);
                    }
                }
                else
                {
                    Login = Settings.User;
                }
            }
        }

        #endregion

        #region Methods



        #endregion

        #region Commands

        public RelayCommand IngresarCommand => new RelayCommand(async () =>
        {
            IsLoading = true;
            if (string.IsNullOrWhiteSpace(Login))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar el usuario", "OK");
                IsLoading = false;
                return;
            }
            if (string.IsNullOrWhiteSpace(Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar la contraseña", "OK");
                IsLoading = false;
                return;
            }

            var rest = new LoginModel();
            var result = await rest.AutenticarUsuarioAsync(Login, Password);

            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                IsLoading = false;
                return;
            }

            IsLoading = false;
            Settings.Confirmado = true;
            Settings.PedidosUrgentes = false;

            Close = true;
        });

        public RelayCommand RegistrarseCommand => new RelayCommand(() =>
        {
            Settings.User = "";
            Settings.Password = "";

            Close = true;
        });

        /*public RelayCommand RestablecerCommand => new RelayCommand(async () =>
        {
            // Validar que el correo sea correcto.

            var response = await Application.Current.MainPage.DisplayAlert("Homie", "Se enviará tu contraseña por correo electronico, estás de acuerdo?", "Si", "No");

            if (response)
            {
                // Se Envía un correo electrónico con la contraseña.
                IsLoading = true;
                var rest = new ChangePasswordModel();
                var result = await rest.PostAsync(Password);

                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", "Hubo un error al enviar la contraseña, intentalo mas tarde.", "OK");
                    return;
                }
                else
                {
                    // Cuando se realizó con exito:
                    await Application.Current.MainPage.DisplayAlert("Correcto", "Se envió tu contraseña por correo electronico.", "OK");
                }

                IsLoading = false;
            }
        });*/

        #endregion
    }
}
