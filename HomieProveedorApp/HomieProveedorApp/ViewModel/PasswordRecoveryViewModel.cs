﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    public class PasswordRecoveryViewModel : HomieViewModelBase
    {
        #region Fields and Properties

        private bool _close;
        private string _email;
        private string _phone;

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged();
            }
        }

        public string Phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public PasswordRecoveryViewModel() { }

        #endregion

        #region Commands

        public RelayCommand RecoverCommand => new RelayCommand(async () =>
        {
            IsLoading = true;
            if (string.IsNullOrWhiteSpace(Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar su correo", "OK");
                IsLoading = false;
                return;
            }
            if (string.IsNullOrWhiteSpace(Phone))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar su teléfono", "OK");
                IsLoading = false;
                return;
            }
            if (Phone.Length < 10 && Phone.Length > 10)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Verificar que ingresó correctamente su teléfono", "OK");
                IsLoading = false;
                return;
            }

            var rest = new PasswordRecoveryModel();
            var result = await rest.PostAsync(Email, Phone);
            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Favor de verificar que sea un mail y número valido, previamente dado de alta", "OK");
                IsLoading = false;
                return;
            }
            else
            {
                // Aqui se envia el SMS con la contraseña obtenida.
                var response = await PostAsync(Phone, result.ErrorDescription);
                if (response.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", "Favor de verificar que sea un mail y número valido, previamente dado de alta", "OK");
                    IsLoading = false;
                    return;
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Correcto", "Se acaba de envíar su contraseña por correo", "OK");
                    IsLoading = false;
                    return;
                }
            }

            IsLoading = false;

        });

        #endregion

        public async Task<RootObject<bool>> PostAsync(string phone, string password)
        {
            var senderBase = new SenderModel<bool>("sender/sms");
            var parameters = new Dictionary<string, object>();
            parameters.Add("phones", "+52" + phone);
            parameters.Add("message", Settings.MessageSMS + password);

            return await senderBase.PostAsync(parameters, null);
        }
    }
}

