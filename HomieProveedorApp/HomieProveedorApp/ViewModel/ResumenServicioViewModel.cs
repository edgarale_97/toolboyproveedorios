﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class ResumenServicioViewModel : HomieViewModelBase
    {
        #region Fields And Properties

        private Servicio _servicio;
        private ServicioCreate _servicioCreate;
        private bool _close;
        private ImageSource _image;

        public int _entregaMateriales;

        public Servicio Servicio
        {
            get { return _servicio; }
            set
            {
                _servicio = value;
                RaisePropertyChanged();
            }
        }

        public ServicioCreate ServicioCreate
        {
            get { return _servicioCreate; }
            set
            {
                _servicioCreate = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged();
            }
        }

        public int EntregaMateriales
        {
            get { return _entregaMateriales; }
            set
            {
                _entregaMateriales = value;
                RaisePropertyChanged();
            }
        }

        public long IdServicio { get; set; }

        #endregion

        #region Constructor

        public ResumenServicioViewModel(long idServicio)
        {
            EntregaMateriales = 0;
            IdServicio = idServicio;
            ServicioCreate = new ServicioCreate();
            Buscar();
        }

        #endregion

        #region Methods

        private async Task Buscar()
        {
            IsLoading = true;
            var model = new ServiciosModel();
            var result = await model.GetDetalleAsync(IdServicio);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    var materiales = new List<Materiales>();
                    foreach (var material in result.Result.Materiales)
                    {
                        if ((material.AceptadoCliente ?? false) && material.IdServicioMaterial != 0)
                        {
                            materiales.Add(material);
                        }
                    }
                    Servicio = result.Result;
                    Servicio.Materiales = materiales;
                }
                IsLoading = false;
            });
        }

        private async Task TerminarServicio()
        {
            //Console.WriteLine($"Estatus del Material: {EntregaMateriales.ToString()}");
            IsLoading = true;
            var model = new IniciarTerminarServicioModel();
            ServicioCreate.Materiales = Servicio.Materiales;
            var result = await model.PostAsync(IdServicio, ServicioEstatus.Terminado, ServicioCreate);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Correcto", "El servicio se finalizó correctamente", "OK");
                    Helpers.SettingsChat.RemoveElement(IdServicio);
                    MessagingCenter.Send<ResumenServicioViewModel>(this, "ServicioTerminado");
                }
                IsLoading = false;
            });
        }

        public async Task TakePicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontró una cámara disponible", "OK");
                return;
            }
            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = CameraDevice.Rear,
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 30
            });
            if (file == null)
            {
                return;
            }

            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count<10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            ServicioCreate.FotoDocumentacionFinalBytes = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
        }

        public async Task PickPicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "No se otorgaron permisos para accesar a las fotos", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 30
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            ServicioCreate.FotoDocumentacionFinalBytes = array;
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
        }

        #endregion

        #region Commands

        public RelayCommand TerminarServicioCommand => new RelayCommand( async () =>
        {
            var answer = await Application.Current.MainPage.DisplayAlert(null, "¿Esta seguro de finalizar el servicio?",
                "Si",
                "No");
            if (answer)
            {
                await TerminarServicio();
            }
        });

        #endregion
    }
}
