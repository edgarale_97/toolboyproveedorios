﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Views;
using HomieProveedorApp.Views.Shared;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class SamplesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private Sample _selectedSample;

        public SamplesViewModel()
        {
            SamplesCategories = new List<SampleCategory>(SamplesDefinition.SamplesCategories.Values);
            AllSamples = SamplesDefinition.AllSamples;
            SamplesGroupedByCategory = SamplesDefinition.SamplesGroupedByCategory;
        }

        public SamplesViewModel(INavigation navigation)
        {
            SamplesCategories = new List<SampleCategory>(SamplesDefinition.SamplesCategories.Values);
            AllSamples = SamplesDefinition.AllSamples;
            SamplesGroupedByCategory = SamplesDefinition.SamplesGroupedByCategory;

            MessagingCenter.Subscribe<App>((App)Application.Current, "Notificaciones", (variable) => {
                SamplesCategories = new List<SampleCategory>(SamplesDefinition.SamplesCategories.Values);
                SamplesDefinition._allSamples = null;
                AllSamples = SamplesDefinition.AllSamples;
                SamplesGroupedByCategory = SamplesDefinition.SamplesGroupedByCategory;
                RaisePropertyChanged("SamplesCategories");
                RaisePropertyChanged("AllSamples");
                RaisePropertyChanged("SamplesGroupedByCategory");
            });
        }

        public List<SampleCategory> SamplesCategories { get; set; }

        public List<Sample> AllSamples { get; set; }

        public List<SampleGroup> SamplesGroupedByCategory { get; set; }

        public Sample SelectedSample
        {
            get
            {
                return _selectedSample;
            }

            set
            {
                if (value != _selectedSample)
                {
                    _selectedSample = value;

                    RaisePropertyChanged("SelectedSample");
                }
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public static class SamplesDefinition
    {
        private static List<SampleCategory> _samplesCategoryList;
        private static Dictionary<string, SampleCategory> _samplesCategories;
        public static List<Sample> _allSamples;
        private static List<SampleGroup> _samplesGroupedByCategory;

        public static string[] _categoriesColors = {
            "#921243",
            "#B31250",
            "#CD195E",
            "#56329A",
            "#6A40B9",
            "#7C4ECD",
            "#525ABB",
            "#5F7DD4",
            "#7B96E5"
        };

        public static List<SampleCategory> SamplesCategoryList
        {
            get
            {
                if (_samplesCategoryList == null)
                {
                    InitializeSamples();
                }

                return _samplesCategoryList;
            }
        }

        public static Dictionary<string, SampleCategory> SamplesCategories
        {
            get
            {
                if (_samplesCategories == null)
                {
                    InitializeSamples();
                }

                return _samplesCategories;
            }
        }

        public static List<Sample> AllSamples
        {
            get
            {
                if (_allSamples == null)
                {
                    InitializeSamples();
                }
                return _allSamples;
            }
        }

        public static List<SampleGroup> SamplesGroupedByCategory
        {
            get
            {
                if (_samplesGroupedByCategory == null)
                {
                    InitializeSamples();
                }

                return _samplesGroupedByCategory;
            }
        }

        internal static Dictionary<string, SampleCategory> CreateSamples()
        {
            var categories = new Dictionary<string, SampleCategory>();

            categories.Add(
                "HOMIE",
                new SampleCategory
                {
                    Name = Settings.User,
                    BackgroundColor = Color.FromHex(_categoriesColors[0]),
                    BackgroundImage = "",
                    Icon = "",
                    Badge = 2,
                    PedidosUrgentes = Settings.PedidosUrgentes,
                    SamplesList = new List<Sample>
                    {
                        //new Sample("Notificaciones", typeof(NotificacionesPage), "dashboard_thumbnail_6.jpg", Icon.FAEnvelopeO),
                        //new Sample("Solicitudes",typeof(SolicitudesPage),"dashboard_thumbnail_6.jpg", Icon.FAAmbulance),
                        //new Sample("Servicios",typeof(ServiciosPage),"dashboard_thumbnail_6.jpg",Icon.FAClipboard),
                        new Sample("Perfil Técnico", typeof(SettingsPage), "dashboard_thumbnail_1.jpg", "idPerfil.png"),
                        new Sample("Ingresos",typeof(IngresosPage),"dashboard_thumbnail_6.jpg","Ingresos.png"),
                        new Sample("Términos y Condiciones", typeof(TerminosCondicionesPage), "dashboard_thumbnail_6.jpg", "termsConditions.png"),
                        new Sample("Aviso de Privacidad", typeof(AvisoPrivacidadPage), "dashboard_thumbnail_6.jpg", "Privacy.png"),
                        new Sample("Cerrar Sesión", typeof(CerrarSesionPage), "dashboard_thumbnail_1.jpg", "Logout.png")   
                    }
                }
            );

            return categories;
        }

        internal static void InitializeSamples()
        {
            _samplesCategories = CreateSamples();

            _samplesCategoryList = new List<SampleCategory>();

            foreach (var sample in _samplesCategories.Values)
            {
                _samplesCategoryList.Add(sample);
            }

            _allSamples = new List<Sample>();

            _samplesGroupedByCategory = new List<SampleGroup>();

            foreach (var sampleCategory in SamplesCategories.Values)
            {

                var sampleItem = new SampleGroup(sampleCategory.Name.ToUpper(), Settings.PedidosUrgentes);

                foreach (var sample in sampleCategory.SamplesList)
                {
                    _allSamples.Add(sample);
                    sampleItem.Add(sample);
                }

                _samplesGroupedByCategory.Add(sampleItem);
            }
        }

        private static void RootPageCustomNavigation(INavigation navigation)
        {
            SampleCoordinator.RaisePresentMainMenuOnAppearance();
            navigation.PopToRootAsync();
        }
    }

    public class SampleGroup : List<Sample>
    {
        private readonly string _name;
        private readonly bool _pedidosUrgentes;

        public SampleGroup(string name, bool pedidosUrgentes)
        {
            _name = name;
            _pedidosUrgentes = pedidosUrgentes;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public bool PedidosUrgentes
        {
            get { return _pedidosUrgentes; }
        }
    }

    public class SampleCategory
    {
        public string Name { get; set; }

        public Color BackgroundColor { get; set; }

        public String BackgroundImage { get; set; }

        public List<Sample> SamplesList { get; set; }

        public string Icon { get; set; }

        public int Badge { get; set; }

        public bool PedidosUrgentes { get; set; }
    }

    public class Sample
    {
        private readonly string _name;
        private readonly bool _modal;
        private readonly string _icon;
        private readonly Type _pageType;
        private readonly string _backgroundImage;
        private readonly bool _justNotifyNavigateIntent;
        private readonly Action<INavigation> _customNavigation;
        private readonly bool _isNew;
        private readonly string _badgeText;

        public Sample(
            string name,
            Type pageType,
            string backgroundImage,
            string icon = "", // TODO: Cambiar por font awesome
            bool modal = false,
            bool isNew = false,
            bool justNotifyNavigateIntent = false,
            Action<INavigation> customNavigation = null
        )
        {
            _name = name;
            _pageType = pageType;
            _icon = icon;
            _backgroundImage = backgroundImage;
            _modal = modal;
            _justNotifyNavigateIntent = justNotifyNavigateIntent;
            _customNavigation = customNavigation;
            _isNew = isNew;

            //if (Settings.Notificaciones > 0 && Name.ToLower().Contains("notificacion"))
            //{
            //    _isNew = true;
            //    _badgeText = Settings.Notificaciones.ToString();
            //}
        }

        public string BadgeText
        {
            get { return _badgeText; }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string Icon
        {
            get
            {
                return _icon;
            }
        }

        public string BackgroundImage
        {
            get
            {
                return _backgroundImage;
            }
        }

        public async Task NavigateToSample(INavigation navigation)
        {
            SampleCoordinator.RaiseSampleSelected(this);

            if (_justNotifyNavigateIntent)
            {
                return;
            }

            if (_customNavigation != null)
            {
                _customNavigation(navigation);
                return;
            }

            int popCount = 0;
            int firstPageToPopIndex = 0;

            for (int i = navigation.NavigationStack.Count - 1; i >= 0; i--)
            {
                if (navigation.NavigationStack[i].GetType() == _pageType)
                {
                    firstPageToPopIndex = i + 1;
                    popCount = navigation.NavigationStack.Count - 1 - i;
                    break;
                }
            }

            if (popCount > 0)
            {
                for (int i = 1; i < popCount; i++)
                {
                    navigation.RemovePage(navigation.NavigationStack[firstPageToPopIndex]);
                }

                await navigation.PopAsync();

                return;
            }

            var page = CreateContentPage();

            if (_modal)
            {
                await navigation.PushModalAsync(NavigationPageHelper.Create(page));
            }
            else
            {
                await navigation.PushAsync(page);
            }
        }

        private Page CreateContentPage()
        {
            var page = Activator.CreateInstance(_pageType) as Page;

            System.Diagnostics.Debug.Assert(page != null);

            return page;
        }

        public Type PageType
        {
            get
            {
                return _pageType;
            }
        }

        public bool IsNew
        {
            get
            {
                return _isNew;
            }
        }
    }

    public class SampleCoordinator
    {
        public static event EventHandler<SampleEventArgs> SelectedSampleChanged;
        public static event EventHandler<EventArgs> PresentMainMenuOnAppearance;
        public static event EventHandler<SampleEventArgs> SampleSelected;

        private static Sample _selectedSample = null;

        public static void RaisePresentMainMenuOnAppearance()
        {
            if (PresentMainMenuOnAppearance != null)
            {
                PresentMainMenuOnAppearance(typeof(SampleCoordinator), null);
            }
        }

        public static void RaiseSampleSelected(Sample sample)
        {
            if (SampleSelected != null)
            {
                SampleSelected(typeof(SampleCoordinator), new SampleEventArgs(sample));
            }
        }

        public static Sample SelectedSample
        {
            get
            {
                return _selectedSample;
            }

            set
            {
                if (_selectedSample != value)
                {
                    _selectedSample = value;

                    if (SelectedSampleChanged != null)
                    {
                        SelectedSampleChanged(typeof(SampleCoordinator), new SampleEventArgs(value));
                    }
                }
            }
        }
    }

    public class SampleEventArgs : EventArgs
    {
        private readonly Sample _sample;

        public SampleEventArgs(Sample newSample)
        {
            _sample = newSample;
        }

        public Sample Sample
        {
            get
            {
                return _sample;
            }
        }
    }
}
