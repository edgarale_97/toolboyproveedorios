﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class ServiciosViewModel : HomieViewModelBase
    {
        #region Fields and properties

        private int _paginaServicios;
        private int _paginaFinalizados;
        private ObservableCollection<Servicio> _servicios;
        private ObservableCollection<Servicio> _finalizados;
        private bool _pendientesCalificar;
        private CalificacionData _calificacionData;

        private bool _servicioGarantia;

        public ObservableCollection<Servicio> Servicios
        {
            get { return _servicios; }
            set
            {
                _servicios = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Servicio> Finalizados
        {
            get { return _finalizados; }
            set
            {
                _finalizados = value;
                RaisePropertyChanged();
            }
        }

        public bool ServicioGarantia
        {
            get { return _servicioGarantia; }
            set
            {
                _servicioGarantia = value;
                RaisePropertyChanged();
            }
        }

        public bool PendientesCalificar
        {
            get { return _pendientesCalificar; }
            set
            {
                _pendientesCalificar = value;
                RaisePropertyChanged();
            }
        }

        public CalificacionData CalificacionData
        {
            get { return _calificacionData; }
            set
            {
                _calificacionData = value;
                RaisePropertyChanged();
            }
        }

        public int TotalPagesServicios { set; get; }

        public int PaginaServicios
        {
            get { return _paginaServicios; }
            set
            {
                _paginaServicios = value; 
                RaisePropertyChanged();
            }
        }

        public int TotalPagesFinalizados { get; set; }

        public int PaginaFinalizados
        {
            get { return _paginaFinalizados; }
            set
            {
                _paginaFinalizados = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public ServiciosViewModel() {}

        public void Initialize()
        {
            PaginaServicios = 0;
            TotalPagesServicios = 0;
            Servicios = new ObservableCollection<Servicio>();

            PaginaFinalizados = 0;
            TotalPagesFinalizados = 0;
            Finalizados = new ObservableCollection<Servicio>();

            this.CalificacionData = new CalificacionData();

            

            //BuscarServicios();
        }

        #endregion

        #region Methods

        public async Task BuscarServicios()
        {
                if (IsLoading == false)
                {
                    Servicios = new ObservableCollection<Servicio>();

                    IsLoading = true;
                    var model = new ServiciosModel();
                    var result = await model.GetListAsync();

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (result.Error)
                        {
                            await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                        }
                        else
                        {
                            foreach (var servicio in result.Result)
                            {
                                Servicios.Add(servicio);
                            }
                            TotalPagesServicios = result.TotalPages;
                            BuscarPendientesDeCalificar();
                            
                        }
                        IsLoading = false;
                    });
                }
        }

        public async Task BuscarFinalizados()
        {
                Finalizados = new ObservableCollection<Servicio>();
                IsLoading = true;
                var model = new ServiciosModel();
                var result = await model.GetListAsync(0, 100, ServicioEstatus.Terminado);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (result.Error)
                    {
                        await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                    }
                    else
                    {
                        foreach (var servicio in result.Result)
                        {
                            Finalizados.Add(servicio);
                        }

                    }
                    IsLoading = false;
                });
        }

        public async Task BuscarPendientesDeCalificar()
        {
            var model = new CalificacionesModel();
            var result = await model.GetAsync();
            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");

            }
            else
            {
                if (result.Result != null)
                {
                    this.CalificacionData = result.Result;
                    PendientesCalificar = true;
                }
            }

        }

        #endregion

        #region Commands

        public RelayCommand BuscarServiciosCommand => new RelayCommand(async () =>
        {
            Servicios = new ObservableCollection<Servicio>();
            PaginaServicios = 0;
            TotalPagesServicios = 0;
            await BuscarServicios();
        });

        public RelayCommand BuscarFinalizadosCommand => new RelayCommand(async () =>
        {
            Finalizados = new ObservableCollection<Servicio>();
            PaginaFinalizados = 0;
            TotalPagesFinalizados = 0;

            await BuscarFinalizados();
        });

        #endregion
    }
}
