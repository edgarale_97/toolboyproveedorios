﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using ImageCircle.Forms.Plugin;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class SettingViewModel : HomieViewModelBase
    {
        #region Fields and properties

        private ProfileProveedor _user;
        private ImageSource _image;
        private string _fotoPerfil;

        private string _banco;

        private static SettingViewModel m_Instancia;

        public static SettingViewModel Instancia
        {
            get
            {
                if(m_Instancia == null)
                {
                    m_Instancia = new SettingViewModel();
                }
                return m_Instancia;
            }
        }

        public ProfileProveedor User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource Image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged();
            }
        }

        public string FotoPerfil
        {
            get { return _fotoPerfil; }
            set
            {
                _fotoPerfil = value;
                RaisePropertyChanged();
            }
        }

        public string Banco
        {
            get { return _banco; }
            set
            {
                _banco = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SettingViewModel()
        {
            User = new ProfileProveedor();
            Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            IsLoading = true;
            var model = new ProfileModel();
            var result = await model.GetAsync();

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (result.Error)
                {
                    await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                }
                else
                {
                    User = result.Result;
                    User.Banco = User.BancoSeleccion;
                    Banco = User.Banco;
                    //Stream stream = new MemoryStream(User.Foto);
                    //var image = ImageSource.FromStream(() => stream);
                    //Image = image;
                }

                IsLoading = false;
            });
        }

        public async Task Guardar()
        {
            IsLoading = true;
            var model = new ProfileModel();
            var result = await model.PostAsync(User);
            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                IsLoading = false;
            } 
            else
            {
                // Cuando el registro se realizó con exito.
                await Application.Current.MainPage.DisplayAlert("Listo", "Los datos se guardaron con éxito.", "OK");
                IsLoading = false;
            }
        }

        public async Task TakePicture()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontro una cámara disponible.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Small
            });

            if (file == null)
            {
                return;
            }

            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }

            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
            User.Foto = array;

        }

        public async Task PickPicture()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se obtuvieron permisos para acceder a la galería.", "OK");
            }
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 30
            });
            if (file == null)
                return;

            byte[] array;
            using(var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            Image = image;
            User.Foto = array;
        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand(async () =>
        {
            if (string.IsNullOrEmpty(User.Correo))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El correo no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.Nombres))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El nombre no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.ApellidoPaterno))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El apellido paterno no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.ApellidoMaterno))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El apellido materno no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.NumeroTelefonico))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El telefono movil no puede quedar vacio", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.Clabe))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                     "La CLABE interbancaria no puede quedar vacía", "OK");
                return;
            }
            if(User.Clabe.Length <= 18)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                     "La CLABE interbancaria debe contener al menos 18 dígitos", "OK");
                return;
            }

            await Guardar();

        });

        #endregion
    }
}

