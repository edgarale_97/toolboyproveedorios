﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    class SignUpViewModel : HomieViewModelBase
    {
        #region Fields and properties

        private User _user;
        private ImageSource _fotoPerfil;
        private ImageSource _trabajoFoto1;
        private ImageSource _trabajoFoto2;
        private ImageSource _trabajoFoto3;
        private bool _isAnyPhotoPicked;
        private bool _close;
        private bool _isPhoto1Picked = false;
        private bool _isPhoto2Picked = false;
        private bool _isPhoto3Picked = false;
        private bool _isPhotoPerfilPicked = false;

        public User User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource FotoPerfil
        {
            get { return _fotoPerfil; }
            set
            {
                _fotoPerfil = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource TrabajoFoto1
        {
            get { return _trabajoFoto1; }
            set
            {
                _trabajoFoto1 = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource TrabajoFoto2
        {
            get { return _trabajoFoto2; }
            set
            {
                _trabajoFoto2 = value; 
                RaisePropertyChanged();
            }
        }

        public ImageSource TrabajoFoto3
        {
            get { return _trabajoFoto3; }
            set
            {
                _trabajoFoto3 = value;
                RaisePropertyChanged();
            }
        }

        public bool IsPhotoPerfilPicked
        {
            get { return _isPhotoPerfilPicked; }
            set
            {
                _isPhotoPerfilPicked = value;
                RaisePropertyChanged();
            }
        }

        public bool IsPhoto1Picked
        {
            get { return _isPhoto1Picked; }
            set
            {
                _isPhoto1Picked = value;
                RaisePropertyChanged();
            }
        }

        public bool IsPhoto2Picked
        {
            get { return _isPhoto2Picked; }
            set
            {
                _isPhoto2Picked = value;
                RaisePropertyChanged();
            }
        }

        public bool IsPhoto3Picked
        {
            get { return _isPhoto3Picked; }
            set
            {
                _isPhoto3Picked = value;
                RaisePropertyChanged();
            }
        }

        public bool IsAnyPhotoPicked
        {
            get { return _isAnyPhotoPicked; }
            set
            {
                _isAnyPhotoPicked = value;
                RaisePropertyChanged();
            }
        }

        public bool Close
        {
            get { return _close; }
            set
            {
                _close = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SignUpViewModel()
        {
            User = new User();
            IsAnyPhotoPicked = false;
        }

        #endregion

        #region Methods

        public async Task TakePicture(int foto)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "No se encontró una cámara disponible", "OK");
                return;
            }
            
            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                DefaultCamera = foto==0 ? CameraDevice.Front : CameraDevice.Rear,
                CompressionQuality = 30,
                PhotoSize = PhotoSize.Small
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count<10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            switch (foto)
            {
                case 0:
                    FotoPerfil = image;
                    User.FotoPerfil = array;
                    IsPhotoPerfilPicked = true;
                    break;
                case 1:
                    TrabajoFoto1 = image;
                    User.FotoTrabajo1 = array;
                    IsAnyPhotoPicked = true;
                    IsPhoto1Picked = true;
                    break;
                case 2:
                    TrabajoFoto2 = image;
                    User.FotoTrabajo2 = array;
                    IsAnyPhotoPicked = true;
                    IsPhoto2Picked = true;
                    break;
                case 3:
                    TrabajoFoto3 = image;
                    User.FotoTrabajo3 = array;
                    IsAnyPhotoPicked = true;
                    IsPhoto3Picked = true;
                    break;
            }
        }

        public async Task PickPicture(int foto)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "No se otorgaron permisos para accesar a las fotos", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 30
            });
            if (file == null)
            {
                return;
            }
            byte[] array;
            using (var memoryStream = new MemoryStream())
            {
                int count = 0;
                file.GetStream().CopyTo(memoryStream);
                array = memoryStream.ToArray();
                while (count < 10)
                {
                    if (array.Count() == 0)
                    {
                        file.GetStream().CopyTo(memoryStream);
                        array = memoryStream.ToArray();
                        await Task.Delay(1000);
                    }
                    count++;
                }
            }
            Stream stream = new MemoryStream(array);
            var image = ImageSource.FromStream(() => stream);
            switch (foto)
            {
                case 0:
                    FotoPerfil = image;
                    User.FotoPerfil = array;
                    IsPhotoPerfilPicked = true;
                    break;
                case 1:
                    TrabajoFoto1 = image;
                    User.FotoTrabajo1 = array;
                    IsAnyPhotoPicked = true;
                    IsPhoto1Picked = true;
                    break;
                case 2:
                    TrabajoFoto2 = image;
                    User.FotoTrabajo2 = array;
                    IsAnyPhotoPicked = true;
                    IsPhoto2Picked = true;
                    break;
                case 3:
                    TrabajoFoto3 = image;
                    User.FotoTrabajo3 = array;
                    IsAnyPhotoPicked = true;
                    IsPhoto3Picked = true;
                    break;
            }
            
        }

        public async Task Guardar()
        {
            IsLoading = true;
            var model = new SignUpModel();
            var result = await model.PostAsync(User);
            if (result.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Registro Correcto",
                    "En breve, recibirás un correo con los pasos que siguen", "OK");
                Settings.User = User.Correo;
                MessagingCenter.Send<App>((App)Application.Current, "RegistroCompletado");
                //Close = true;
            }
            IsLoading = false;
        }

        #endregion

        #region Commands

        public RelayCommand GuardarCommand => new RelayCommand(async () =>
        {
            if (!IsPhotoPerfilPicked)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                   "Debe agregar una foto de perfil", "OK");
                return;
            }

            if (string.IsNullOrEmpty(User.Nombres))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El nombre no puede quedar vacío", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.ApellidoPaterno))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El apellido paterno no puede quedar vacío", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.Correo))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El correo no puede quedar vacío", "OK");
                return;
            }
            
            
            if (string.IsNullOrEmpty(User.NumeroTelefonico))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El teléfono movil no puede quedar vacío", "OK");
                return;
            }
            if (User.NumeroTelefonico.Length < 10)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "El teléfono movil no puede tener menos de 10 dígitos", "OK");
                return;
            }
            if (!User.LunesDisponible && !User.MartesDisponible && !User.MiercolesDisponible && !User.JuevesDisponible && !User.ViernesDisponible
            && !User.SabadoDisponible && !User.DomingoDisponible)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "Debe escoger al menos un día de trabajo a la semana", "OK");
                return;
            }
            if (User.HoraFinTrabajo <= User.HoraInicioTrabajo)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La hora final no puede ser menor ni igual a la hora inicial", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.Descripcion))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La descripción no puede quedar vacía", "OK");
                return;
            }
            if (!IsAnyPhotoPicked)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "Debe agregar por lo menos una imagen de su trabajo", "OK");
                return;
            }
            if (string.IsNullOrEmpty(User.Banco))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La CLABE introducida no coincide con nigún banco conocido", "OK");
                return;
            }
            
            if (string.IsNullOrEmpty(User.Clabe))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La CLABE interbancaria no puede quedar vacía", "OK");
                return;
            }
            if (User.Clabe.Length < 18)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "La CLABE interbancaria no puede tener menos de 18 dígitos", "OK");
                return;
            }

            await Guardar();

        });

        #endregion
    }
}
