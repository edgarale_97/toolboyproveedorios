﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Models;
using Xamarin.Forms;

namespace HomieProveedorApp.ViewModel
{
    public class SolicitudesViewModel : HomieViewModelBase
    {
        #region Fields and properties

        private Solicitud _solicitud;
        private bool _existeSolicitud;
        private bool _aceptado;
        private bool _rechazado;

        private bool _notaDireccion;

        public Solicitud Solicitud
        {
            get { return _solicitud; }
            set
            {
                _solicitud = value;
                RaisePropertyChanged();
            }
        }

        public bool ExisteSolicitud
        {
            get { return _existeSolicitud; }
            set
            {
                _existeSolicitud = value;
                RaisePropertyChanged();
            }
        }

        public bool Aceptado
        {
            get { return _aceptado; }
            set
            {
                _aceptado = value;
                RaisePropertyChanged();
            }
        }

        public bool Rechazado
        {
            get { return _rechazado; }
            set
            {
                _rechazado = value;
                RaisePropertyChanged();
            }
        }

        public bool NotaDireccion
        {
            get { return _notaDireccion; }
            set
            {
                _notaDireccion = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Constructor

        public SolicitudesViewModel()
        {
            Aceptado = false;
            Rechazado = false;
            ExisteSolicitud = false;
            Solicitud = new Solicitud();
            //Buscar();
        }

        #endregion

        #region Methods

        public async Task Buscar()
        {
            if (!IsLoading)
            {
                IsLoading = true;
                var model = new SolicitudesModel();
                var res = await model.GetAsync();
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (res.Error)
                    {
                        if (res.ErrorDescription == "No tiene ninguna solicitud")
                        {
                            ExisteSolicitud = false;
                        }
                        else
                        {
                            ExisteSolicitud = false;
                            await Application.Current.MainPage.DisplayAlert("Error", res.ErrorDescription, "OK");
                        }

                    }
                    else
                    {
                        ExisteSolicitud = true;
                        if (res.Result != null && res.Result.Categorias != null)
                        {
                            res.Result.Categorias = res.Result.Categorias.OrderBy(x => x.IdCategoriaPadre).ToList();
                            if (res.Result.DetalleDireccion != null)
                            {
                                NotaDireccion = true;
                            }
                            else
                            {
                                NotaDireccion = false;
                            }
                        }
                        Solicitud = res.Result;
                    }
                    IsLoading = false;
                });
            }  
        }

        public async Task AceptarRechazarSolicitud(int estatus)
        {
            IsLoading = true;
            var model = new SolicitudesModel();
            var result = await model.PostAsync(Solicitud.IdServicio, estatus);
            if (result.Error)
            {
                if (result.ErrorDescription.ToLower() == "lo sentimos, el servicio fue aceptado por otro proveedor")
                {
                    IsLoading = false;
                    Buscar();
                }
                await Application.Current.MainPage.DisplayAlert("Error", result.ErrorDescription, "OK");
                
            }
            else
            {
                //var action = estatus == ServicioEstatus.SolicitudAceptada
                //    ? "Has aceptado la solicitud."
                //    : "Has rechazado la solicitud.";
                //await Application.Current.MainPage.DisplayAlert(null, action, "OK");
                IsLoading = false;
                if (estatus == ServicioEstatus.SolicitudAceptada)
                {
                    Aceptado = true;
                    await Task.Delay(3000);
                    Aceptado = false;
                    MessagingCenter.Send<SolicitudesViewModel, long>(this,"SolicitudAceptada",Solicitud.IdServicio);
                }
                else
                {
                    Rechazado = true;
                    await Task.Delay(3000);
                    Rechazado = false;
                }
                Buscar();
            }
        }

        #endregion

        #region Commands

        public RelayCommand AceptarCommand => new RelayCommand(async () =>
        {
            var action =
                await Application.Current.MainPage.DisplayAlert("Confirmar", "¿Desea Aceptar la solicitud?", "Si",
                    "No");
            if (action)
            {
                await AceptarRechazarSolicitud(ServicioEstatus.SolicitudAceptada);
            }
            
        });

        public RelayCommand RechazarCommand => new RelayCommand(async () =>
        {
            var action =
                await Application.Current.MainPage.DisplayAlert("Confirmar", "¿Desea Rechazar la solicitud?", "Si",
                    "No");
            if(action)
                await AceptarRechazarSolicitud(ServicioEstatus.SolicitudDenegada);
        });


        #endregion

    }
}
