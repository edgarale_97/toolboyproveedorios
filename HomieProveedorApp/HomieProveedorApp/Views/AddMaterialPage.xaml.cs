﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddMaterialPage : ContentPage
	{
	    private AddMaterialViewModel _vm;
        public AddMaterialPage (long idServicio)
		{
		    NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent ();
            _vm = new AddMaterialViewModel(idServicio);
            _vm.PropertyChanged += VmOnPropertyChanged;
		    BindingContext = _vm;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    public AddMaterialPage(Materiales material)
	    {
	        NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            _vm = new AddMaterialViewModel(material);
	        _vm.PropertyChanged += VmOnPropertyChanged;
	        BindingContext = _vm;
	        Title = "Editar Material";

	        if (Device.RuntimePlatform == Device.iOS)
	        {
	            this.Padding = new Thickness(0, 30, 0, 0);
	        }
        }

	    private void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            if (_vm.Close)
	            {
	                Navigation.PopAsync();
	            }
	        }
	    }
	}
}