﻿using System;
using System.Collections.Generic;
using HomieProveedorApp.Helpers;
using Xamarin.Forms;

namespace HomieProveedorApp.Views
{
    public partial class AvisoPrivacidadPage : ContentPage
    {
        public AvisoPrivacidadPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            //if (Device.RuntimePlatform == Device.iOS)
            //{
            //    this.Padding = new Thickness(0, 30, 0, 0);
            //}

            labelAviso.Text = Settings.AvisoDePrivacidad;

        }
    }
}
