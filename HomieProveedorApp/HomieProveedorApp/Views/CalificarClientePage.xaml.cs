﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalificarClientePage : ContentPage
	{
	    private CalificarClienteViewModel viewModel;
	    private bool _animate;
		public CalificarClientePage (CalificacionData data)
		{
            NavigationPage.SetHasNavigationBar(this, false);
			InitializeComponent ();
            viewModel = new CalificarClienteViewModel(data);
            viewModel.PropertyChanged += ViewModelOnPropertyChanged;
		    BindingContext = viewModel;
		}

	    private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            if (viewModel.Close)
	            {
	                Navigation.PopAsync();
	            }
	        }
	    }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        var content = this.Content;
	        this.Content = null;
	        this.Content = content;

	        _animate = true;
	        AnimateIn().Forget();
	    }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();
	        _animate = false;
	    }

	    public async Task AnimateIn()
	    {
	        await AnimateItem(img, 10500);
	    }

	    public async Task AnimateItem(View uiElement, uint duration)
	    {
	        while (_animate)
	        {
	            await uiElement.ScaleTo(1.05, duration, Easing.SinInOut);
	            await Task.WhenAll(
	                uiElement.FadeTo(1, duration, Easing.SinInOut),
	                uiElement.LayoutTo(new Rectangle(new Point(0, 0), new Size(uiElement.Width, uiElement.Height))),
	                uiElement.FadeTo(.9, duration, Easing.SinInOut),
	                uiElement.ScaleTo(1.15, duration, Easing.SinInOut));
	        }
	    }
    }
}