﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.ViewModel;
using HomieProveedorApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChatPage : ContentPage
	{
	    private ChatViewModel _vm;
        private int idTipo;

		public ChatPage (long idServicio, int IdTipo, bool readOnly)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
		    ContainerSend.IsVisible = !readOnly;

            _vm = new ChatViewModel(idServicio);
            _vm.PropertyChanged += VmOnPropertyChanged;

		    this.BindingContext = _vm;

            MessagingCenter.Subscribe<CotizacionViewModel,bool>(this,"CotizacionEnviada",Callback);

            SendButton.GestureRecognizers.Add(new TapGestureRecognizer(view =>
            {
                _vm.EnviarMensajeCommand.Execute(null);
            }));

            //MessagingCenter.Subscribe<App>((App)Application.Current, "OnActivated", (variable) => {
            //    _vm.StartChatHub();
            //    Debug.WriteLine("OnActivated");
            //});

            //MessagingCenter.Subscribe<App>((App)Application.Current, "DidEnterBackground", (variable) => {
            //    _vm.StopChatHub();
            //    Debug.WriteLine("DidEnterBackground");
            //});

            idTipo = IdTipo;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void Callback(CotizacionViewModel cotizacionViewModel, bool b)
	    {
	        if (b)
	        {
	            
            }
	    }

	    private void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Envia")
	        {
	            if(_vm.Mensajes.Count > 0)
                    ListViewMensajes.ScrollTo(_vm.Mensajes[_vm.Mensajes.Count-1],ScrollToPosition.End,true);
	        }
	        if (e.PropertyName == "Cliente")
	        {
                bar.LabelText = _vm.Cliente.Nombre;
            }
        }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();  
            _vm.StopChatHub();
            Debug.Write("OnDisappearing");
	    }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
            _vm.StartChatHub();
            Debug.Write("Appearing");
	        NavigationPage.SetHasNavigationBar(this, false);
        }

	    private async void ListViewMensajes_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        var selectedItem = ((ListView) sender).SelectedItem;
	        var servicio = (Entities.ServicioChat) selectedItem;

	        if (!string.IsNullOrWhiteSpace(servicio.Video))
	        {
                //var video = new VideoPage(servicio.Video);
                //if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(VideoPage)) &&
                //    !Navigation.ModalStack.Any(x => x.GetType() == typeof(VideoPage)))
                //{
                //    await Navigation.PushAsync(video);
	            //}
	            Device.OnPlatform(
	                iOS: () =>
	                {
	                    Device.OpenUri(new Uri(servicio.Video));
	                },
	                Android:
	                () =>
	                {
	                    Device.OpenUri(new Uri(servicio.Video));
	                });
            }
	        if (!string.IsNullOrWhiteSpace(servicio.Imagen))
	        {
                //var imagen = new ImagenPage(servicio.Imagen);
                //if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ImagenPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ImagenPage)))
                //    await Navigation.PushAsync(imagen);
	            Device.OnPlatform(
	                iOS: () =>
	                {
	                    Device.OpenUri(new Uri(servicio.Imagen));
	                },
	                Android:
	                () =>
	                {
	                    Device.OpenUri(new Uri(servicio.Imagen));
	                });
            }
        }

	    private async void Bar_OnClicked(object sender, EventArgs e)
	    {
	        string[] opciones = {"Archivos"};
	        var accion = await DisplayActionSheet("Mas opciones", "Cancel", null, opciones);
	        switch (accion)
	        {
                case "Archivos":
                    var page = new ArchivosServicioPage(_vm.IdServicio);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ArchivosServicioPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(ArchivosServicioPage)))
                        await Navigation.PushAsync(page);
                    break;

            }
	    }

	    private async void CotizarButton_OnClicked(object sender, EventArgs e)
	    {
	        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CotizacionViewModel)) &&
	            !Navigation.ModalStack.Any(x => x.GetType() == typeof(CotizacionPage)))
	            await Navigation.PushAsync(new CotizacionPage(_vm.IdServicio));
        }

	    public async Task Update()
	    {
	        await _vm.ActualizarMensaje();
	    }
    }
}