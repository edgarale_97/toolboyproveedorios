﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views.Common
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeLabelNavigation : ContentView
	{
		public HomeLabelNavigation ()
		{
			InitializeComponent ();
		    flecha.GestureRecognizers.Add(new TapGestureRecognizer(view =>
		    {
                switch (LabelText)
                {
					case "Reset Password":
						Navigation.PopModalAsync();
						break;
					case "Términos y condiciones":
						Navigation.PopModalAsync();
						break;
					default:
						MessagingCenter.Send(this, "Home");
						break;

				}
			}));
        }

	    public static BindableProperty LabelTextProperty =
	        BindableProperty.Create(
	            nameof(LabelText),
	            typeof(string),
	            typeof(HomeLabelNavigation),
	            defaultValue: "",
	            defaultBindingMode: BindingMode.OneWay
	        );

	    public string LabelText
	    {
	        get { return (string)GetValue(LabelTextProperty); }
	        set { SetValue(LabelTextProperty, value); }
	    }

	    public static BindableProperty IsVisibleTextProperty =
	        BindableProperty.Create(
	            nameof(IsVisibleText),
	            typeof(bool),
	            typeof(HomeLabelNavigation),
	            defaultValue: true,
	            defaultBindingMode: BindingMode.OneWay
	        );

	    public bool IsVisibleText
	    {
	        get { return (bool)GetValue(IsVisibleTextProperty); }
	        set { SetValue(IsVisibleTextProperty, value); }
	    }
    }
}