﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views.Common
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ImagenPage : ContentPage
	{
		public ImagenPage (string imagen)
		{
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            loadingGrid.IsVisible = true;
            ImageHomie.Finish += ImageHomie_Finish;
            ImageHomie.Source = new Uri(imagen);

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

        private void ImageHomie_Finish(object sender, FFImageLoading.Forms.CachedImageEvents.FinishEventArgs e)
        {
            loadingGrid.IsVisible = false;
        }
    }
}