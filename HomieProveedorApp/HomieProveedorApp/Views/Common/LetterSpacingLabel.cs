﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HomieProveedorApp.Views.Common
{
    public class LetterSpacingLabel : Label
    {
        public float LetterSpacing { get; set; }
    }
}
