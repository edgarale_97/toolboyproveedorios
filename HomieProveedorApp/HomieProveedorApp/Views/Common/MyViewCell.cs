﻿using System;
using System.Collections.Generic;
using System.Text;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace HomieProveedorApp
{
    public class MyViewCell : ViewCell
    {
        private CachedImage _cachedImage = null;

        protected override void OnBindingContextChanged()
        {
            if (_cachedImage == null)
            {
                foreach (var child in ((Grid)View).Children)
                {
                    if (child is CachedImage)
                    {
                        _cachedImage = (CachedImage)child;
                        break;
                    }
                }
            }

            _cachedImage.Source = null; // prevent showing old images occasionally

            var item = BindingContext as Entities.Servicio;

            if (item == null)
                return;

            _cachedImage.Source = item.Foto;

            base.OnBindingContextChanged();
        }
    }
}