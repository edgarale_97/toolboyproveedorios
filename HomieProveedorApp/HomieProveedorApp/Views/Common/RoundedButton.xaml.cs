﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views.Common
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RoundedButton : Grid
	{
		public RoundedButton ()
		{
			InitializeComponent ();
		    var tapGestureRecognizer = new TapGestureRecognizer();
		    tapGestureRecognizer.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(async () =>
		        {
		            if (this.IsEnabled)
		            {
		                this.AnchorX = 0.48;
		                this.AnchorY = 0.48;
		                await this.ScaleTo(0.8, 50, Easing.Linear);
		                //await Task.Delay(100);
		                await this.ScaleTo(1, 50, Easing.Linear);

		                if (Command != null && this.IsEnabled)
		                {
		                    Command.Execute(CommandParameter);
		                }

		                Clicked?.Invoke(this, EventArgs.Empty);
		            }
		        });
		    };

		    this.GestureRecognizers.Add(tapGestureRecognizer);
        }

        public event EventHandler Clicked;

        public static readonly BindableProperty CommandProperty = BindableProperty.Create<RoundedButton, ICommand>(p => p.Command, null);
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }


        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create<RoundedButton, object>(p => p.CommandParameter, null);
        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }


        /* CIRCLE */

        public new static BindableProperty BackgroundColorProperty =
            BindableProperty.Create(
                nameof(BackgroundColor),
                typeof(Color),
                typeof(RoundedButton),
                defaultValue: Color.Black,
                defaultBindingMode: BindingMode.OneWay
            );

        public new Color BackgroundColor
        {
            get { return (Color)GetValue(BackgroundColorProperty); }
            set { SetValue(BackgroundColorProperty, value); }
        }

        public new static BindableProperty StrokeColorProperty =
            BindableProperty.Create(
                nameof(StrokeColor),
                typeof(Color),
                typeof(RoundedButton),
                defaultValue: Color.FromHex("#D98527"),
                defaultBindingMode: BindingMode.OneWay
            );

        public new Color StrokeColor
        {
            get { return (Color)GetValue(StrokeColorProperty); }
            set { SetValue(StrokeColorProperty, value); }
        }

        /* ICON */

        public new static BindableProperty TextColorProperty =
            BindableProperty.Create(
                nameof(TextColor),
                typeof(Color),
                typeof(RoundedButton),
                defaultValue: Color.White,
                defaultBindingMode: BindingMode.OneWay
            );

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public new static BindableProperty FontSizeProperty =
            BindableProperty.Create(
                nameof(FontSize),
                typeof(double),
                typeof(RoundedButton),
                defaultValue: 14d,
                defaultBindingMode: BindingMode.OneWay
            );

        public new double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static BindableProperty IconTextProperty =
            BindableProperty.Create(
                nameof(IconText),
                typeof(string),
                typeof(RoundedButton),
                string.Empty,
                defaultBindingMode: BindingMode.OneWay,
                propertyChanged: (bindable, oldValue, newValue) => {
                    var ctrl = (RoundedButton)bindable;
                    ctrl.IconText = (string)newValue;
                }
            );

        public string IconText
        {
            get { return (string)GetValue(IconTextProperty); }
            set
            {
                SetValue(IconTextProperty, value);
                FaIcon.Text = (value);
                if (!string.IsNullOrWhiteSpace(value))
                {
                    FaIcon.IsVisible = true;
                }
            }
        }

        public static BindableProperty TextProperty =
            BindableProperty.Create(
                nameof(Text),
                typeof(string),
                typeof(RoundedButton),
                string.Empty,
                defaultBindingMode: BindingMode.OneWay,
                propertyChanged: (bindable, oldValue, newValue) => {
                    var ctrl = (RoundedButton)bindable;
                    ctrl.Text = (string)newValue;
                }
            );

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set
            {
                SetValue(TextProperty, value);
                TextLabel.Text = value;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    TextLabel.IsVisible = true;
                }
            }
        }
    }
}