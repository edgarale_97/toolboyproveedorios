﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CotizacionPage : ContentPage
	{
	    private CotizacionViewModel _vm;
        public CotizacionPage(long idServicio)
		{
            NavigationPage.SetHasNavigationBar(this,false);
            InitializeComponent ();
            _vm = new CotizacionViewModel(idServicio);
            _vm.PropertyChanged += VmOnPropertyChanged;
		    BindingContext = _vm;
            MessagingCenter.Subscribe<AddMaterialViewModel, bool>(this,"MaterialAdded", Callback);

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            if (_vm.Close)
	            {
	                Navigation.PopAsync();
	            }
	        }
	    }

	    private void Callback(AddMaterialViewModel addMaterialViewModel, bool materiales)
	    {
	        if (materiales)
	        {
	            _vm.BuscarMateriales();
	        }
	    }

	    private async void RoundedButton_OnClicked(object sender, EventArgs e)
	    {
	        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(AddMaterialPage)) &&
	            !Navigation.ModalStack.Any(x => x.GetType() == typeof(AddMaterialPage)))
	            await Navigation.PushAsync(new AddMaterialPage(_vm.IdServicio));
        }

	    private async void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        var selectedItem = ((ListView) sender).SelectedItem;
	        var material = (Materiales) selectedItem;
	        if (material.Editable)
	        {
	            if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(AddMaterialPage)) &&
	                !Navigation.ModalStack.Any(x => x.GetType() == typeof(AddMaterialPage)))
	                await Navigation.PushAsync(new AddMaterialPage(material));
	        }
	        else
	        {
	            await DisplayAlert("Error", "No se puede editar un material de una cotización aceptada", "OK");
	        }
	        
	    }
	}
}