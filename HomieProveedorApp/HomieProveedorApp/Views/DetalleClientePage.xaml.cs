﻿using System;
using System.Collections.Generic;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;

namespace HomieProveedorApp.Views
{
    public partial class DetalleClientePage : ContentPage
    {
        private DetalleClienteViewModel vm;

        public DetalleClientePage(long idCliente)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            vm = new DetalleClienteViewModel(idCliente);
            this.BindingContext = vm;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }
    }
}
