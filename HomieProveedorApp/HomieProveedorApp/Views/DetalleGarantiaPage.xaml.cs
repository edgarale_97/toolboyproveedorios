﻿using System;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalleGarantiaPage : ContentPage
    {
        private DetalleActivoViewModel viewModel;

        public DetalleGarantiaPage(long idServicio)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            viewModel = new DetalleActivoViewModel(idServicio);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
            this.BindingContext = viewModel;

            if(Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

            ListaCaracteristicas.IsVisible = true;

            var caracteristicas = new TapGestureRecognizer();
            caracteristicas.Tapped += (s, e) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    ListaCaracteristicas.IsVisible = true;
                });
            };

            var fotos = new TapGestureRecognizer();
            fotos.Tapped += (s, e) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    ListaCaracteristicas.IsVisible = false;

                });
            };
        }

        private async void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            
        }

        //private async void SolicitarButton_OnClicked(object sender, EventArgs e)
        //{
        //    var mensaje = "No se cuenta con convenio aún de esta Marca, el servicio se cobrara.";
        //    var answer = await Application.Current.MainPage.DisplayAlert("Alerta", mensaje, "Si", "No");
        //    if (answer)
        //    {
        //        var idTipo = TipoServicio.Urgente;
        //        var activo = viewModel.Activos.IdActivos;
        //        var vigencia = viewModel.Activos.BoolVigente;
        //        if (viewModel.Direccion == null)
        //        {
        //            await Application.Current.MainPage.DisplayAlert("Alerta", "Debe seleccionar una dirección.", "OK");
        //            return;
        //        }
        //        if (viewModel.IsComplete == true)
        //        {
        //            if (viewModel.CategoriaSeleccionada != null)
        //            {
        //                Navigation.PushAsync(new Page2SeleccionCategoria(idTipo, viewModel.CategoriaSeleccionada, viewModel.Direccion, (int)activo, vigencia));
        //            }
        //        }
        //    }
        //}

    }
}
