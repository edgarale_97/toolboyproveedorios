﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using HomieProveedorApp.Views.Common;
using Microsoft.AspNet.SignalR.Client.Http;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetalleServicioPage : ContentPage
	{
	    private DetalleServicioViewModel _vm;

        private bool isGarantia;

		public DetalleServicioPage (long idServicio, int IdTipo, bool finalizado = false)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
            _vm = new DetalleServicioViewModel(idServicio, IdTipo);
            _vm.PropertyChanged += VmOnPropertyChanged;
		    BindingContext = _vm;

            //map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(new Position(19.4326018d, -99.1353936d), 12d);

            /*mapImage.GestureRecognizers.Add(new TapGestureRecognizer(view =>
            {
                var uri = string.Empty;
                Device.OnPlatform(iOS: ()=> { uri = "http://maps.apple.com/maps"; }, Android: ()=> {uri = "http://maps.google.com/maps";});
                Device.OpenUri(new Uri($"{uri}?q={_vm.Servicio.Latitud},{_vm.Servicio.Longitud}"));
            }));*/

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
		    if (finalizado)
		    {
		        StackBotones.IsVisible = false;
		        bar.IsVisibleMenu = false;
            }

            if(_vm.IdTipo == 1)
            {
                LabelTipo.Text = "Garantía";
            }
        }

	    private async void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Servicio")
	        {
	            //AddMarker((double)_vm.Servicio.Latitud, (double)_vm.Servicio.Longitud, _vm.Servicio.Direccion);
	            if (_vm.Servicio.Cargos.Count > 0)
	            {
	                ListViewRecibos.HeightRequest = _vm.Servicio.Cargos.Count * 33;
	            }
	            else
	            {
	                ListViewRecibos.HeightRequest = 0;
	            }

	            if (_vm.Servicio.Estatus == EstatusServicioCliente.Terminado)
	            {
	                StackBotones.IsVisible = false;
	                bar.IsVisibleMenu = false;
	            }

	            if (_vm.Servicio != null)
	            {
	                ListViewCategorias.HeightRequest = _vm.Servicio.Categorias.Count * 50;
	            }
            }
	        if (e.PropertyName == "Close")
	        {
	            if (_vm.Close)
	            {
	                await Navigation.PopAsync();
	            }
	        }
	    }

	    //private async void AddMarker(double latitud, double longitud, string direccion)
	    //{
	    //    var icon = BitmapDescriptorFactory.FromView(new BindingPinView());
	    //    await Task.Delay(1000);

     //       map.Pins.Clear();
	    //    var pin = new Pin
	    //    {
	    //        Type = PinType.Place,
	    //        Label = string.IsNullOrEmpty(direccion) ? "-" : direccion,
	    //        Position = new Position(latitud, longitud),
	    //        IsDraggable = false,
	    //        Icon = icon
     //       };
	    //    map.Pins.Add(pin);
	    //    map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitud, longitud), Distance.FromMeters(50)), true);
	    //}

        private async void MenuItem_OnClicked(object sender, EventArgs e)
	    {
	        var chat = new ChatPage(_vm.IdServicio, _vm.IdTipo, false);
	        //chat.Disappearing += ServicioPage_Disappearing;
	        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)) &&
	            !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChatPage)))
	            await Navigation.PushAsync(chat);
        }

	    private async void RoundedButton_OnClicked(object sender, EventArgs e)
	    {
	        var resumen = new ResumenServicioPage(_vm.IdServicio);
	        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ResumenServicioPage)) &&
	            !Navigation.ModalStack.Any(x => x.GetType() == typeof(ResumenServicioPage)))
	        {
	            await Navigation.PushAsync(resumen);
	            MessagingCenter.Subscribe<ResumenServicioPage, bool>(this, "ServicioTerminado", async (page, b) =>
	            {
	                if (b)
	                {
	                    MessagingCenter.Unsubscribe<ResumenServicioPage, bool>(this, "ServicioTerminado");
                        await Navigation.PopAsync();
	                }
	            });
            }

        }

	    private async void IniciarServicioButton_OnClicked(object sender, EventArgs e)
	    {
            var documentacion = new DocumentacionInicialPage(_vm.IdServicio);
	        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DocumentacionInicialPage)) &&
	            !Navigation.ModalStack.Any(x => x.GetType() == typeof(DocumentacionInicialPage)))
	        {
	            await Navigation.PushAsync(documentacion);
	            MessagingCenter.Subscribe<DocumentacionInicialPage, bool>(this, "DocumentacionInicialTerminada", (page, b) =>
	            {
	                MessagingCenter.Unsubscribe<ResumenServicioPage, bool>(this, "ServicioTerminado");

	                _vm.Buscar();
	            });
            }
	    }

	    private async void Bar_OnClicked(object sender, EventArgs e)
	    {
	        string accion;
	        if (_vm.Servicio.Estatus == EstatusServicioCliente.Iniciado || _vm.Servicio.Estatus == EstatusServicioCliente.Terminado)
	        {
                var options = new List<string>();

                if (_vm.Servicio.ChatHabilitado)
                {
                    options = new List<string> { "Chat", "Materiales Extra", "En Camino", "Llamada de emergencia", "Perfil Cliente"};
                }
                else
                {
                    options = new List<string> { "Materiales Extra", "En Camino", "Llamada de emergencia" };
                }

	            accion = await DisplayActionSheet("Mas opciones", "Cancelar", null, options.ToArray());
	        }
	        else
	        {
                var options = new List<string>();

                if (_vm.Servicio.ChatHabilitado)
                {
                    options = new List<string> { "Chat", "Materiales Extra", "En Camino", "Llamada de emergencia", "Perfil Cliente", "Cancelar Servicio" };
                }
                else
                {
                    if (_vm.ServicioIniciado)
                    {
                        options = new List<string>
                        {
                            "Materiales Extra",
                            "En Camino",
                            "Llamada de emergencia",
                            "Cancelar Servicio"
                        };
                    }
                    else
                    {
                        options = new List<string>
                        {
                            "En Camino",
                            "Llamada de emergencia",
                            "Cancelar Servicio"
                        };
                    }
                    
                }

	            accion = await DisplayActionSheet("Mas opciones", "Cancelar", null, options.ToArray());
            }
	        
	        switch (accion)
	        {
                case "Chat":
                    var chat = new ChatPage(_vm.IdServicio, _vm.IdTipo, false);
                    //chat.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChatPage)))
                        await Navigation.PushAsync(chat);
                    break;
                case "Materiales Extra":
                    var cotizacion = new CotizacionPage(_vm.IdServicio);
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CotizacionPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(CotizacionPage)))
                        await Navigation.PushAsync(cotizacion);
                    break;
                case "En Camino":
                    _vm.EnCaminoCommand.Execute(null);
                    break;
                case "Llamada de emergencia":
                    _vm.EmergenciaCommand.Execute(null);
                    break;
                case "Cancelar Servicio":
                    _vm.CancelarCommand.Execute(null);
                    break;
                case "Perfil Cliente":
                    //Abre vista del cliente.
                    var profileClient = new DetalleClientePage(_vm.Servicio.IdCliente);
                    await Navigation.PushAsync(profileClient);
                    break;

            }
	    }
	}
}