﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DocumentacionInicialPage : ContentPage
	{
	    private DocumentacionInicialViewModel viewModel;

		public DocumentacionInicialPage (long idServicio)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
            viewModel = new DocumentacionInicialViewModel(idServicio);
            viewModel.PropertyChanged += ViewModelOnPropertyChanged;
		    BindingContext = viewModel;

            var tgr = new TapGestureRecognizer();
		    tgr.Tapped += (sender, args) =>
		    {
		        viewModel.TakePicture();
		    };
            ImagenInicio.GestureRecognizers.Add(tgr);

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
             
        }

	    private async void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            if (viewModel.Close)
	            {
	                MessagingCenter.Send<DocumentacionInicialPage, bool>(this, "DocumentacionInicialTerminada", true);
                    await Navigation.PopAsync();
	            }
	        }
	    }

        private async void SwitchEstado_OnToggled(object sender, ToggledEventArgs e)
		{
			viewModel.RefaccionesToggle = e.Value;
		}

	}
}