﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void ButtonIniciarSesion_OnClicked(object sender, EventArgs e)
	    {
	        MessagingCenter.Send(this, "Login");
        }
	    private void ButtonUnete_OnClicked(object sender, EventArgs e)
	    {
	        MessagingCenter.Send(this, "Registro");
	    }

        protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        // Workaround to fix image placement issue in iOS. It occurrs only when this 
	        // page (within a NavigationPage) is set as the detail of the MasterDetailPage 
	        // i.e. Display it from the RootPage
	        // navigation view
	        var content = this.Content;
	        this.Content = null;
	        this.Content = content;

            Animations();
        }

        private async void Animations()
        {
            ImageVuker.Opacity = 0;
            ImageVuker.IsVisible = true;
            ImageVuker.Scale = 0.8;

            ButtonUnete.Opacity = 0;
            ButtonUnete.IsVisible = true;
            ButtonIniciarSesion.Opacity = 0;
            ButtonIniciarSesion.IsVisible = true;

            await Task.WhenAll(
                ImageVuker.FadeTo(1, 600),
                ImageVuker.ScaleTo(1, 600)
            );

            await Task.WhenAll(
                ButtonUnete.FadeTo(1, 500),
                ButtonIniciarSesion.FadeTo(1, 500)
            );
        }
    }
}