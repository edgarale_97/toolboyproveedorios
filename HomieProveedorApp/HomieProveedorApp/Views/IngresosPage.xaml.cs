﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IngresosPage : ContentPage
	{
	    private IngresosViewModel viewModel;
		public IngresosPage ()
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
		    viewModel = new IngresosViewModel();
            
		    BindingContext = viewModel;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private async void FechaInicio_OnDateSelected(object sender, DateChangedEventArgs e)
	    {
            await viewModel.Buscar();
        }

	    private async void FechaFin_OnDateSelected(object sender, DateChangedEventArgs e)
	    {
            FechaInicio.MaximumDate = FechaFin.Date;
            await viewModel.Buscar();
        }
	}
}