﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
	    private LoginViewModel _vm;
	    public event EventHandler LoginConfirmado;
        public LoginPage ()
		{
			InitializeComponent ();
		    _vm = new LoginViewModel();
		    _vm.PropertyChanged += _vm_PropertyChanged;
		    this.BindingContext = _vm;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }
	    private void _vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Close")
	        {
	            OnLoginConfirmado();
	        }
	    }

	    protected virtual void OnLoginConfirmado()
	    {
	        EventHandler handler = LoginConfirmado;
	        if (handler != null)
	        {
	            handler(this, EventArgs.Empty);
	        }
	    }

        private async void Recover_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new PasswordRecoveryPage());
        }
    }
}