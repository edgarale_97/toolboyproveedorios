﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using HomieProveedorApp.ViewModel;
using Plugin.VersionTracking;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views.Navigation
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainMenuPage : ContentPage
	{
	    private readonly INavigation _navigation;
	    public event EventHandler CerrarSesion;

        public MainMenuPage (INavigation navigation)
		{
			InitializeComponent ();
		    _navigation = navigation;

		    BindingContext = new SamplesViewModel(navigation);
		    //LabelNombre.Text = Settings.User;

		    var vt = CrossVersionTracking.Current;
		    LabelVersion.Text = $"v{vt.CurrentVersion}";

		    SwitchEstado.IsToggled = Settings.PedidosUrgentes;
		    SwitchEstado.Toggled += SwitchEstado_OnToggled;
        }

	    private async void SampleListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        var sample = sampleListView.SelectedItem as Sample;

	        if (sample != null)
	        {
	            if (sample.PageType == typeof(CerrarSesionPage))
	            {
	                var answer = await DisplayAlert("Cerrar Sesión", "Estás seguro que deseas cerrar sesión?", "Si", "No");
	                if (answer)
	                {
	                    var model = new LoginModel();
	                    await model.LogOutAsync();

	                    OnCerrarSesion();
	                }
	            }
	            else
	            {
	                await sample.NavigateToSample(_navigation);
	            }

	            sampleListView.SelectedItem = null;
	        }
        }

	    private async void OnCloseButtonClicked(object sender, EventArgs args)
	    {
	        await Navigation.PopAsync();
	    }

	    protected virtual void OnCerrarSesion()
	    {
	        EventHandler handler = CerrarSesion;
	        if (handler != null)
	        {
	            handler(this, EventArgs.Empty);
	        }
	    }

	    private async void SwitchEstado_OnToggled(object sender, ToggledEventArgs e)
	    {
	        Settings.PedidosUrgentes = SwitchEstado.IsToggled;

            var model = new PedidoUrgenteModel();
	        await model.PostAsync(SwitchEstado.IsToggled);
	    }
    }
}