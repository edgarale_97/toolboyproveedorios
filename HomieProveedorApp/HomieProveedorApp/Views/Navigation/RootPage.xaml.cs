﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using HomieProveedorApp.ViewModel;
using HomieProveedorApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views.Navigation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RootPage : MasterDetailPage
    {

        public event EventHandler CerrarSesion;

        public RootPage()
        {
            InitializeComponent();

            // Empty pages are initially set to get optimal launch experience
            Master = new ContentPage { Title = "Homie Proveedores" };
            var page = new ContentPage
            {
                BackgroundColor = Color.White,
                Content = new CustomActivityIndicator
                {
                    IsVisible = true,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                }
            };
            Detail = NavigationPageHelper.Create(page);

            MessagingCenter.Subscribe<MainLabel>(this, "Menu", (sender) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.IsPresented = true;
                });
            });

            this.IsGestureEnabled = false;
        }
        public async void OnSettingsTapped(Object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new SettingsPage());
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var navigationPage = string.Empty;
            if (Detail?.Navigation != null && Detail.Navigation.NavigationStack.Count > 0)
            {
                navigationPage = Detail.Navigation.NavigationStack[0].GetType().Name;
                Debug.WriteLine("Navigation stack: " + navigationPage);
            }

            if (navigationPage != "TarjetasPage")
            {
                await NavigationService.BeginInvokeOnMainThreadAsync(InitializeMasterDetail);

                var model = new LoginModel();
                var result = await model.AutenticarUsuarioAsync(Settings.User, Settings.Password);

                if (result.Error)
                {
                    await model.LogOutAsync();
                    Settings.User = string.Empty;
                    Settings.Password = string.Empty;
                    Settings.Token = string.Empty;
                    Settings.Confirmado = false;
                    OnCerrarSesion();
                }
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void LaunchSampleInDetail(Page page, bool animated)
        {
            Detail = NavigationPageHelper.Create(page);
            IsPresented = false;
        }

        private async void InitializeMasterDetail()
        {
            var master = new MainMenuPage(new NavigationService(Navigation, LaunchSampleInDetail));
            master.CerrarSesion += Master_CerrarSesion;
            Master = master;

            var detail = new TabPage();
            detail.CerrarSesion += Master_CerrarSesion;
            Detail = NavigationPageHelper.Create(detail);
        }

        private void Master_CerrarSesion(object sender, EventArgs e)
        {
            OnCerrarSesion();
        }

        protected virtual void OnCerrarSesion()
        {
            EventHandler handler = CerrarSesion;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}