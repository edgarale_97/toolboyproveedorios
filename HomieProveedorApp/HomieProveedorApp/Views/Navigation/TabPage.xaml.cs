﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BottomBar.XamarinForms;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.ViewModel;
using HomieProveedorApp.Views.Common;
using Newtonsoft.Json;
using Plugin.Badge;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs;

namespace HomieProveedorApp.Views.Navigation
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TabPage : TabbedPage
	{
	    public event EventHandler CerrarSesion;

        public TabPage ()
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();

		    //this.BarTextColor = Color.FromHex("#00a99d");
		    //this.FixedMode = true;

		    MessagingCenter.Subscribe<MainLabelNavigation>(this, "AbrirServicios", async (sender) =>
		    {
		        await AbrirServicios();
		    });

		    MessagingCenter.Subscribe<SolicitudesViewModel, long>(this, "SolicitudAceptada", async (model, id) =>
		    {
		        if (id != 0)
		        {
		            await AbrirServicios();
		            await Navigation.PushAsync(new ChatPage(id, 1, false));

		        }
		    });

            MessagingCenter.Subscribe<ResumenServicioViewModel>(this,"ServicioTerminado", async model =>
            {
                await AbrirServicios();
            });

            TabPerfil.CerrarSesion += TabPerfilOnCerrarSesion;

            // Cuando llega notificación con la app en primer plano.
            MessagingCenter.Subscribe<App, Notificaciones>((App)Application.Current, "HubNotification", async (sender, notificacion) =>
		    {
		        OpenFromNotification(notificacion);
		    });

            // Notificación seleccionada desde la pantalla de notificaciones.
		    MessagingCenter.Subscribe<App, Notificaciones>((App)Application.Current, "NotificacionSeleccionada", async (sender, notificacion) =>
		    {
		        OpenNotificacionSeleccionada(notificacion);
		    });

            // Notificación abierta desde el notification bar.
		    MessagingCenter.Subscribe<App, Notificaciones>((App)Application.Current, "OpenFromNotificationBar", async (sender, notificacion) =>
		    {
		        OpenFromNotificacionBar(notificacion);
		    });

            MessagingCenter.Subscribe<App, long>((App)Application.Current, "DetalleServicio", async (sender, id) => {

                // Mostramos el view del servicio.
                await Navigation.PopToRootAsync();
                await Navigation.PushAsync(new DetalleServicioPage(id, 1));
            });

            if (Device.RuntimePlatform == Device.iOS)
		    {
		        TabServicios.Padding = new Thickness(0, 30, 0, 0);
				TabGarantias.Padding = new Thickness(0, 30, 0, 0);
				TabHome.Padding = new Thickness(0, 30, 0, 0);
		        TabNotificaciones.Padding = new Thickness(0, 30, 0, 0);
		        TabPerfil.Padding = new Thickness(0, 30, 0, 0);
		    }

            BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, Settings.Notificaciones);
        }

	    private void TabPerfilOnCerrarSesion(object o, EventArgs eventArgs)
	    {
	        OnCerrarSesion();
	    }


	    protected virtual void OnCerrarSesion()
	    {
	        EventHandler handler = CerrarSesion;
	        if (handler != null)
	        {
	            handler(this,EventArgs.Empty);
	        }
	    }

	    public async Task AbrirServicios()
	    {
	        await Navigation.PopToRootAsync();

	        var vm = TabServicios.BindingContext as ServiciosViewModel;
	        await vm.BuscarServicios();
	        this.SelectedItem = TabServicios;
	    }

	    public async Task AbrirSolicitudes()
	    {
	        await Navigation.PopToRootAsync();

	        var vm = TabHome.BindingContext as SolicitudesViewModel;
	        await vm.Buscar();
	        this.SelectedItem = TabHome;
	    }

	    protected async override void OnAppearing()
	    {
	        base.OnAppearing();
	        BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, Settings.Notificaciones);
	    }

        private async void OpenFromNotification(Notificaciones notificacion)
	    {
	        //BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, Settings.Notificaciones);

            // Si llegá una notificación
            if (!string.IsNullOrWhiteSpace(notificacion.PageToOpen))
	        {
	            var parameters = JsonConvert.DeserializeObject<NotificationParameters>(notificacion.Parameters);
				Settings.Notificaciones = 1;
				CrossBadge.Current.ClearBadge();
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, 0);
				CrossBadge.Current.SetBadge(Settings.Notificaciones);
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, Settings.Notificaciones);
	            switch (notificacion.PageToOpen)
	            {
	                case NotificacionPage.Servicio:
	                    break;

	                case NotificacionPage.Chat:

	                    if (Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
	                    {
	                        var page = Navigation.NavigationStack.Where(x => x.GetType() == typeof(ChatPage));

	                        if (page != null)
	                        {
								var chat = (ChatPage)page;
								await chat.Update();
	                        }
                        }
	                    else
	                    {
	                        if (Device.RuntimePlatform == Device.iOS)
	                            await Application.Current.MainPage.DisplayAlert(notificacion.Titulo, notificacion.Descripcion, "OK");
	                    }

                        break;

	                case NotificacionPage.SolicitudServicio:

                        break;
                }
	        }
	    }

        private async void OpenNotificacionSeleccionada(Notificaciones notificacion)
        {
            // Cuando se abre directamente del apartado de notificaciones
            if (!string.IsNullOrWhiteSpace(notificacion.PageToOpen))
            {
                var parameters = JsonConvert.DeserializeObject<NotificationParameters>(notificacion.Parameters);
				CrossBadge.Current.ClearBadge();
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, 0);
				Settings.Notificaciones = 0;
				switch (notificacion.PageToOpen)
                {
                    case NotificacionPage.Servicio:
                        await AbrirServicios();

                        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                            !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                        {
                            //if (parameters.TiempoEspera > 0)
                            //{
                            //    await Task.Delay(parameters.TiempoEspera);
                            //}

                            await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, 1));
                        }

                        break;

                    case NotificacionPage.Chat:

                        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
                        {
                            await AbrirServicios();

                            await Navigation.PushAsync(new ChatPage(parameters.IdServicio, 1, false));
                        }
                        else
                        {
							var page = Navigation.NavigationStack.FirstOrDefault(x => x.GetType() == typeof(ChatPage));

							if (page != null)
							{
								var chat = (ChatPage)page;
								await chat.Update();
							}
						}

                        break;

                    case NotificacionPage.SolicitudServicio:

                        await AbrirSolicitudes();

                        break;
                }
            }
        }

	    private async void OpenFromNotificacionBar(Notificaciones notificacion)
	    {
	        if (!string.IsNullOrWhiteSpace(notificacion.PageToOpen))
	        {
	            var parameters = JsonConvert.DeserializeObject<NotificationParameters>(notificacion.Parameters);
				CrossBadge.Current.ClearBadge();
				BottomBarPageExtensions.SetBadgeCount(TabNotificaciones, 0);
				Settings.Notificaciones = 0;
				switch (notificacion.PageToOpen)
	            {
	                case NotificacionPage.Servicio:
	                    await AbrirServicios();

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
	                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
	                    {
	                        //if (parameters.TiempoEspera > 0)
	                        //{
	                        //    await Task.Delay(parameters.TiempoEspera);
	                        //}

	                        await Navigation.PushAsync(new DetalleServicioPage(parameters.IdServicio, 1));
	                    }

                        break;

	                case NotificacionPage.Chat:

	                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)))
	                    {
	                        await Navigation.PushAsync(new ChatPage(parameters.IdServicio, 1, false));
	                    }
						else
						{
							var page = Navigation.NavigationStack.FirstOrDefault(x => x.GetType() == typeof(ChatPage));

							if (page != null)
							{
								var chat = (ChatPage)page;
								await chat.Update();
							}
						}

						break;

	                case NotificacionPage.SolicitudServicio:

	                    await AbrirSolicitudes();

	                    break;
	            }
	        }
	    }
    }
}