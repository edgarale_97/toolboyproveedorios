﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NotificacionesPage : ContentPage
	{
	    private NotificacionesViewModel _vm;
	    public NotificacionesPage()
	    {
	        NavigationPage.SetHasNavigationBar(this, false);
	        InitializeComponent();

	        _vm = new NotificacionesViewModel();
	        _vm.PropertyChanged += VmOnPropertyChanged;
	        BindingContext = _vm;

	        if (Device.RuntimePlatform == Device.iOS)
	        {
	            this.Padding = new Thickness(0, 30, 0, 0);
	        }
        }

	    private void VmOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
	    {

	    }

	    private void ListViewNotificaciones_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        var notificacionSeleccionada = (Notificaciones)e.Item;

	        if (notificacionSeleccionada != null)
	        {
	            MessagingCenter.Send<App, Notificaciones>((App)Xamarin.Forms.Application.Current, "NotificacionSeleccionada", notificacionSeleccionada);
            }
	    }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

	        _vm.Buscar();
        }
	}
}