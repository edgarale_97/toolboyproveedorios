﻿using System;
using System.Collections.Generic;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;

namespace HomieProveedorApp.Views
{
    public partial class PasswordRecoveryPage : ContentPage
    {
        private PasswordRecoveryViewModel vm;

        public PasswordRecoveryPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            vm = new PasswordRecoveryViewModel();
            BindingContext = vm;

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }
        }
    }
}
