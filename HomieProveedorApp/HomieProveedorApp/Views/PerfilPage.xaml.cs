﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using HomieProveedorApp.ViewModel;
using Plugin.VersionTracking;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PerfilPage : ContentPage
	{
	    private readonly SamplesViewModel _vm;
	    public event EventHandler CerrarSesion;
		public PerfilPage ()
		{
			InitializeComponent ();

		    _vm = new SamplesViewModel();
		    BindingContext = _vm;

		    var vt = CrossVersionTracking.Current;
		    LabelVersion.Text = $"v{vt.CurrentVersion}";
		    var a = sampleListView.GroupDisplayBinding;

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private async void SampleListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        var sample = sampleListView.SelectedItem as Sample;
	        if (sample != null)
	        {
	            if (sample.PageType == typeof(CerrarSesionPage))
	            {
	                var answer = await DisplayAlert("Cerrar Sesión", "¿Estás seguro que deseas cerrar sesión?", "Si",
	                    "No");
	                if (answer)
	                {
	                    var model = new LoginModel();
	                    await model.LogOutAsync();

	                    OnCerrarSesion();

	                }

	            }
	            else
	            {
	                var page = Activator.CreateInstance(sample.PageType) as Page;
	                await Navigation.PushAsync(page);
                }
	            sampleListView.SelectedItem = null;

	        }
	    }

	    protected virtual void OnCerrarSesion()
	    {
	        EventHandler handler = CerrarSesion;
	        if (handler != null)
	        {
	            handler(this, EventArgs.Empty);
	        }
	    }

        private async void SwitchEstado_OnToggled(object sender, ToggledEventArgs e)
	    {
	        Settings.PedidosUrgentes = e.Value;

	        var model = new PedidoUrgenteModel();
	        await model.PostAsync(e.Value);
	    }
	}
}