﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using HomieProveedorApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResumenServicioPage : ContentPage
	{
	    private ResumenServicioViewModel viewModel;
		public ResumenServicioPage (long idServicio)
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
            viewModel = new ResumenServicioViewModel(idServicio);
            viewModel.PropertyChanged += ViewModelOnPropertyChanged;
		    BindingContext = viewModel;

            //map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(new Position(19.4326018d, -99.1353936d), 12d);
            var tgr = new TapGestureRecognizer();
		    tgr.Tapped += (sender, args) =>
		    {
		        viewModel.TakePicture();
		    };

            ImagenFinal.GestureRecognizers.Add(tgr);

		    /*mapImage.GestureRecognizers.Add(new TapGestureRecognizer(view =>
		    {
		        var uri = string.Empty;
		        Device.OnPlatform(iOS: () => { uri = "http://maps.apple.com/maps"; }, Android: () => { uri = "http://maps.google.com/maps"; });
		        Device.OpenUri(new Uri($"{uri}?q={viewModel.Servicio.Latitud},{viewModel.Servicio.Longitud}"));
		    }));*/

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private async void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Servicio")
	        {
                //AddMarker((double)viewModel.Servicio.Latitud, (double)viewModel.Servicio.Longitud, viewModel.Servicio.Direccion);
	            if (viewModel.Servicio != null)
	            {
	                ListViewCategorias.HeightRequest = viewModel.Servicio.Categorias.Count * 50;
                    if(viewModel.Servicio.Materiales.Count > 0)
					{
						ListViewMateriales.HeightRequest = viewModel.Servicio.Materiales.Count * 33;
					}
                    else
					{
						ListViewMateriales.HeightRequest = 0;
					}
	            }
                else
				{

				}
            }
	        if (e.PropertyName == "Close")
	        {
	            if (viewModel.Close)
	            {
	                MessagingCenter.Send<ResumenServicioPage, bool>(this,"ServicioTerminado",true);
	                await Navigation.PopAsync();
	            }
	        }
	    }

        private async void SwitchEstado_OnToggled1(object sender, ToggledEventArgs e)
		{
            var sw1 = SwitchEstado1.IsToggled;
            var sw2 = SwitchEstado2.IsToggled;
            var sw3 = SwitchEstado3.IsToggled;
            if (sw1)
            {
                if (sw2)
                {
                    SwitchEstado2.IsToggled = false;
                }
                if (sw3)
                {
                    SwitchEstado3.IsToggled = false;
                }
            }
        }

        private async void SwitchEstado_OnToggled2(object sender, ToggledEventArgs e)
		{
            var sw1 = SwitchEstado1.IsToggled;
            var sw2 = SwitchEstado2.IsToggled;
            var sw3 = SwitchEstado3.IsToggled;
            if (sw2)
            {
                if (sw1)
                {
                    SwitchEstado1.IsToggled = false;
                }
                if (sw3)
                {
                    SwitchEstado3.IsToggled = false;
                }
            }
        }

        private async void SwitchEstado_OnToggled3(object sender, ToggledEventArgs e)
		{
            var sw1 = SwitchEstado1.IsToggled;
            var sw2 = SwitchEstado2.IsToggled;
            var sw3 = SwitchEstado3.IsToggled;
            if (sw3)
            {
                if (sw1)
                {
                    SwitchEstado1.IsToggled = false;
                }
                if (sw2)
                {
                    SwitchEstado2.IsToggled = false;
                }
            }
        }

		//private async void AddMarker(double latitud, double longitud, string direccion)
		//{
		//    var icon = BitmapDescriptorFactory.FromView(new BindingPinView());
		//    await Task.Delay(1000);
		//    map.Pins.Clear();
		//    var pin = new Pin
		//    {
		//        Type = PinType.Place,
		//        Label = string.IsNullOrEmpty(direccion) ? "-" : direccion,
		//        Position = new Position(latitud, longitud),
		//        IsDraggable = false,
		//        Icon = icon
		//       };
		//       map.Pins.Add(pin);
		//       map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitud, longitud),Distance.FromMeters(50)), true);
		//}

	}
}