﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ServiciosPage : ContentPage
	{
	    private ServiciosViewModel _vm;
		public ServiciosPage ()
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();

            _vm = new ServiciosViewModel();
            _vm.PropertyChanged += VmOnPropertyChanged;
		    BindingContext = _vm;

		    LabelListServicios.IsVisible = true;
		    LabelListFinalizados.IsVisible = false;

		    ListaServicios.IsVisible = true;
		    ListFinalizados.IsVisible = false;

            var tgr2 = new TapGestureRecognizer();
		    tgr2.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(async () =>
		        {
		            ListaServicios.IsVisible = true;
		            //ServiciosIcon.TextColor = (Color) Application.Current.Resources["WizardCompleteColor"];
		            ServiciosLabel.TextColor = (Color) Application.Current.Resources["WizardCompleteColor"];

		            ListFinalizados.IsVisible = false;
		            //FinalizadosIcon.TextColor = (Color) Application.Current.Resources["WizardInclompleteColor"];
		            FinalizadosLabel.TextColor = (Color) Application.Current.Resources["WizardInclompleteColor"];

		            _vm.PaginaServicios = 0;
		            await _vm.BuscarServicios();
		        });
		    };
            ServiciosLayout.GestureRecognizers.Add(tgr2);

            var tgr3 = new TapGestureRecognizer();
		    tgr3.Tapped += (s, e) =>
		    {
		        Device.BeginInvokeOnMainThread(async () =>
		        {
		            ListaServicios.IsVisible = false;
		            //ServiciosIcon.TextColor = (Color) Application.Current.Resources["WizardInclompleteColor"];
		            ServiciosLabel.TextColor = (Color) Application.Current.Resources["WizardInclompleteColor"];

		            ListFinalizados.IsVisible = true;
		            //FinalizadosIcon.TextColor = (Color) Application.Current.Resources["WizardCompleteColor"];
		            FinalizadosLabel.TextColor = (Color) Application.Current.Resources["WizardCompleteColor"];

		            _vm.PaginaFinalizados = 0;
		            await _vm.BuscarFinalizados();
		        });
		    };
            FinalizadosLayout.GestureRecognizers.Add(tgr3);

            _vm.Initialize();

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private async void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Servicios")
	        {
	            if (_vm.Servicios.Count() == 0)
	            {
	                LabelListServicios.IsVisible = true;
	            }
	            else
	            {
	                LabelListServicios.IsVisible = false;
	            }
                _vm.Servicios.CollectionChanged += ServiciosOnCollectionChanged;
	        }
	        if (e.PropertyName == "Finalizados")
	        {
	            if (_vm.Finalizados.Count() == 0)
	            {
	                LabelListFinalizados.IsVisible = true;
	            }
	            else
	            {
	                LabelListFinalizados.IsVisible = false;
	            }
                _vm.Finalizados.CollectionChanged += FinalizadosOnCollectionChanged;
	        }
	        if (e.PropertyName == "PendientesCalificar")
	        {
	            if (_vm.PendientesCalificar)
	            {
	                var calificar = new CalificarClientePage(_vm.CalificacionData);
	                if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(CalificarClientePage)) &&
	                    !Navigation.ModalStack.Any(x => x.GetType() == typeof(CalificarClientePage)))
	                    await Navigation.PushAsync(calificar);
	            }
	        }
	    }

	    private void FinalizadosOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
	    {
	        if (_vm.Finalizados.Count() == 0)
	        {
	            LabelListFinalizados.IsVisible = true;
	        }
	        else
	        {
	            LabelListFinalizados.IsVisible = false;
	        }
        }

	    private void ServiciosOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
	    {
	        if (_vm.Servicios.Count() == 0)
	        {
	            LabelListServicios.IsVisible = true;
	        }
	        else
	        {
	            LabelListServicios.IsVisible = false;
	        }
        }

	    private async void ListViewEspera_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        var selectedItem = ((ListView)sender).SelectedItem;
	        var servicio = (Entities.Servicio)selectedItem;
	        var detalle = new DetalleServicioPage(servicio.IdServicio, servicio.IdTipo, true);
	        detalle.Disappearing += ServicioPage_Disappearing;
	        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
	            !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
	            await Navigation.PushAsync(detalle, true);
        }

	    private async void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        //Servicios proximos
	        var selectedItem = ((ListView) sender).SelectedItem;
	        var servicio = (Entities.Servicio) selectedItem;
	        switch (servicio.Estatus)
	        {
                case ServicioEstatus.SolicitudAceptada:
                    if (servicio.ChatHabilitado)
                    {
                        var chat = new ChatPage(servicio.IdServicio, servicio.IdTipo, false);
                        chat.Disappearing += ServicioPage_Disappearing;
                        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChatPage)) &&
                            !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChatPage)))
                            await Navigation.PushAsync(chat);
                    }
                    else
                    {
                        var det = new DetalleServicioPage(servicio.IdServicio, servicio.IdTipo);
                        det.Disappearing += ServicioPage_Disappearing;
                        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                            !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                            await Navigation.PushAsync(det);
                    }
                    
                    break;
                case ServicioEstatus.AceptadoCliente:
                case ServicioEstatus.Confirmado:
                case ServicioEstatus.Iniciado:
                    var detalle = new DetalleServicioPage(servicio.IdServicio, servicio.IdTipo);
                    detalle.Disappearing += ServicioPage_Disappearing;
                    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(DetalleServicioPage)) &&
                        !Navigation.ModalStack.Any(x => x.GetType() == typeof(DetalleServicioPage)))
                        await Navigation.PushAsync(detalle);
                    break;

	        }
	    }

	    private async void ServicioPage_Disappearing(object sender, EventArgs e)
	    {
	        if (sender.GetType() == typeof(ChatPage))
	        {
	            _vm.PaginaServicios = 0;
	            await _vm.BuscarServicios();
	        }

	        //if (sender.GetType() == typeof(EventoDiasPage))
	        //{
	        //    var resumen = (EventoDiasPage)sender;
	        //    if (resumen.IsCompleted)
	        //    {
	        //        viewModel.Pagina = 0;
	        //        await viewModel.Buscar();
	        //    }
	        //}
	    }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        _vm.BuscarServicios();
	    }

	    private async void ArticleView_Disappearing(object sender, EventArgs e)
	    {

	        if (ListaServicios.IsVisible)
	        {
	            _vm.PaginaServicios = 0;
	            await _vm.BuscarServicios();
	        }
	        else
	        {
	            _vm.PaginaFinalizados = 0;
	            await _vm.BuscarFinalizados();
	        }

	    }
    }
}