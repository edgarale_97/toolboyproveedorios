﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.Helpers;
using HomieProveedorApp.Models;
using HomieProveedorApp.ViewModel;
using HomieProveedorApp.Views.Common;
using ImageCircle.Forms.Plugin.Abstractions;
using Xamarin.Forms;

namespace HomieProveedorApp.Views
{
    public partial class SettingsPage : ContentPage
    {
        public bool Lunes;

        private SettingViewModel setting;

        public SettingsPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            MessagingCenter.Subscribe<byte[]>(this, "ImageSelected", (args) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var source = ImageSource.FromStream(() => new MemoryStream(args));
                    setting.Image = source;
                    setting.User.Foto = args;
                    //SwitchView(source);
                });

            });
            setting = new SettingViewModel();
            BindingContext = setting;
            
            //SetGestureRecognizers();

            //if (Device.RuntimePlatform == Device.iOS)
            //{
            //    this.Padding = new Thickness(0, 30, 0, 0); 4152 3132 4970 2973
            //}

        }

        //private async void SwitchView(ImageSource source)
        //{
        //    await Navigation.PushAsync(new SfImageEditorPage() { ImageSource = source });
        //}

        //private void PickerBanco_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    setting.User.Banco = PickerBanco.Items[PickerBanco.SelectedIndex];
        //}

        private void Clabe_Unfocused(object sender, FocusEventArgs e)
        {
            ClabeValidationMethod();
        }

        private void ClabeValidationMethod()
        {
            if (string.IsNullOrEmpty(Clabe_Bancaria.Text))
            {
                Clabe_Bancaria.PlaceholderColor = Color.Red;
                ClabeValidation.IsVisible = true;
                setting.User.Banco = "";
                setting.Banco = "";
            }
            else
            {
                Clabe_Bancaria.PlaceholderColor = Color.Default;
                ClabeValidation.IsVisible = false;
                if (Clabe_Bancaria.Text.Length == 18)
                {
                    string bankCode = Clabe_Bancaria.Text.Substring(0, 3);
                    BankCodeEvaluate(bankCode);
                }
            }


        }

        private void BankCodeEvaluate(string bankCode)
        {
            switch (bankCode)
            {
                case "002":
                    setting.User.Banco = "Banco Nacional de México, S.A.";
                    setting.Banco = "Banco Nacional de México, S.A.";
                    break;
                case "003":
                    setting.User.Banco = "Banca Serfín, S.A.";
                    setting.Banco = "Banca Serfín, S.A.";
                    break;
                case "006":
                    setting.User.Banco = "Banco Nacional de Comercio Exterior";
                    setting.Banco = "Banco Nacional de Comercio Exterior";
                    break;
                case "009":
                    setting.User.Banco = "Banco Nacional de Obras y Servicios Públicos";
                    setting.Banco = "Banco Nacional de Obras y Servicios Públicos";
                    break;
                case "012":
                    setting.User.Banco = "BBVA Bancomer, S.A.";
                    setting.Banco = "BBVA Bancomer, S.A.";
                    break;
                case "014":
                    setting.User.Banco = "Banco Santander, S.A.";
                    setting.Banco = "Banco Santander, S.A.";
                    break;
                case "019":
                    setting.User.Banco = "Banco Nacional del Ejército, Fuerza Aérea y Armada";
                    setting.Banco = "Banco Nacional del Ejército, Fuerza Aérea y Armada";
                    break;
                case "021":
                    setting.User.Banco = "HSBC México, S.A.";
                    setting.Banco = "HSBC México, S.A.";
                    break;
                case "022":
                    setting.User.Banco = "GE Money Bank, S.A.";
                    setting.Banco = "GE Money Bank, S.A.";
                    break;
                case "030":
                    setting.User.Banco = "Banco del Bajío, S.A.";
                    setting.Banco = "Banco del Bajío, S.A.";
                    break;
                case "032":
                    setting.User.Banco = "IXE Banco, S.A.";
                    setting.Banco = "IXE Banco, S.A.";
                    break;
                case "036":
                    setting.User.Banco = "Banco Inbursa, S.A.";
                    setting.Banco = "Banco Inbursa, S.A.";
                    break;
                case "037":
                    setting.User.Banco = "Banco Interacciones, S.A.";
                    setting.Banco = "Banco Interacciones, S.A.";
                    break;
                case "042":
                    setting.User.Banco = "Banca Mifel, S.A.";
                    setting.Banco = "Banca Mifel, S.A.";
                    break;
                case "044":
                    setting.User.Banco = "Scotiabank Inverlat, S.A.";
                    setting.Banco = "Scotiabank Inverlat, S.A.";
                    break;
                case "058":
                    setting.User.Banco = "Banco Regional de Monterrey, S.A.";
                    setting.Banco = "Banco Regional de Monterrey, S.A.";
                    break;
                case "059":
                    setting.User.Banco = "Banco Invex, S.A.";
                    setting.Banco = "Banco Invex, S.A.";
                    break;
                case "060":
                    setting.User.Banco = "Bansi, S.A.";
                    setting.Banco = "Bansi, S.A.";
                    break;
                case "062":
                    setting.User.Banco = "Banca Afirme, S.A.";
                    setting.Banco = "Banca Afirme, S.A.";
                    break;
                case "072":
                    setting.User.Banco = "Banco Mercantil del Norte, S.A.";
                    setting.Banco = "Banco Mercantil del Norte, S.A.";
                    break;
                case "102":
                    setting.User.Banco = "The Royal Bank of Scotland México, S.A.";
                    setting.Banco = "The Royal Bank of Scotland México, S.A.";
                    break;
                case "103":
                    setting.User.Banco = "American Express Bank (México), S.A.";
                    setting.Banco = "American Express Bank (México), S.A.";
                    break;
                case "106":
                    setting.User.Banco = "Bank of America México, S.A.";
                    setting.Banco = "Bank of America México, S.A.";
                    break;
                case "108":
                    setting.User.Banco = "Bank of Tokyo-Mitsubishi UFJ (México), S.A.";
                    setting.Banco = "Bank of Tokyo-Mitsubishi UFJ (México), S.A.";
                    break;
                case "110":
                    setting.User.Banco = "Banco J.P. Morgan, S.A.";
                    setting.Banco = "Banco J.P. Morgan, S.A.";
                    break;
                case "112":
                    setting.User.Banco = "Banco Monex, S.A.";
                    setting.Banco = "Banco Monex, S.A.";
                    break;
                case "113":
                    setting.User.Banco = "Banco Ve por Mas, S.A.";
                    setting.Banco = "Banco Ve por Mas, S.A.";
                    break;
                case "114":
                    setting.User.Banco = "Bank One México, S.A.";
                    setting.Banco = "Bank One México, S.A.";
                    break;
                case "116":
                    setting.User.Banco = "ING Bank (México), S.A.";
                    setting.Banco = "ING Bank (México), S.A.";
                    break;
                case "119":
                    setting.User.Banco = "HSBC";
                    setting.Banco = "HSBC";
                    break;
                case "124":
                    setting.User.Banco = "Deutsche Bank México, S.A.";
                    setting.Banco = "Deutsche Bank México, S.A.";
                    break;
                case "126":
                    setting.User.Banco = "Banco Credit Suisse (México), S.A.";
                    setting.Banco = "Banco Credit Suisse (México), S.A.";
                    break;
                case "127":
                    setting.User.Banco = "Banco Azteca, S.A.";
                    setting.Banco = "Banco Azteca, S.A.";
                    break;
                case "128":
                    setting.User.Banco = "Banco Autofin México, S.A.";
                    setting.Banco = "Banco Autofin México, S.A.";
                    break;
                case "129":
                    setting.User.Banco = "Barclays Bank México, S.A.";
                    setting.Banco = "Barclays Bank México, S.A.";
                    break;
                case "130":
                    setting.User.Banco = "Banco Compartamos, S.A.";
                    setting.Banco = "Banco Compartamos, S.A.";
                    break;
                case "131":
                    setting.User.Banco = "Banco Ahorro Famsa, S.A.";
                    setting.Banco = "Banco Ahorro Famsa, S.A.";
                    break;
                case "132":
                    setting.User.Banco = "Banco Multiva, S.A.";
                    setting.Banco = "Banco Multiva, S.A.";
                    break;
                case "133":
                    setting.User.Banco = "Banco Actinver, S.A.";
                    setting.Banco = "Banco Actinver, S.A.";
                    break;
                case "134":
                    setting.User.Banco = "Banco Wal Mart de México Adelante, S.A.";
                    setting.Banco = "Banco Wal Mart de México Adelante, S.A.";
                    break;
                case "135":
                    setting.User.Banco = "Nacional Financiera, S.N.C.";
                    setting.Banco = "Nacional Financiera, S.N.C.";
                    break;
                case "136":
                    setting.User.Banco = "Inter Banco, S.A.";
                    setting.Banco = "Inter Banco, S.A.";
                    break;
                case "137":
                    setting.User.Banco = "BanCoppel, S.A.";
                    setting.Banco = "BanCoppel, S.A.";
                    break;
                case "138":
                    setting.User.Banco = "ABC Capital, S.A. I.B.M.";
                    setting.Banco = "ABC Capital, S.A. I.B.M.";
                    break;
                case "139":
                    setting.User.Banco = "UBS Banco, S.A.";
                    setting.Banco = "UBS Banco, S.A.";
                    break;
                case "140":
                    setting.User.Banco = "Consubanco, S.A.";
                    setting.Banco = "Consubanco, S.A.";
                    break;
                case "141":
                    setting.User.Banco = "Volkswagen Bank S.A. Institución de Banca Múltiple";
                    setting.Banco = "Volkswagen Bank S.A. Institución de Banca Múltiple";
                    break;
                case "143":
                    setting.User.Banco = "Consultoría Internacional Banco, S.A.";
                    setting.Banco = "Consultoría Internacional Banco, S.A.";
                    break;
                case "145":
                    setting.User.Banco = "Banco BASE, S.A. de I.B.M.";
                    setting.Banco = "Banco BASE, S.A. de I.B.M.";
                    break;
                case "147":
                    setting.User.Banco = "Bankaool, S.A., Institución de Banca Múltiple";
                    setting.Banco = "Bankaool, S.A., Institución de Banca Múltiple";
                    break;
                case "148":
                    setting.User.Banco = "Banco PagaTodo S.A., Institución de Banca Múltiple";
                    setting.Banco = "Banco PagaTodo S.A., Institución de Banca Múltiple";
                    break;
                case "150":
                    setting.User.Banco = "Banco Inmobiliario Mexicano, S.A., Institución de Banca Múltiple";
                    setting.Banco = "Banco Inmobiliario Mexicano, S.A., Institución de Banca Múltiple";
                    break;
                case "155":
                    setting.User.Banco = "Industrial and Commercial Bank of China, S.A., Institución de Banca Múltiple";
                    setting.Banco = "Industrial and Commercial Bank of China, S.A., Institución de Banca Múltiple";
                    break;
                case "156":
                    setting.User.Banco = "Banco Sabadell, S.A. I.B.M.";
                    setting.Banco = "Banco Sabadell, S.A. I.B.M.";
                    break;
                case "161":
                    setting.User.Banco = "Bancrecer, S.A.";
                    setting.Banco = "Bancrecer, S.A.";
                    break;
                case "166":
                    setting.User.Banco = "Banco del Ahorro Nacional y Servicios Financieros, S.N.C.";
                    setting.Banco = "Banco del Ahorro Nacional y Servicios Financieros, S.N.C.";
                    break;
                case "168":
                    setting.User.Banco = "Sociedad Hipotecaria Federal, S.N.C.";
                    setting.Banco = "Sociedad Hipotecaria Federal, S.N.C.";
                    break;
                case "600":
                    setting.User.Banco = "Monex Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Monex Casa de Bolsa, S.A. de C.V.";
                    break;
                case "601":
                    setting.User.Banco = "GBM Grupo Bursátil Mexicano, S.A. de C.V.";
                    setting.Banco = "GBM Grupo Bursátil Mexicano, S.A. de C.V.";
                    break;
                case "602":
                    setting.User.Banco = "Masari Casa de Bolsa, S.A.";
                    setting.Banco = "Masari Casa de Bolsa, S.A.";
                    break;
                case "604":
                    setting.User.Banco = "Inversora Bursátil, S.A. de C.V.";
                    setting.Banco = "Inversora Bursátil, S.A. de C.V.";
                    break;
                case "605":
                    setting.User.Banco = "Valué, S.A. de C.V., Casa de Bolsa";
                    setting.Banco = "Valué, S.A. de C.V., Casa de Bolsa";
                    break;
                case "606":
                    setting.User.Banco = "Estructuradores del Mercado de Valores Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Estructuradores del Mercado de Valores Casa de Bolsa, S.A. de C.V.";
                    break;
                case "607":
                    setting.User.Banco = "Casa de Cambio Tiber, S.A. de C.V.";
                    setting.Banco = "Casa de Cambio Tiber, S.A. de C.V.";
                    break;
                case "608":
                    setting.User.Banco = "Vector Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Vector Casa de Bolsa, S.A. de C.V.";
                    break;
                case "610":
                    setting.User.Banco = "B y B Casa de Cambio, S.A. de C.V.";
                    setting.Banco = "B y B Casa de Cambio, S.A. de C.V.";
                    break;
                case "611":
                    setting.User.Banco = "Intercam Casa de Cambio, S.A. de C.V.";
                    setting.Banco = "Intercam Casa de Cambio, S.A. de C.V.";
                    break;
                case "613":
                    setting.User.Banco = "Multivalores Casa de Bolsa, S.A. de C.V. Multiva Gpo. Fin.";
                    setting.Banco = "Multivalores Casa de Bolsa, S.A. de C.V. Multiva Gpo. Fin.";
                    break;
                case "614":
                    setting.User.Banco = "Acciones y Valores Banamex, S.A. de C.V., Casa de Bolsa";
                    setting.Banco = "Acciones y Valores Banamex, S.A. de C.V., Casa de Bolsa";
                    break;
                case "615":
                    setting.User.Banco = "Merrill Lynch México, S.A. de C.V., Casa de Bolsa";
                    setting.Banco = "Merrill Lynch México, S.A. de C.V., Casa de Bolsa";
                    break;
                case "616":
                    setting.User.Banco = "Casa de Bolsa Finamex, S.A. de C.V.";
                    setting.Banco = "Casa de Bolsa Finamex, S.A. de C.V.";
                    break;
                case "617":
                    setting.User.Banco = "Valores Mexicanos Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Valores Mexicanos Casa de Bolsa, S.A. de C.V.";
                    break;
                case "618":
                    setting.User.Banco = "Única Casa de Cambio, S.A. de C.V.";
                    setting.Banco = "Única Casa de Cambio, S.A. de C.V.";
                    break;
                case "619":
                    setting.User.Banco = "MAPFRE Tepeyac S.A.";
                    setting.Banco = "MAPFRE Tepeyac S.A.";
                    break;
                case "620":
                    setting.User.Banco = "Profuturo G.N.P., S.A. de C.V.";
                    setting.Banco = "Profuturo G.N.P., S.A. de C.V.";
                    break;
                case "621":
                    setting.User.Banco = "Actinver Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Actinver Casa de Bolsa, S.A. de C.V.";
                    break;
                case "622":
                    setting.User.Banco = "OPERADORA ACTINVER, S.A. DE C.V.";
                    setting.Banco = "OPERADORA ACTINVER, S.A. DE C.V.";
                    break;
                case "623":
                    setting.User.Banco = "Skandia Vida S.A. de C.V.";
                    setting.Banco = "Skandia Vida S.A. de C.V.";
                    break;
                case "624":
                    setting.User.Banco = "Consultoría Internacional Casa de Cambio, S.A. de C.V.";
                    setting.Banco = "Consultoría Internacional Casa de Cambio, S.A. de C.V.";
                    break;
                case "626":
                    setting.User.Banco = "Deutsche Securities, S.A. de C.V.";
                    setting.Banco = "Deutsche Securities, S.A. de C.V.";
                    break;
                case "627":
                    setting.User.Banco = "Zúrich Compañía de Seguros, S.A.";
                    setting.Banco = "Zúrich Compañía de Seguros, S.A.";
                    break;
                case "628":
                    setting.User.Banco = "Zúrich Vida, Compañía de Seguros, S.A.";
                    setting.Banco = "Zúrich Vida, Compañía de Seguros, S.A.";
                    break;
                case "629":
                    setting.User.Banco = "Hipotecaria su Casita, S.A. de C.V.";
                    setting.Banco = "Hipotecaria su Casita, S.A. de C.V.";
                    break;
                case "630":
                    setting.User.Banco = "Intercam Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Intercam Casa de Bolsa, S.A. de C.V.";
                    break;
                case "631":
                    setting.User.Banco = "CI Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "CI Casa de Bolsa, S.A. de C.V.";
                    break;
                case "632":
                    setting.User.Banco = "Bulltick Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Bulltick Casa de Bolsa, S.A. de C.V.";
                    break;
                case "633":
                    setting.User.Banco = "Sterling Casa de Cambio, S.A. de C.V.";
                    setting.Banco = "Sterling Casa de Cambio, S.A. de C.V.";
                    break;
                case "634":
                    setting.User.Banco = "Fincomún, Servicios Financieros Comunitarios, S.A. de C.V.";
                    setting.Banco = "Fincomún, Servicios Financieros Comunitarios, S.A. de C.V.";
                    break;
                case "636":
                    setting.User.Banco = "HDI Seguros, S.A. de C.V.";
                    setting.Banco = "HDI Seguros, S.A. de C.V.";
                    break;
                case "637":
                    setting.User.Banco = "OrderExpress Casa de Cambio , S.A. de C.V. AAC";
                    setting.Banco = "OrderExpress Casa de Cambio , S.A. de C.V. AAC";
                    break;
                case "638":
                    setting.User.Banco = "Akala, S.A. de C.V., Sociedad Financiera Popular";
                    setting.Banco = "Akala, S.A. de C.V., Sociedad Financiera Popular";
                    break;
                case "640":
                    setting.User.Banco = "J.P. Morgan Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "J.P. Morgan Casa de Bolsa, S.A. de C.V.";
                    break;
                case "642":
                    setting.User.Banco = "Operadora de Recursos Reforma, S.A. de C.V.";
                    setting.Banco = "Operadora de Recursos Reforma, S.A. de C.V.";
                    break;
                case "646":
                    setting.User.Banco = "Sistema de Transferencias y Pagos STP, S.A. de C.V., SOFOM E.N.R.";
                    setting.Banco = "Sistema de Transferencias y Pagos STP, S.A. de C.V., SOFOM E.N.R.";
                    break;
                case "647":
                    setting.User.Banco = "Telecomunicaciones de México";
                    setting.Banco = "Telecomunicaciones de México";
                    break;
                case "648":
                    setting.User.Banco = "Evercore Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Evercore Casa de Bolsa, S.A. de C.V.";
                    break;
                case "649":
                    setting.User.Banco = "Skandia Operadora S.A. de C.V.";
                    setting.Banco = "Skandia Operadora S.A. de C.V.";
                    break;
                case "651":
                    setting.User.Banco = "Seguros Monterrey New York Life, S.A de C.V.";
                    setting.Banco = "Seguros Monterrey New York Life, S.A de C.V.";
                    break;
                case "652":
                    setting.User.Banco = "Solución Asea, S.A. de C.V., Sociedad Financiera Popular";
                    setting.Banco = "Solución Asea, S.A. de C.V., Sociedad Financiera Popular";
                    break;
                case "653":
                    setting.User.Banco = "Kuspit Casa de Bolsa, S.A. de C.V.";
                    setting.Banco = "Kuspit Casa de Bolsa, S.A. de C.V.";
                    break;
                case "655":
                    setting.User.Banco = "J.P. SOFIEXPRESS, S.A. de C.V., S.F.P.";
                    setting.Banco = "J.P. SOFIEXPRESS, S.A. de C.V., S.F.P.";
                    break;
                case "656":
                    setting.User.Banco = "UNAGRA, S.A. de C.V., S.F.P.";
                    setting.Banco = "UNAGRA, S.A. de C.V., S.F.P.";
                    break;
                case "659":
                    setting.User.Banco = "Opciones Empresariales Del Noreste, S.A. DE C.V.";
                    setting.Banco = "Opciones Empresariales Del Noreste, S.A. DE C.V.";
                    break;
                case "670":
                    setting.User.Banco = "Libertad Servicios Financieros, S.A. De C.V.";
                    setting.Banco = "Libertad Servicios Financieros, S.A. De C.V.";
                    break;
                case "674":
                    setting.User.Banco = "AXA Seguros, S.A. De C.V.";
                    setting.Banco = "AXA Seguros, S.A. De C.V.";
                    break;
                case "677":
                    setting.User.Banco = "Caja Popular Mexicana, S.C. de A.P. de R.L. De C.V.";
                    setting.Banco = "Caja Popular Mexicana, S.C. de A.P. de R.L. De C.V.";
                    break;
                case "679":
                    setting.User.Banco = "Financiera Nacional De Desarrollo Agropecuario, Rural, F y P.";
                    setting.Banco = "Financiera Nacional De Desarrollo Agropecuario, Rural, F y P.";
                    break;
                case "684":
                    setting.User.Banco = "Operadora De Pagos Móviles De México, S.A. De C.V.";
                    setting.Banco = "Operadora De Pagos Móviles De México, S.A. De C.V.";
                    break;
                case "901":
                    setting.User.Banco = "CLS Bank International";
                    setting.Banco = "CLS Bank International";
                    break;
                case "902":
                    setting.User.Banco = "SD. INDEVAL, S.A. de C.V.";
                    setting.Banco = "SD. INDEVAL, S.A. de C.V.";
                    break;
                case "999":
                    setting.User.Banco = "";
                    setting.Banco = "";
                    break;
                default:
                    setting.User.Banco = "";
                    setting.Banco = "";
                    break;
            }
        }

        private async Task BuscarPerfil()
        {
            await setting.Buscar();
        }

        //public void SetGestureRecognizers()
        //{
        //    avatar.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
        //    {
        //        var action = await DisplayActionSheet("Selecciona una Foto", "Cancelar", null, "Cámara", "Galería");
        //        if (action == "Cámara")
        //        {
        //            Device.BeginInvokeOnMainThread(() =>
        //            {
        //                var fileName = SetImageFileName();
        //                DependencyService.Get<CameraInterface>().LaunchCamera(FileFormatEnum.JPEG, fileName);
        //            });
        //        }
        //        if (action == "Galería")
        //        {
        //            Device.BeginInvokeOnMainThread(() =>
        //            {
        //                var fileName = SetImageFileName();
        //                DependencyService.Get<CameraInterface>().LaunchGallery(FileFormatEnum.JPEG, fileName,1);
        //            });
        //        }
        //    }));
        //}

        //private string SetImageFileName(string fileName = null)
        //{
        //    if (Device.RuntimePlatform == Device.Android)
        //    {
        //        if (fileName != null)
        //            App.ImageIdToSave = fileName;
        //        else
        //            App.ImageIdToSave = App.DefaultImageId;

        //        return App.ImageIdToSave;
        //    }
        //    else
        //    {
        //        if (fileName != null)
        //        {
        //            App.ImageIdToSave = fileName;
        //            return fileName;
        //        }
        //        else
        //            return null;
        //    }
        //}
    }
    //private void RoundedButton_OnClicked(object sender, EventArgs e)
    //{
    //    // Cambiar contraseña.
    //    if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ChangePasswordPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ChangePasswordPage)))
    //        Navigation.PushModalAsync(NavigationPageHelper.Create(new ChangePasswordPage()));
    //}

}

