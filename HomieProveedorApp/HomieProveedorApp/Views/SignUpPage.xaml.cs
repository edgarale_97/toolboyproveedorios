﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignUpPage : ContentPage
	{
	    private bool Lunes;
	    private bool Martes;
	    private bool Miercoles;
	    private bool Jueves;
	    private bool Viernes;
	    private bool Sabado;
	    private bool Domingo;
	    private SignUpViewModel _vm;

        private TermsConditionsPage termsConditions;
		public SignUpPage ()
		{
			InitializeComponent ();
            _vm = new SignUpViewModel();
		    BindingContext = _vm;
            SetGestureRecognizers();
		}


	    public void SetGestureRecognizers()
	    {
            avatar.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    _vm.TakePicture(0);
                }
                if (action == "Galería")
                {
                    _vm.PickPicture(0);
                }
            }));
	        GLunes.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
	        {
	            if (!Lunes)
	            {
	                LbLunes.FadeTo(0, 300);
	                await BgLunes.FadeTo(0, 300);
	                BgLunes.BadgeBackgroundColor = Color.FromHex("#00a99d");
	                LbLunes.TextColor = Color.White;

	                LbLunes.FadeTo(1, 300);
	                await BgLunes.FadeTo(1, 300);
	                _vm.User.LunesDisponible = true;
	                Lunes = true;
	            }
	            else
	            {
	                LbLunes.FadeTo(0, 300);
	                await BgLunes.FadeTo(0, 300);
	                BgLunes.BadgeBackgroundColor = Color.White;
	                LbLunes.TextColor = Color.Black;

	                LbLunes.FadeTo(1, 300);
	                await BgLunes.FadeTo(1, 300);
	                _vm.User.LunesDisponible = false;
	                Lunes = false;
	            }
            }));
            GMartes.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                if (!Martes)
                {
                    LbMartes.FadeTo(0, 300);
                    await BgMartes.FadeTo(0, 300);
                    BgMartes.BadgeBackgroundColor = Color.FromHex("#00a99d");
                    LbMartes.TextColor = Color.White;

                    LbMartes.FadeTo(1, 300);
                    await BgMartes.FadeTo(1, 300);
                    _vm.User.MartesDisponible = true;
                    Martes = true;
                }
                else
                {
                    LbMartes.FadeTo(0, 300);
                    await BgMartes.FadeTo(0, 300);
                    BgMartes.BadgeBackgroundColor = Color.White;
                    LbMartes.TextColor = Color.Black;

                    LbMartes.FadeTo(1, 300);
                    await BgMartes.FadeTo(1, 300);
                    _vm.User.MartesDisponible = false;
                    Martes = false;
                }
            }));
            GMiercoles.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                if (!Miercoles)
                {
                    LbMiercoles.FadeTo(0, 300);
                    await BgMiercoles.FadeTo(0, 300);
                    BgMiercoles.BadgeBackgroundColor = Color.FromHex("#00a99d");
                    LbMiercoles.TextColor = Color.White;

                    LbMiercoles.FadeTo(1, 300);
                    await BgMiercoles.FadeTo(1, 300);
                    _vm.User.MiercolesDisponible = true;
                    Miercoles = true;
                }
                else
                {
                    LbMiercoles.FadeTo(0, 300);
                    await BgMiercoles.FadeTo(0, 300);
                    BgMiercoles.BadgeBackgroundColor = Color.White;
                    LbMiercoles.TextColor = Color.Black;

                    LbMiercoles.FadeTo(1, 300);
                    await BgMiercoles.FadeTo(1, 300);
                    _vm.User.MiercolesDisponible = false;
                    Miercoles = false;
                }
            }));
            GJueves.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                if (!Jueves)
                {
                    LbJueves.FadeTo(0, 300);
                    await BgJueves.FadeTo(0, 300);
                    BgJueves.BadgeBackgroundColor = Color.FromHex("#00a99d");
                    LbJueves.TextColor = Color.White;

                    LbJueves.FadeTo(1, 300);
                    await BgJueves.FadeTo(1, 300);
                    _vm.User.JuevesDisponible = true;
                    Jueves = true;
                }
                else
                {
                    LbJueves.FadeTo(0, 300);
                    await BgJueves.FadeTo(0, 300);
                    BgJueves.BadgeBackgroundColor = Color.White;
                    LbJueves.TextColor = Color.Black;

                    LbJueves.FadeTo(1, 300);
                    await BgJueves.FadeTo(1, 300);
                    _vm.User.JuevesDisponible = false;
                    Jueves = false;
                }
            }));
            GViernes.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                if (!Viernes)
                {
                    LbViernes.FadeTo(0, 300);
                    await BgViernes.FadeTo(0, 300);
                    BgViernes.BadgeBackgroundColor = Color.FromHex("#00a99d");
                    LbViernes.TextColor = Color.White;

                    LbViernes.FadeTo(1, 300);
                    await BgViernes.FadeTo(1, 300);
                    _vm.User.ViernesDisponible = true;
                    Viernes = true;
                }
                else
                {
                    LbViernes.FadeTo(0, 300);
                    await BgViernes.FadeTo(0, 300);
                    BgViernes.BadgeBackgroundColor = Color.White;
                    LbViernes.TextColor = Color.Black;

                    LbViernes.FadeTo(1, 300);
                    await BgViernes.FadeTo(1, 300);
                    _vm.User.ViernesDisponible = false;
                    Viernes = false;
                }
            }));
            GSabado.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                if (!Sabado)
                {
                    LbSabado.FadeTo(0, 300);
                    await BgSabado.FadeTo(0, 300);
                    BgSabado.BadgeBackgroundColor = Color.FromHex("#00a99d");
                    LbSabado.TextColor = Color.White;

                    LbSabado.FadeTo(1, 300);
                    await BgSabado.FadeTo(1, 300);
                    _vm.User.SabadoDisponible = true;
                    Sabado = true;
                }
                else
                {
                    LbSabado.FadeTo(0, 300);
                    await BgSabado.FadeTo(0, 300);
                    BgSabado.BadgeBackgroundColor = Color.White;
                    LbSabado.TextColor = Color.Black;

                    LbSabado.FadeTo(1, 300);
                    await BgSabado.FadeTo(1, 300);
                    _vm.User.SabadoDisponible = false;
                    Sabado = false;
                }
            }));
            GDomingo.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                if (!Domingo)
                {
                    LbDomingo.FadeTo(0, 300);
                    await BgDomingo.FadeTo(0, 300);
                    BgDomingo.BadgeBackgroundColor = Color.FromHex("#00a99d");
                    LbDomingo.TextColor = Color.White;

                    LbDomingo.FadeTo(1, 300);
                    await BgDomingo.FadeTo(1, 300);
                    _vm.User.DomingoDisponible = true;
                    Domingo = true;
                }
                else
                {
                    LbDomingo.FadeTo(0, 300);
                    await BgDomingo.FadeTo(0, 300);
                    BgDomingo.BadgeBackgroundColor = Color.White;
                    LbDomingo.TextColor = Color.Black;

                    LbDomingo.FadeTo(1, 300);
                    await BgDomingo.FadeTo(1, 300);
                    _vm.User.DomingoDisponible = false;
                    Domingo = false;
                }
            }));
            LayoutPhoto1.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    _vm.TakePicture(1);
                }
                if (action == "Galería")
                {
                    _vm.PickPicture(1);
                }
            }));
            LayoutPhoto2.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    _vm.TakePicture(2);
                }
                if (action == "Galería")
                {
                    _vm.PickPicture(2);
                }
            }));
            LayoutPhoto3.GestureRecognizers.Add(new TapGestureRecognizer(async (view) =>
            {
                var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    _vm.TakePicture(3);
                }
                if (action == "Galería")
                {
                    _vm.PickPicture(3);
                }
            }));

	        if (Device.RuntimePlatform == Device.iOS)
	        {
	            this.Padding = new Thickness(0, 30, 0, 0);
	        }
        }

	    private async void BtnAddImages_OnClicked(object sender, EventArgs e)
	    {
	        var action = await DisplayActionSheet("Seleccionar Foto", "Cancelar", null, "Cámara", "Galería");
	        if (action == "Cámara")
	        {
	            _vm.TakePicture(1);
	        }
	        if (action == "Galería")
	        {
	            _vm.PickPicture(1);
	        }
	    }

        private async void BtnDeleteImage1_OnClicked(object sender, EventArgs e)
        {
            _vm.TrabajoFoto1 = null;
            _vm.User.FotoTrabajo1 = null;
            _vm.IsPhoto1Picked = false;
        }

        private async void BtnDeleteImage2_OnClicked(object sender, EventArgs e)
        {
            _vm.TrabajoFoto2 = null;
            _vm.User.FotoTrabajo2 = null;
            _vm.IsPhoto2Picked = false;
        }

        private async void BtnDeleteImage3_OnClicked(object sender, EventArgs e)
        {
            _vm.TrabajoFoto3 = null;
            _vm.User.FotoTrabajo3 = null;
            _vm.IsPhoto3Picked = false;
        }

        //private void PickerBanco_OnSelectedIndexChanged(object sender, EventArgs e)
	    //{
	    //    _vm.User.Banco = PickerBanco.Items[PickerBanco.SelectedIndex];
	    //}

        private void Page_Disappearing(object sender, EventArgs e)
        {
            /*CheckBoxTerminos.Toggled -= CheckBoxTerminos_Toggled;

	        CheckBoxTerminos.IsToggled = _terminosCondiciones.Aceptado;
	        _vm.AceptoTerminos = _terminosCondiciones.Aceptado;

	        CheckBoxTerminos.Toggled += CheckBoxTerminos_Toggled; */
        }

        private async void Terms_PageAsync(object sender, EventArgs e)
        {
            termsConditions = new TermsConditionsPage();
            await Navigation.PushModalAsync(termsConditions);
            termsConditions.Disappearing += Page_Disappearing;
        }
        
        void Nombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(Nombre.Text.Length > 50)
            {
                Nombre.Text = Nombre.Text.Remove(Nombre.Text.Length - 1);
            }
        }

        void ApellidoPaterno_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Apellido_Paterno.Text.Length > 20)
            {
                Apellido_Paterno.Text = Apellido_Paterno.Text.Remove(Apellido_Paterno.Text.Length - 1);
            }
        }

        void ApellidoMaterno_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Apellido_Materno.Text.Length > 20)
            {
                Apellido_Materno.Text = Apellido_Materno.Text.Remove(Apellido_Materno.Text.Length - 1);
            }
        }

        void Email_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Email.Text.Length > 100)
            {
                Email.Text = Email.Text.Remove(Email.Text.Length - 1);
            }
        }

        private void Descripcion_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Descripcion.Text.Length > 1000)
            {
                Descripcion.Text = Descripcion.Text.Remove(Descripcion.Text.Length - 1);
            }
        }

        private void Nombre_Unfocused(object sender, FocusEventArgs e)
        {
            NombreValidationMethod();
        }

        private void Apellido_Paterno_Unfocused(object sender, FocusEventArgs e)
        {
            ApellidoValidationMethod();
        }

        private void Email_Unfocused(object sender, FocusEventArgs e)
        {
            EmailValidationMethod();
        }

        private void PhoneNumber_Unfocused(object sender, FocusEventArgs e)
        {
            PhoneNumberValidationMethod();
        }

        private void Clabe_Unfocused(object sender, FocusEventArgs e)
        {
            ClabeValidationMethod();
        }

        private void NombreValidationMethod()
        {
            if (string.IsNullOrEmpty(Nombre.Text))
            {
                Nombre.PlaceholderColor = Color.Red;
                NombreValidation.IsVisible = true;
            }
            else
            {
                Nombre.PlaceholderColor = Color.Default;
                NombreValidation.IsVisible = false;
            }
        }

        private void ApellidoValidationMethod()
        {
            if (string.IsNullOrEmpty(Apellido_Paterno.Text))
            {
                Apellido_Paterno.PlaceholderColor = Color.Red;
                ApellidoValidation.IsVisible = true;
            }
            else
            {
                Apellido_Paterno.PlaceholderColor = Color.Default;
                ApellidoValidation.IsVisible = false;
            }
        }

        private void EmailValidationMethod()
        {
            if (string.IsNullOrEmpty(Email.Text))
            {
                Email.PlaceholderColor = Color.Red;
                EmailValidation.IsVisible = true;
            }
            else
            {
                Email.PlaceholderColor = Color.Default;
                EmailValidation.IsVisible = false;
            }
        }

        private void PhoneNumberValidationMethod()
        {
            if (string.IsNullOrEmpty(PhoneNumber.Text))
            {
                PhoneNumber.PlaceholderColor = Color.Red;
                PhoneNumberValidation.IsVisible = true;
            }
            else
            {
                PhoneNumber.PlaceholderColor = Color.Default;
                PhoneNumberValidation.IsVisible = false;
            }
        }

        private void ClabeValidationMethod()
        {
            if (string.IsNullOrEmpty(Clabe_Bancaria.Text))
            {
                Clabe_Bancaria.PlaceholderColor = Color.Red;
                ClabeValidation.IsVisible = true;
                Banco_Nombre.Text = "";
            }
            else
            {
                Clabe_Bancaria.PlaceholderColor = Color.Default;
                ClabeValidation.IsVisible = false;
                if (Clabe_Bancaria.Text.Length >= 16)
                {
                    string bankCode = Clabe_Bancaria.Text.Substring(0, 3);
                    BankCodeEvaluate(bankCode);
                }
            }


        }

        private void BankCodeEvaluate(string bankCode)
        {
            switch (bankCode)
            {
                case "002":
                    Banco_Nombre.Text = "Banco Nacional de México, S.A.";
                    _vm.User.Banco = "Banco Nacional de México, S.A.";
                    break;
                case "003":
                    Banco_Nombre.Text = "Banca Serfín, S.A.";
                    _vm.User.Banco = "Banca Serfín, S.A.";
                    break;
                case "006":
                    Banco_Nombre.Text = "Banco Nacional de Comercio Exterior";
                    _vm.User.Banco = "Banco Nacional de Comercio Exterior";
                    break;
                case "009":
                    Banco_Nombre.Text = "Banco Nacional de Obras y Servicios Públicos";
                    _vm.User.Banco = "Banco Nacional de Obras y Servicios Públicos";
                    break;
                case "012":
                    Banco_Nombre.Text = "BBVA Bancomer, S.A.";
                    _vm.User.Banco = "BBVA Bancomer, S.A.";
                    break;
                case "014":
                    Banco_Nombre.Text = "Banco Santander, S.A.";
                    _vm.User.Banco = "Banco Santander, S.A.";
                    break;
                case "019":
                    Banco_Nombre.Text = "Banco Nacional del Ejército, Fuerza Aérea y Armada";
                    _vm.User.Banco = "Banco Nacional del Ejército, Fuerza Aérea y Armada";
                    break;
                case "021":
                    Banco_Nombre.Text = "HSBC México, S.A.";
                    _vm.User.Banco = "HSBC México, S.A.";
                    break;
                case "022":
                    Banco_Nombre.Text = "GE Money Bank, S.A.";
                    _vm.User.Banco = "GE Money Bank, S.A.";
                    break;
                case "030":
                    Banco_Nombre.Text = "Banco del Bajío, S.A.";
                    _vm.User.Banco = "Banco del Bajío, S.A.";
                    break;
                case "032":
                    Banco_Nombre.Text = "IXE Banco, S.A.";
                    _vm.User.Banco = "IXE Banco, S.A.";
                    break;
                case "036":
                    Banco_Nombre.Text = "Banco Inbursa, S.A.";
                    _vm.User.Banco = "Banco Inbursa, S.A.";
                    break;
                case "037":
                    Banco_Nombre.Text = "Banco Interacciones, S.A.";
                    _vm.User.Banco = "Banco Interacciones, S.A.";
                    break;
                case "042":
                    Banco_Nombre.Text = "Banca Mifel, S.A.";
                    _vm.User.Banco = "Banca Mifel, S.A.";
                    break;
                case "044":
                    Banco_Nombre.Text = "Scotiabank Inverlat, S.A.";
                    _vm.User.Banco = "Scotiabank Inverlat, S.A.";
                    break;
                case "058":
                    Banco_Nombre.Text = "Banco Regional de Monterrey, S.A.";
                    _vm.User.Banco = "Banco Regional de Monterrey, S.A.";
                    break;
                case "059":
                    Banco_Nombre.Text = "Banco Invex, S.A.";
                    _vm.User.Banco = "Banco Invex, S.A.";
                    break;
                case "060":
                    Banco_Nombre.Text = "Bansi, S.A.";
                    _vm.User.Banco = "Bansi, S.A.";
                    break;
                case "062":
                    Banco_Nombre.Text = "Banca Afirme, S.A.";
                    _vm.User.Banco = "Banca Afirme, S.A.";
                    break;
                case "072":
                    Banco_Nombre.Text = "Banco Mercantil del Norte, S.A.";
                    _vm.User.Banco = "Banco Mercantil del Norte, S.A.";
                    break;
                case "102":
                    Banco_Nombre.Text = "The Royal Bank of Scotland México, S.A.";
                    _vm.User.Banco = "The Royal Bank of Scotland México, S.A.";
                    break;
                case "103":
                    Banco_Nombre.Text = "American Express Bank (México), S.A.";
                    _vm.User.Banco = "American Express Bank (México), S.A.";
                    break;
                case "106":
                    Banco_Nombre.Text = "Bank of America México, S.A.";
                    _vm.User.Banco = "Bank of America México, S.A.";
                    break;
                case "108":
                    Banco_Nombre.Text = "Bank of Tokyo-Mitsubishi UFJ (México), S.A.";
                    _vm.User.Banco = "Bank of Tokyo-Mitsubishi UFJ (México), S.A.";
                    break;
                case "110":
                    Banco_Nombre.Text = "Banco J.P. Morgan, S.A.";
                    _vm.User.Banco = "Banco J.P. Morgan, S.A.";
                    break;
                case "112":
                    Banco_Nombre.Text = "Banco Monex, S.A.";
                    _vm.User.Banco = "Banco Monex, S.A.";
                    break;
                case "113":
                    Banco_Nombre.Text = "Banco Ve por Mas, S.A.";
                    _vm.User.Banco = "Banco Ve por Mas, S.A.";
                    break;
                case "114":
                    Banco_Nombre.Text = "Bank One México, S.A.";
                    _vm.User.Banco = "Bank One México, S.A.";
                    break;
                case "116":
                    Banco_Nombre.Text = "ING Bank (México), S.A.";
                    _vm.User.Banco = "ING Bank (México), S.A.";
                    break;
                case "119":
                    Banco_Nombre.Text = "HSBC";
                    _vm.User.Banco = "HSBC";
                    break;
                case "124":
                    Banco_Nombre.Text = "Deutsche Bank México, S.A.";
                    _vm.User.Banco = "Deutsche Bank México, S.A.";
                    break;
                case "126":
                    Banco_Nombre.Text = "Banco Credit Suisse (México), S.A.";
                    _vm.User.Banco = "Banco Credit Suisse (México), S.A.";
                    break;
                case "127":
                    Banco_Nombre.Text = "Banco Azteca, S.A.";
                    _vm.User.Banco = "Banco Azteca, S.A.";
                    break;
                case "128":
                    Banco_Nombre.Text = "Banco Autofin México, S.A.";
                    _vm.User.Banco = "Banco Autofin México, S.A.";
                    break;
                case "129":
                    Banco_Nombre.Text = "Barclays Bank México, S.A.";
                    _vm.User.Banco = "Barclays Bank México, S.A.";
                    break;
                case "130":
                    Banco_Nombre.Text = "Banco Compartamos, S.A.";
                    _vm.User.Banco = "Banco Compartamos, S.A.";
                    break;
                case "131":
                    Banco_Nombre.Text = "Banco Ahorro Famsa, S.A.";
                    _vm.User.Banco = "Banco Ahorro Famsa, S.A.";
                    break;
                case "132":
                    Banco_Nombre.Text = "Banco Multiva, S.A.";
                    _vm.User.Banco = "Banco Multiva, S.A.";
                    break;
                case "133":
                    Banco_Nombre.Text = "Banco Actinver, S.A.";
                    _vm.User.Banco = "Banco Actinver, S.A.";
                    break;
                case "134":
                    Banco_Nombre.Text = "Banco Wal Mart de México Adelante, S.A.";
                    _vm.User.Banco = "Banco Wal Mart de México Adelante, S.A.";
                    break;
                case "135":
                    Banco_Nombre.Text = "Nacional Financiera, S.N.C.";
                    _vm.User.Banco = "Nacional Financiera, S.N.C.";
                    break;
                case "136":
                    Banco_Nombre.Text = "Inter Banco, S.A.";
                    _vm.User.Banco = "Inter Banco, S.A.";
                    break;
                case "137":
                    Banco_Nombre.Text = "BanCoppel, S.A.";
                    _vm.User.Banco = "BanCoppel, S.A.";
                    break;
                case "138":
                    Banco_Nombre.Text = "ABC Capital, S.A. I.B.M.";
                    _vm.User.Banco = "ABC Capital, S.A. I.B.M.";
                    break;
                case "139":
                    Banco_Nombre.Text = "UBS Banco, S.A.";
                    _vm.User.Banco = "UBS Banco, S.A.";
                    break;
                case "140":
                    Banco_Nombre.Text = "Consubanco, S.A.";
                    _vm.User.Banco = "Consubanco, S.A.";
                    break;
                case "141":
                    Banco_Nombre.Text = "Volkswagen Bank S.A. Institución de Banca Múltiple";
                    _vm.User.Banco = "Volkswagen Bank S.A. Institución de Banca Múltiple";
                    break;
                case "143":
                    Banco_Nombre.Text = "Consultoría Internacional Banco, S.A.";
                    _vm.User.Banco = "Consultoría Internacional Banco, S.A.";
                    break;
                case "145":
                    Banco_Nombre.Text = "Banco BASE, S.A. de I.B.M.";
                    _vm.User.Banco = "Banco BASE, S.A. de I.B.M.";
                    break;
                case "147":
                    Banco_Nombre.Text = "Bankaool, S.A., Institución de Banca Múltiple";
                    _vm.User.Banco = "Bankaool, S.A., Institución de Banca Múltiple";
                    break;
                case "148":
                    Banco_Nombre.Text = "Banco PagaTodo S.A., Institución de Banca Múltiple";
                    _vm.User.Banco = "Banco PagaTodo S.A., Institución de Banca Múltiple";
                    break;
                case "150":
                    Banco_Nombre.Text = "Banco Inmobiliario Mexicano, S.A., Institución de Banca Múltiple";
                    _vm.User.Banco = "Banco Inmobiliario Mexicano, S.A., Institución de Banca Múltiple";
                    break;
                case "155":
                    Banco_Nombre.Text = "Industrial and Commercial Bank of China, S.A., Institución de Banca Múltiple";
                    _vm.User.Banco = "Industrial and Commercial Bank of China, S.A., Institución de Banca Múltiple";
                    break;
                case "156":
                    Banco_Nombre.Text = "Banco Sabadell, S.A. I.B.M.";
                    _vm.User.Banco = "Banco Sabadell, S.A. I.B.M.";
                    break;
                case "161":
                    Banco_Nombre.Text = "Bancrecer, S.A.";
                    _vm.User.Banco = "Bancrecer, S.A.";
                    break;
                case "166":
                    Banco_Nombre.Text = "Banco del Ahorro Nacional y Servicios Financieros, S.N.C.";
                    _vm.User.Banco = "Banco del Ahorro Nacional y Servicios Financieros, S.N.C.";
                    break;
                case "168":
                    Banco_Nombre.Text = "Sociedad Hipotecaria Federal, S.N.C.";
                    _vm.User.Banco = "Sociedad Hipotecaria Federal, S.N.C.";
                    break;
                case "600":
                    Banco_Nombre.Text = "Monex Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Monex Casa de Bolsa, S.A. de C.V.";
                    break;
                case "601":
                    Banco_Nombre.Text = "GBM Grupo Bursátil Mexicano, S.A. de C.V.";
                    _vm.User.Banco = "GBM Grupo Bursátil Mexicano, S.A. de C.V.";
                    break;
                case "602":
                    Banco_Nombre.Text = "Masari Casa de Bolsa, S.A.";
                    _vm.User.Banco = "Masari Casa de Bolsa, S.A.";
                    break;
                case "604":
                    Banco_Nombre.Text = "Inversora Bursátil, S.A. de C.V.";
                    _vm.User.Banco = "Inversora Bursátil, S.A. de C.V.";
                    break;
                case "605":
                    Banco_Nombre.Text = "Valué, S.A. de C.V., Casa de Bolsa";
                    _vm.User.Banco = "Valué, S.A. de C.V., Casa de Bolsa";
                    break;
                case "606":
                    Banco_Nombre.Text = "Estructuradores del Mercado de Valores Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Estructuradores del Mercado de Valores Casa de Bolsa, S.A. de C.V.";
                    break;
                case "607":
                    Banco_Nombre.Text = "Casa de Cambio Tiber, S.A. de C.V.";
                    _vm.User.Banco = "Casa de Cambio Tiber, S.A. de C.V.";
                    break;
                case "608":
                    Banco_Nombre.Text = "Vector Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Vector Casa de Bolsa, S.A. de C.V.";
                    break;
                case "610":
                    Banco_Nombre.Text = "B y B Casa de Cambio, S.A. de C.V.";
                    _vm.User.Banco = "B y B Casa de Cambio, S.A. de C.V.";
                    break;
                case "611":
                    Banco_Nombre.Text = "Intercam Casa de Cambio, S.A. de C.V.";
                    _vm.User.Banco = "Intercam Casa de Cambio, S.A. de C.V.";
                    break;
                case "613":
                    Banco_Nombre.Text = "Multivalores Casa de Bolsa, S.A. de C.V. Multiva Gpo. Fin.";
                    _vm.User.Banco = "Multivalores Casa de Bolsa, S.A. de C.V. Multiva Gpo. Fin.";
                    break;
                case "614":
                    Banco_Nombre.Text = "Acciones y Valores Banamex, S.A. de C.V., Casa de Bolsa";
                    _vm.User.Banco = "Acciones y Valores Banamex, S.A. de C.V., Casa de Bolsa";
                    break;
                case "615":
                    Banco_Nombre.Text = "Merrill Lynch México, S.A. de C.V., Casa de Bolsa";
                    _vm.User.Banco = "Merrill Lynch México, S.A. de C.V., Casa de Bolsa";
                    break;
                case "616":
                    Banco_Nombre.Text = "Casa de Bolsa Finamex, S.A. de C.V.";
                    _vm.User.Banco = "Casa de Bolsa Finamex, S.A. de C.V.";
                    break;
                case "617":
                    Banco_Nombre.Text = "Valores Mexicanos Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Valores Mexicanos Casa de Bolsa, S.A. de C.V.";
                    break;
                case "618":
                    Banco_Nombre.Text = "Única Casa de Cambio, S.A. de C.V.";
                    _vm.User.Banco = "Única Casa de Cambio, S.A. de C.V.";
                    break;
                case "619":
                    Banco_Nombre.Text = "MAPFRE Tepeyac S.A.";
                    _vm.User.Banco = "MAPFRE Tepeyac S.A.";
                    break;
                case "620":
                    Banco_Nombre.Text = "Profuturo G.N.P., S.A. de C.V.";
                    _vm.User.Banco = "Profuturo G.N.P., S.A. de C.V.";
                    break;
                case "621":
                    Banco_Nombre.Text = "Actinver Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Actinver Casa de Bolsa, S.A. de C.V.";
                    break;
                case "622":
                    Banco_Nombre.Text = "OPERADORA ACTINVER, S.A. DE C.V.";
                    _vm.User.Banco = "OPERADORA ACTINVER, S.A. DE C.V.";
                    break;
                case "623":
                    Banco_Nombre.Text = "Skandia Vida S.A. de C.V.";
                    _vm.User.Banco = "Skandia Vida S.A. de C.V.";
                    break;
                case "624":
                    Banco_Nombre.Text = "Consultoría Internacional Casa de Cambio, S.A. de C.V.";
                    _vm.User.Banco = "Consultoría Internacional Casa de Cambio, S.A. de C.V.";
                    break;
                case "626":
                    Banco_Nombre.Text = "Deutsche Securities, S.A. de C.V.";
                    _vm.User.Banco = "Deutsche Securities, S.A. de C.V.";
                    break;
                case "627":
                    Banco_Nombre.Text = "Zúrich Compañía de Seguros, S.A.";
                    _vm.User.Banco = "Zúrich Compañía de Seguros, S.A.";
                    break;
                case "628":
                    Banco_Nombre.Text = "Zúrich Vida, Compañía de Seguros, S.A.";
                    _vm.User.Banco = "Zúrich Vida, Compañía de Seguros, S.A.";
                    break;
                case "629":
                    Banco_Nombre.Text = "Hipotecaria su Casita, S.A. de C.V.";
                    _vm.User.Banco = "Hipotecaria su Casita, S.A. de C.V.";
                    break;
                case "630":
                    Banco_Nombre.Text = "Intercam Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Intercam Casa de Bolsa, S.A. de C.V.";
                    break;
                case "631":
                    Banco_Nombre.Text = "CI Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "CI Casa de Bolsa, S.A. de C.V.";
                    break;
                case "632":
                    Banco_Nombre.Text = "Bulltick Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Bulltick Casa de Bolsa, S.A. de C.V.";
                    break;
                case "633":
                    Banco_Nombre.Text = "Sterling Casa de Cambio, S.A. de C.V.";
                    _vm.User.Banco = "Sterling Casa de Cambio, S.A. de C.V.";
                    break;
                case "634":
                    Banco_Nombre.Text = "Fincomún, Servicios Financieros Comunitarios, S.A. de C.V.";
                    _vm.User.Banco = "Fincomún, Servicios Financieros Comunitarios, S.A. de C.V.";
                    break;
                case "636":
                    Banco_Nombre.Text = "HDI Seguros, S.A. de C.V.";
                    _vm.User.Banco = "HDI Seguros, S.A. de C.V.";
                    break;
                case "637":
                    Banco_Nombre.Text = "OrderExpress Casa de Cambio , S.A. de C.V. AAC";
                    _vm.User.Banco = "OrderExpress Casa de Cambio , S.A. de C.V. AAC";
                    break;
                case "638":
                    Banco_Nombre.Text = "Akala, S.A. de C.V., Sociedad Financiera Popular";
                    _vm.User.Banco = "Akala, S.A. de C.V., Sociedad Financiera Popular";
                    break;
                case "640":
                    Banco_Nombre.Text = "J.P. Morgan Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "J.P. Morgan Casa de Bolsa, S.A. de C.V.";
                    break;
                case "642":
                    Banco_Nombre.Text = "Operadora de Recursos Reforma, S.A. de C.V.";
                    _vm.User.Banco = "Operadora de Recursos Reforma, S.A. de C.V.";
                    break;
                case "646":
                    Banco_Nombre.Text = "Sistema de Transferencias y Pagos STP, S.A. de C.V., SOFOM E.N.R.";
                    _vm.User.Banco = "Sistema de Transferencias y Pagos STP, S.A. de C.V., SOFOM E.N.R.";
                    break;
                case "647":
                    Banco_Nombre.Text = "Telecomunicaciones de México";
                    _vm.User.Banco = "Telecomunicaciones de México";
                    break;
                case "648":
                    Banco_Nombre.Text = "Evercore Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Evercore Casa de Bolsa, S.A. de C.V.";
                    break;
                case "649":
                    Banco_Nombre.Text = "Skandia Operadora S.A. de C.V.";
                    _vm.User.Banco = "Skandia Operadora S.A. de C.V.";
                    break;
                case "651":
                    Banco_Nombre.Text = "Seguros Monterrey New York Life, S.A de C.V.";
                    _vm.User.Banco = "Seguros Monterrey New York Life, S.A de C.V.";
                    break;
                case "652":
                    Banco_Nombre.Text = "Solución Asea, S.A. de C.V., Sociedad Financiera Popular";
                    _vm.User.Banco = "Solución Asea, S.A. de C.V., Sociedad Financiera Popular";
                    break;
                case "653":
                    Banco_Nombre.Text = "Kuspit Casa de Bolsa, S.A. de C.V.";
                    _vm.User.Banco = "Kuspit Casa de Bolsa, S.A. de C.V.";
                    break;
                case "655":
                    Banco_Nombre.Text = "J.P. SOFIEXPRESS, S.A. de C.V., S.F.P.";
                    _vm.User.Banco = "J.P. SOFIEXPRESS, S.A. de C.V., S.F.P.";
                    break;
                case "656":
                    Banco_Nombre.Text = "UNAGRA, S.A. de C.V., S.F.P.";
                    _vm.User.Banco = "UNAGRA, S.A. de C.V., S.F.P.";
                    break;
                case "659":
                    Banco_Nombre.Text = "Opciones Empresariales Del Noreste, S.A. DE C.V.";
                    _vm.User.Banco = "Opciones Empresariales Del Noreste, S.A. DE C.V.";
                    break;
                case "670":
                    Banco_Nombre.Text = "Libertad Servicios Financieros, S.A. De C.V.";
                    _vm.User.Banco = "Libertad Servicios Financieros, S.A. De C.V.";
                    break;
                case "674":
                    Banco_Nombre.Text = "AXA Seguros, S.A. De C.V.";
                    _vm.User.Banco = "AXA Seguros, S.A. De C.V.";
                    break;
                case "677":
                    Banco_Nombre.Text = "Caja Popular Mexicana, S.C. de A.P. de R.L. De C.V.";
                    _vm.User.Banco = "Caja Popular Mexicana, S.C. de A.P. de R.L. De C.V.";
                    break;
                case "679":
                    Banco_Nombre.Text = "Financiera Nacional De Desarrollo Agropecuario, Rural, F y P.";
                    _vm.User.Banco = "Financiera Nacional De Desarrollo Agropecuario, Rural, F y P.";
                    break;
                case "684":
                    Banco_Nombre.Text = "Operadora De Pagos Móviles De México, S.A. De C.V.";
                    _vm.User.Banco = "Operadora De Pagos Móviles De México, S.A. De C.V.";
                    break;
                case "901":
                    Banco_Nombre.Text = "CLS Bank International";
                    _vm.User.Banco = "CLS Bank International";
                    break;
                case "902":
                    Banco_Nombre.Text = "SD. INDEVAL, S.A. de C.V.";
                    _vm.User.Banco = "SD. INDEVAL, S.A. de C.V.";
                    break;
                case "999":
                    Banco_Nombre.Text = "";
                    _vm.User.Banco = "";
                    break;
                default:
                    Banco_Nombre.Text = "";
                    _vm.User.Banco = "";
                    break;
            }
        }

        private void BtnRegistrarse_Clicked(object sender, EventArgs e)
        {
            NombreValidationMethod();
            ApellidoValidationMethod();
            EmailValidationMethod();
            PhoneNumberValidationMethod();
            ClabeValidationMethod();
        }
    }
}