﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using HomieProveedorApp.ViewModel;
using HomieProveedorApp.Views.Common;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SolicitudesPage : ContentPage
	{
	    private SolicitudesViewModel _vm;
		public SolicitudesPage ()
		{
            NavigationPage.SetHasNavigationBar(this,false);
			InitializeComponent ();
            _vm = new SolicitudesViewModel();
            _vm.PropertyChanged += VmOnPropertyChanged;
		    BindingContext = _vm;

		    img.GestureRecognizers.Add(new TapGestureRecognizer(async view =>
		    {
		        var image = new ImagenPage(_vm.Solicitud.Foto);
		        if (!Navigation.NavigationStack.Any(x => x.GetType() == typeof(ImagenPage)) && !Navigation.ModalStack.Any(x => x.GetType() == typeof(ImagenPage)))
		            await Navigation.PushAsync(image);
		    }));

            mapImage.GestureRecognizers.Add(new TapGestureRecognizer(view =>
            {
                var uri = string.Empty;
                Device.OnPlatform( iOS: () => { uri = "http://maps.apple.com/maps"; }, Android: () => { uri = "http://maps.google.com/maps"; });
                Device.OpenUri(new Uri($"{uri}?q={_vm.Solicitud.Latitud},{_vm.Solicitud.Longitud}"));
            }));

		    if (Device.RuntimePlatform == Device.iOS)
		    {
		        this.Padding = new Thickness(0, 30, 0, 0);
		    }
        }

	    private void VmOnPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
	        if (e.PropertyName == "Aceptado")
	        {
	            AvAceptar.Play();
	        }
	        if (e.PropertyName == "Rechazado")
	        {
	            AvRechazar.Play();
	        }
	        if (e.PropertyName == "Solicitud")
	        {
	            if (_vm.Solicitud != null)
	            {
	                ListViewCategorias.HeightRequest = _vm.Solicitud.Categorias.Count * 50;
                }
	        }
	    }

	    protected override async void OnAppearing()
	    {
	        base.OnAppearing();
            await _vm.Buscar();
        }
	}
}