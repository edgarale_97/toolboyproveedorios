﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomieProveedorApp.Entities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HomieProveedorApp.Views.Templates
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResumenMaterialesItemTemplate : ContentView
	{
	    private Materiales _material;
		public ResumenMaterialesItemTemplate ()
		{
			InitializeComponent ();
		    BindingContextChanged += (sender, args) =>
		    {
		        if (BindingContext != null)
		        {
		            _material = (Materiales) BindingContext;
		        }
		    };
		}
	}
}