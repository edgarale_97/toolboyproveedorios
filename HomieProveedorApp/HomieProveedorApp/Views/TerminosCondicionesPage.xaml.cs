﻿using System;
using System.Collections.Generic;
using HomieProveedorApp.Helpers;
using Xamarin.Forms;

namespace HomieProveedorApp.Views
{
    public partial class TerminosCondicionesPage : ContentPage
    {
        public TerminosCondicionesPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            //if (Device.RuntimePlatform == Device.iOS)
            //{
            //    this.Padding = new Thickness(0, 30, 0, 0);
            //}

            label.Text = Settings.TerminosYCondiciones;

        }
    }
}
