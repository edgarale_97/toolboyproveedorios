﻿using System;
using System.Collections.Generic;
using HomieProveedorApp.Helpers;
using Xamarin.Forms;

namespace HomieProveedorApp.Views
{
    public partial class TermsConditionsPage : ContentPage
    {
        public event EventHandler TerminosConfirmado;

        public TermsConditionsPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            label.Text = Settings.TerminosYCondiciones;

            if(Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 30, 0, 0);
            }

        }

        protected virtual void OnTerminosConfirmado()
        {
            EventHandler handler = TerminosConfirmado;
            if(handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}
